﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class MessageEntity
    {
        private int msgId;
        private Int64 conversationId;
        private string msgSubject;
        private string msgBody;
        private int msgType;
        private bool msgStatus;
        private Int64 sentByAccountId;
        private Int64 recipientAccountId;
        private Int64 adId;
        private DateTime creationDate;
        private DateTime timestamp;
        private int msgThread;
        private string profilePic;
        private string bandName;
        private Int64 msgEntity; // Ad, Instrument Classified, general etc.
        private string ipaddress;
        private Int64 user1;
        private Int64 user2;
        private string userName1;
        private string userName2;
        private string profilePicUser1;
        private string profilePicUser2;

        public MessageEntity()
        {
        }
        public Int64 User1
        {
            get;
            set;
        }
        public Int64 User2
        {
            get;
            set;
        }
        public string UserName1
        {
            get;
            set;
        }
        public string UserName2
        {
            get;
            set;
        }
        public string ProfilePicUser1
        {
            get;
            set;
        }
        public string ProfilePicUser2
        {
            get;
            set;
        }
        public string SenderName
        {
            get;
            set;
        }
        public Int64 ConversationId
        {
            get;
            set;
        }
        public string picPath
        {
            get;
            set;
        }
        public string IpAddress
        {
            get;
            set;
        }
        public Int64 MsgEntity
        {
            get;
            set;
        }
        public string ProfilePic
        {
            get;
            set;
        }
        public string BandName
        {
            get;
            set;
        }
        public int MsgThread
        {
            get;
            set;
        }
        public int MsgId
        {
            get;
            set;
        }
        public string MsgSubject
        {
            get;
            set;
        }
        public string MsgBody
        {
            get;
            set;
        }
        public int MsgType
        {
            get;
            set;
        }
        public bool MsgStatus
        {
            get;
            set;
        }
        public Int64 SentByAccountId
        {
            get;
            set;
        }
        public Int64 RecipientAccountId
        {
            get;
            set;
        }
        public Int64 AdId
        {
            get;
            set;
        }
        public DateTime CreationDate
        {
            get;
            set;
        }
        public String DisplayDate
        {
            get;
            set;
        }
        public DateTime TimeStamp
        {
            get;
            set;
        }

    }

   
}
