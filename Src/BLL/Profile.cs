﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Profile
    {
        private string soundCloud;
        private string youtube;
        private string facebook;
        private string twitter;
        private Int64 profileId;
        private string image;
        private string fName;
        private string lName;
        private string location;
        private string[] category;
        private string genre;
        private DateTime updationDate;
        private DateTime creationDate;
        private string updationIPAddress;
        private Int64 accountId;
        private DateTime dob;
        private string gender;
        private string age;
        private int countryCode;
        private int cityCode;
        private int stateCode;
        private string countryName;
        private string stateName;
        private string cityName;
        private DateTime timestamp;
        private string soundcloudframe;
        private string youtubeframe;
        private int profiletype;
        private string displayName;
        private string bandname;
        private int tagId;
        private string tagName;



        public string TagName
        {
            get;
            set;
        }
        public int TagId
        {
            get;
            set;
        }
        public Int64 ProfileId
        {
            get;
            set;
        }
        public string BandName
        {
            get;
            set;
        }
        public string DisplayName
        {
            get;
            set;
        }
        public string ProfilePic
        {
            get;
            set;
        }
        public string FName
        {
            get;
            set;
        }
        public string LName
        {
            get;
            set;
        }
        public string ContactNo
        {
            get;
            set;
        }
        public string Image
        {
            get;
            set;
        }
        public string Gender
        {
            get;
            set;
        }
        public string CountryName
        {
            get;
            set;
        }
        public string StateName
        {
            get;
            set;
        }
        public string CityName
        {
            get;
            set;
        }
        public int CountryCode
        {
            get;
            set;
        }
        public int StateCode
        {
            get;
            set;
        }
        public int CityCode
        {
            get;
            set;
        }
    
        public string PinCode
        {
            get;
            set;
        }
        public DateTime DOB
        {
            get;
            set;
        }
        public int Category
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
        public DateTime CreationDate
        {
            get;
            set;
        }
        public DateTime UpdationDate
        {
            get;
            set;
        }
        public DateTime Timestamp
        {
            get;
            set;
        }
        public Int64 AccountId
        {
            get;
            set;
        }
        public string Location // Value fetched through IP address
        {
            get;
            set;
        }
        public string SoundCloud
        {
            get;
            set;
        }
        public string Twitter
        {
            get;
            set;
        }
        public string Youtube
        {
            get;
            set;
        }
        public string Facebook
        {
            get;
            set;
        }
        public string SoundCloudFrame
        {
            get;
            set;
        }
        public string YoutubeFrame
        {
            get;
            set;
        }
        public int ProfileType
        {
            get;
            set;
        }
    }
}
