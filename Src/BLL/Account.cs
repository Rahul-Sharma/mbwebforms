﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Account
    {
        private Int64 accountId;
        private string email;
        private string password;
        private string fName;
        private string name;
        private bool emailVerified;
       // private string mName;
        private string mName;
        private string lName;
        private int age;
        private string dob;
        private int timestamp;
        private string gender;
        private string location;
        private bool fBReg;
        private bool twitterReg;
        private bool manualReg;
        private string profilepic;


        public Int64 AccountId
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public string ProfilePic
        {
            get;
            set;
        }
        
        public string FBUserName
        {
            get;
            set;
        }
        public Int64 FbId
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string FName
        {
            get;
            set;
        }
        public string Mname
        {
            get;
            set;
        }
        public string LName
        {
            get;
            set;
        }
        public bool EmailVerified
        {
            get;
            set;
        }
        public string DOB
        {
            get;
            set;
        }
        public int Age
        {
            get;
            set;
        }
        
        public DateTime Timestamp
        {
            get;
            set;
        }
      
        public string Gender
        {
            get;
            set;
        }
        public string Location
        {
            get;
            set;
        }

        public bool FBReg
        {
            get;
            set;
        }
        public bool TwitterReg
        {
            get;
            set;
        }
        public bool ManualReg
        {
            get;
            set;
        }
     

    }

}
