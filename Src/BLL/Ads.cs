﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Ads
    {
        private Int64 accountId;
        private Int64 adId;
        private string description; //Guid
        private string genre; // this ; set by selecting multiple genre from list and concatanating them into one field
        private string subject;
        private DateTime dateposted;
        private string postdate;
        private int countryCode;
        private string cityCode;
        private string stateCode;
        private string country;
        private string state;
        private string city;
        private bool status; // 1 is active; 0 is inactive!
        private bool isDeleted; // 1 is Deleted; 0 is is not!
        private string location;
        private int Int64; //Guid
        private int views;
        private string tags;
        private int tagId;
        private int tagCount;
        private int objId;
        private string postObjective;
        private string searchobjective;

        public int ObjId
        {
            get;
            set;
        }
        public string PostObjective
        {
            get;
            set;
        }
        public string SearchObjective
        {
            get;
            set;
        }

        public Ads()
        {
        }
        public Ads(Int64 AccountId, Int64 _AdId, string _Subject, string _Description, string _PicPath, string _BandName, string _tags)
        {
            AdId = _AdId;
            Subject = _Subject;
            Description = _Description;
            PicPath = _PicPath;
            DisplayName = _BandName;
            Tags = _tags;
        }
        public int TagId
        {
            get;
            set;
        }
        public string PostDate
        {
            get;
            set;
        }
        public int TagCount
        {
            get;
            set;
        }
        public string AdIdStr // for datalist findcontrol
        {
            get;
            set;
        }
        public string TagName
        {
            get;
            set;
        }
        public string DisplayName
        {
            get;
            set;
        }
        public string PicPath
        {
            get;
            set;
        }
        public Int64 AccountId
        {
            get;
            set;
        }
        public Int64 AdId
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
        public string Genre
        {
            get;
            set;
        }
        public string Subject
        {
            get;
            set;
        }
        public DateTime DatePosted
        {
            get;
            set;
        }
        public int CountryCode
        {
            get;
            set;
        }
        public int CityCode
        {
            get;
            set;
        }
        public int StateCode
        {
            get;
            set;
        }
        public string StatusCode
        {
            get;
            set;
        }
        public string Country
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
        public bool Status
        {
            get;
            set;
        }
        public bool IsDeleted
        {
            get;
            set;
        }
        public string Location
        {
            get;
            set;
        }
        public Int64 ProfileId
        {
            get;
            set;
        }
        public int Views
        {
            get;
            set;
        }
        public string Tags
        {
            get;
            set;
        }

    }

}
