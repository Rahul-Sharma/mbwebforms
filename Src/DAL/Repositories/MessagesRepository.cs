﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Repositories
{
    public class MessagesRepository
    {
        public int SendMessage(MessageEntity msg)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "messagesend";
            cmd.Parameters.AddWithValue("@Sender", msg.SentByAccountId);
            cmd.Parameters.AddWithValue("@Recipient", msg.RecipientAccountId);
            cmd.Parameters.AddWithValue("@Message", msg.MsgBody);
            cmd.Parameters.AddWithValue("@Status", msg.MsgStatus);
            cmd.Parameters.AddWithValue("@EntityId", msg.MsgEntity);
            cmd.Parameters.AddWithValue("@Type", msg.MsgType);
            cmd.Parameters.AddWithValue("@IPAddress", msg.IpAddress);
            DataFunction objDf = new DataFunction();
            int ifSent = objDf.ExecuteNonQueryWithParameters(objDf.Connection(), cmd);
            return ifSent;
        }
        public int SendReplyByConId(MessageEntity msg)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "messagereply";
            cmd.Parameters.AddWithValue("@ConversationId", msg.ConversationId);
            cmd.Parameters.AddWithValue("@Sender", msg.SentByAccountId);
            cmd.Parameters.AddWithValue("@Message", msg.MsgBody);
            cmd.Parameters.AddWithValue("@Status", msg.MsgStatus);
            cmd.Parameters.AddWithValue("@Type", msg.MsgType);
            cmd.Parameters.AddWithValue("@IPAddress", msg.IpAddress);
            DataFunction objDf = new DataFunction();
            int ifSent = objDf.ExecuteNonQueryWithParameters(objDf.Connection(), cmd);
            return ifSent;
        }
        //    List<Messages> GetRecievedMessages(){
        //    }
        //    List<Messages> GetSentMessages()
        //{
        //}
        //    List<Messages> GetMessageThread()
        //    {
        //        //return List<> int;
        //    }
        //}

        //For Messaging section
        public List<Ads> LoadAdsSnippets(Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getadsnippets";
            cmd.Parameters.AddWithValue("AccountId", accId);
            DataFunction df = new DataFunction();
            List<Ads> lst = df.loadAdSnippets(df.Connection(), cmd);
            return lst;
        }
        public MessageEntity getSendMsgAttributes(Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getsendmessageattr";
            cmd.Parameters.AddWithValue("AccountId", accId);
            DataFunction df = new DataFunction();
            MessageEntity msg = new MessageEntity();
            msg = df.getSendMsgattributes(df.Connection(), cmd);
            return msg;
        }
        public List<MessageEntity> LoadInbox(Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getmessagelist";
            cmd.Parameters.AddWithValue("AccountId", accId);
            DataFunction df = new DataFunction();
            List<MessageEntity> lst = df.loadInbox(df.Connection(), cmd);
            return lst;
        }
        public List<MessageEntity> LoadSent(Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
           
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "msglstsent";
            cmd.Parameters.AddWithValue("AccountId", accId);
            DataFunction df = new DataFunction();
            List<MessageEntity> lst = df.openMsgThread(df.Connection(), cmd);
            return lst;
        }
        public List<MessageEntity> OpenMsgThreadbyConversationId(Int64 convId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "openconversation";
            cmd.Parameters.AddWithValue("@ConversationId", convId);
            DataFunction df = new DataFunction();
            List<MessageEntity> lst = df.openMsgThread(df.Connection(), cmd);
            return lst;
        }
        public List<MessageEntity> getMsgsByAd(Int64 EntityId, Int64 AccId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "msglstbyads";
            cmd.Parameters.AddWithValue("@EntityId", EntityId);
            cmd.Parameters.AddWithValue("@AccountId", AccId);
            DataFunction df = new DataFunction();
            List<MessageEntity> lst = df.openMsgThread(df.Connection(), cmd);
            return lst;
        }
    }
}
