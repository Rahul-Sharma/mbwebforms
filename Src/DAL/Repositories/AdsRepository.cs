﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using DAL.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Repositories
{
    public class AdsRepository
    {
        public List<Ads> ViewAddByAccount(Int64 AccountId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@AccountId", AccountId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "LoadAdByAccountId";
            List<Ads> lst = getAds(cmd);
            //List<Ads> lstAds = df.LoadAdsUser(df.Connection(), cmd);
            return lst;
        }

        public List<Ads> ViewAddByLocation(string state, string city)
        {
            List<Ads> lstAds = new List<Ads>();

            return lstAds;
        }
        public Ads ViewAddByAdId(Int64 AdId)
        {
            Ads obj = new Ads();
            return obj;
        }
        public Int64 UpdateAd(Ads adObj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "EditAd";
            cmd.Parameters.AddWithValue("@Subject", adObj.Subject);
            cmd.Parameters.AddWithValue("@Description", adObj.Description);
            cmd.Parameters.AddWithValue("@ObjId", adObj.ObjId);
            cmd.Parameters.AddWithValue("@AdId", adObj.AdId);
            DataFunction df = new DataFunction();
            Int64 returnVal = df.ReturnUpdateInsert(df.Connection(), cmd);
            return returnVal;
        }
        public Int64 DeleteAd(string AdId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteAd";
            cmd.Parameters.AddWithValue("@AdId", AdId);
            DataFunction df = new DataFunction();
            Int64 returnVal = df.ReturnUpdateInsert(df.Connection(), cmd);
            return returnVal;
        }
        public Int64 getAdReplyCount(Int64 AdId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdReplyCount";
            cmd.Parameters.AddWithValue("@EntityId", AdId);
            DataFunction df = new DataFunction();
            Int64 returnVal = df.ReturnUpdateInsert(df.Connection(), cmd);
            return returnVal;
        }

        public int InsertTag(Int64 AdId, int TagId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@AdId", AdId);
            cmd.Parameters.AddWithValue("@TagId", TagId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "insertadtag";
            DataFunction df = new DataFunction();
            int returVal = df.ExecuteNonQueryWithParameters(df.Connection(), cmd);
            return returVal;

        }
        public List<Ads> LoadCountryListForAdSearch()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getCountryListForAdSearch";
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.LoadCountryList(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadCitiesForSelectedState(int stateCode)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getCitiesListForState";
            cmd.Parameters.AddWithValue("@StateId", stateCode);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.LoadCitiesList(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadStatesForSelectedCountry(int countryId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getStateListForCountry";
            cmd.Parameters.AddWithValue("@CountryId", countryId);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.LoadStateList(df.Connection(), cmd);
            return lstAds;
        }

        public Int64 PostAdd(Ads Ad)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            Int64 inserted = 0;
            DataFunction func = new DataFunction();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@AccountId", SqlDbType.BigInt);
            param[0].Value = Ad.AccountId;
            param[1] = new SqlParameter("@Subject", SqlDbType.NVarChar, 100);
            param[1].Value = Ad.Subject;
            param[2] = new SqlParameter("@Description", SqlDbType.VarChar, 500);
            param[2].Value = Ad.Description;
            param[3] = new SqlParameter("@Country", SqlDbType.VarChar, 50);
            param[3].Value = Ad.Country;
            param[4] = new SqlParameter("@State", SqlDbType.VarChar, 50);
            param[4].Value = Ad.State;
            param[5] = new SqlParameter("@City", SqlDbType.VarChar, 50);
            param[5].Value = Ad.City;
            param[6] = new SqlParameter("@Tags", SqlDbType.VarChar, 150);
            param[6].Value = Ad.Tags;
            param[7] = new SqlParameter("@ObjId", SqlDbType.Int);
            param[7].Value = Ad.ObjId;
            inserted = func.CheckResult(func.Connection(), "PostAd", param);
            return inserted;
        }

        public List<Ads> ViewAdsForPublicUser()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsForPublicUser";
            DataFunction df = new DataFunction();
            List<Ads> lst = getAds(cmd); 
            //List<Ads> lstAds = df.LoadAdsUser(df.Connection(), cmd);
            //return lstAds;
            return lst;
        }
        public List<Ads> getAdsByAdId(Int64 AdId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdByAdId";
            cmd.Parameters.AddWithValue("@AdId", AdId);
            DataFunction df = new DataFunction();
            List<Ads> lst = getAds(cmd);
            //List<Ads> lstAds = df.LoadAdsUser(df.Connection(), cmd);
            //return lstAds;
            return lst;
        }

        public List<Ads> showAdsByLocationAndObjective(int countryId, int stateId, int cityId, int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByLocObj";
            cmd.Parameters.AddWithValue("@CountryCode",countryId);
            cmd.Parameters.AddWithValue("@StateCode", stateId);
            cmd.Parameters.AddWithValue("@CityCode", cityId);
            cmd.Parameters.AddWithValue("@ObjId", objId);
            List<Ads> lst = getAds(cmd);
            return lst;
        }

        public List<Ads> ViewAdsByLocaleUser(int City, int State, int Country, Int64 AccountId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Country", Country);
            cmd.Parameters.AddWithValue("@State", State);
            cmd.Parameters.AddWithValue("@City", City);
            cmd.Parameters.AddWithValue("@AccountId", AccountId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByLocaleUser";
            List<Ads> lst = getAds(cmd);
            return lst;
            //DataFunction df = new DataFunction();
            //List<Ads> lstAds = df.LoadAdsUser(df.Connection(), cmd);
            //return lstAds;
        }
        public List<Ads> getCitiesForAds(int country, int state)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", country);
            cmd.Parameters.AddWithValue("@StateCode", state);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getCitiesForAdByCountry";
            DataFunction df = new DataFunction();
            List<Ads> lst = new List<Ads>();
            return (lst = df.LoadCitiesList(df.Connection(), cmd));
        }
        public List<string> LoadTags()
        {
            DataFunction df = new DataFunction();
            List<string> lst = new List<string>();
            lst = df.FillListByColName(df.Connection(), "getTags", "TagName");
            return lst;
        }
        public List<Ads> LoadTagsIds()
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "loadtags";
            List<Ads> lst = new List<Ads>();
            DataFunction df = new DataFunction();
            lst = df.loadTagsIdsDF(df.Connection(), cmd);
            return lst;

        }
        public List<Ads> LoadObjectives()
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "LoadPostObjectives";
            List<Ads> lst = new List<Ads>();
            DataFunction df = new DataFunction();
            lst = df.LoadObjectives(df.Connection(), cmd);
            return lst;

        }
        public List<Ads> SearchObjectives()
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "LoadSearchBojectives";
            List<Ads> lst = new List<Ads>();
            DataFunction df = new DataFunction();
            lst = df.LoadSearchBojectives(df.Connection(), cmd);
            return lst;

        }
        public List<Ads> LoadTagsByLocationAndAccount(int city, int state, int country, Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "showTagsByLocationAndAccount";
            cmd.Parameters.AddWithValue("@CountryCode", country);
            cmd.Parameters.AddWithValue("@StateCode", state);
            cmd.Parameters.AddWithValue("@CityCode", city);
            cmd.Parameters.AddWithValue("@AccountId", accId);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdCount(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadTagsByRegionAndAccount()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "showTagsByLocationAndAccount";
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdsDF(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadSearchTagsDefault()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getTagsAllPosted";
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdCount(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadSearchTagsByRegion(int country, int state, int city)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ShowTagsByRegion";
            cmd.Parameters.AddWithValue("@CountryCode", country);
            cmd.Parameters.AddWithValue("@StateCode", state);
            cmd.Parameters.AddWithValue("@CityCode", city);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdCount(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadSearchTagsByRegionAndObj(int country, int state, int city, int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ShowTagsByObjAndLoc";
            cmd.Parameters.AddWithValue("@CountryCode", country);
            cmd.Parameters.AddWithValue("@StateCode", state);
            cmd.Parameters.AddWithValue("@CityCode", city);
            cmd.Parameters.AddWithValue("@ObjId", objId);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdCount(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadSearchTagsByObj(int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ShowTagsByObj";
            cmd.Parameters.AddWithValue("@ObjId", objId);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdCount(df.Connection(), cmd);
            return lstAds;
        }
        public List<Ads> LoadTagsIdsByAd(Int64 AdId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "generatetagsforad";
            cmd.Parameters.AddWithValue("@AdId", AdId);
            DataFunction df = new DataFunction();
            List<Ads> lstAds = df.loadTagsIdsDF(df.Connection(), cmd);
            return lstAds;
        }

        public List<string> LoadCountries()
        {
            DataFunction df = new DataFunction();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getcountrylist";
            List<string> lstAds = FillListByColName(cmd, "Country");
            return lstAds;
        }


        private List<string> FillListByColName(SqlCommand cmd, string colName)
        {
            List<string> lst = new List<string>();
            DataFunction df = new DataFunction();
            SqlDataReader reader = df.getReader(df.Connection(), cmd);
            while (reader.Read())
            {
                lst.Add(Convert.ToString(reader[colName]));
            }
            reader.Close();
            return lst;
        }
        public List<string> LoadStatesByCountry()
        {
            DataFunction df = new DataFunction();
            List<string> lstAds = df.FillListByColName(df.Connection(), "getcountrylist", "State");
            return lstAds;
        }
        public List<string> LoadCitiesByState()
        {
            DataFunction df = new DataFunction();
            List<string> lstAds = df.FillListByColName(df.Connection(), "getcitylist", "City");
            return lstAds;
        }
        public List<Ads> ViewAdsByAccountandTag(int CountryCode, int StateCode, int CityCode, Int64 AccountId, int tagId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", CountryCode);
            cmd.Parameters.AddWithValue("@StateCode", StateCode);
            cmd.Parameters.AddWithValue("@CityCode", CityCode);
            cmd.Parameters.AddWithValue("@AccountId", AccountId);
            cmd.Parameters.AddWithValue("@TagId", tagId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetAdsbyLocAndTagForAuthUser";
            List<Ads> lst = getAds(cmd);
            //DataFunction df = new DataFunction();
            //List<Ads> lstAds = df.LoadAdsUser(df.Connection(), cmd);
            return lst;
        }
        public List<Ads> getAdsByAllFilters(int countryCode, int stateCode, int cityCode, int tagId, int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", countryCode);
            cmd.Parameters.AddWithValue("@StateCode", stateCode);
            cmd.Parameters.AddWithValue("@CityCode", cityCode);
            cmd.Parameters.AddWithValue("@TagId", tagId);
            cmd.Parameters.AddWithValue("@ObjId", objId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByAllFilters";
            List<Ads> lst = getAds(cmd);
            return lst;
            //DataFunction df = new DataFunction();
            //List<Ads> lstAds = df.LoadAdsFiltered(df.Connection(), cmd);
            //return lstAds;
        }
        public List<Ads> getAdsByLocAndTag(int countryCode, int stateCode, int cityCode, int tagId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", countryCode);
            cmd.Parameters.AddWithValue("@StateCode", stateCode);
            cmd.Parameters.AddWithValue("@CityCode", cityCode);
            cmd.Parameters.AddWithValue("@TagId", tagId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByLocTag";
            List<Ads> lst = getAds(cmd);
            return lst;
        }
        public List<Ads> getAdsByLocAndObj(int countryCode, int stateCode, int cityCode, int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", countryCode);
            cmd.Parameters.AddWithValue("@StateCode", stateCode);
            cmd.Parameters.AddWithValue("@CityCode", cityCode);
            cmd.Parameters.AddWithValue("@ObjId", objId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByLocObj";
            List<Ads> lst = getAds(cmd);
            return lst;
        }
        public List<Ads> getAdsByTagAndObjId(int tagId, int objId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@TagId", tagId);
            cmd.Parameters.AddWithValue("@ObjId", objId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByTagObj";
            List<Ads> lst = getAds(cmd);
            return lst;
        }
        public List<Ads> getAdsByLocation(int countryCode, int stateCode, int cityCode)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", countryCode);//
            cmd.Parameters.AddWithValue("@StateCode", stateCode);//
            cmd.Parameters.AddWithValue("@CityCode", cityCode); //
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByLocation";
            List<Ads> lst = getAds(cmd);
            return lst;
        }




        public List<Ads> getAdsByTag(int tagId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@TagId", tagId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByTag";
            List<Ads> lst = getAds(cmd);
            return lst;
        }
        public List<Ads> getAdsByObjective(int objId)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@ObjId", objId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAdsByObjective";
            List<Ads> lst = getAds(cmd);
            return lst;
        }

        private List<Ads> getAds(SqlCommand cmd)
        {
            DataFunction df = new DataFunction();
            SqlDataReader reader = df.getReader(df.Connection(), cmd);
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    AccountId = Convert.ToInt64(reader["AccountId"]),
                    AdId = Convert.ToInt64(reader["AdId"]),
                    AdIdStr = Convert.ToString(reader["AdId"]),
                    Subject = Convert.ToString(reader["Subject"]),
                    Description = Convert.ToString(reader["Description"]),
                    PicPath = Convert.ToString(reader["ProfilePic"]),
                    DisplayName = Convert.ToString(reader["BandName"]),
                    Tags = Convert.ToString(reader["Tags"]),
                    PostDate = Convert.ToString(reader["DatePosted"])
                });
            }
            reader.Close();
            return lst;
        }
    }
}
