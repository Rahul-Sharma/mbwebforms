﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections;
using System.Web;
using BLL;
using System.Data;
using System.Data.SqlClient;
using DAL.Interfaces;
using System.Web.Configuration;


namespace DAL.Repositories
{
    public class AccountRepository
    {
        //private Connection conn;
        //public AccountRepository()
        //{
        //    conn = new Connection();
        //}
        public int createaccount(Account acc)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CreateAccount";
            cmd.Parameters.AddWithValue("@AccountId", acc.AccountId);
            cmd.Parameters.AddWithValue("@Email", acc.Email);
            cmd.Parameters.AddWithValue("@Gender", acc.Gender);
            cmd.Parameters.AddWithValue("@FbLocation", acc.Location);
            cmd.Parameters.AddWithValue("@FbId", acc.FbId);
            if (acc.FBUserName == "")
                cmd.Parameters.AddWithValue("@FbUserName", acc.FbId);
            else
                cmd.Parameters.AddWithValue("@FbUserName", acc.FBUserName);
            cmd.Parameters.AddWithValue("@Name", acc.Name);
            cmd.Parameters.AddWithValue("@FbReg", acc.FBReg);
            cmd.Parameters.AddWithValue("@TwitterReg", acc.TwitterReg);
            cmd.Parameters.AddWithValue("@ManualReg", acc.ManualReg);
            cmd.Parameters.AddWithValue("@EmailVerified", acc.EmailVerified);
            cmd.Parameters.AddWithValue("@ProfilePic", acc.ProfilePic);
            DataFunction objDf = new DataFunction();
            int ifSent = objDf.ExecuteNonQueryWithParameters(objDf.Connection(), cmd);
            return ifSent;
        }
        public Int64 createAccountByEmail(Account acc)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CreateAccountByEmail";
            cmd.Parameters.AddWithValue("@AccountId", acc.AccountId);
            cmd.Parameters.AddWithValue("@Password",acc.Password);
            cmd.Parameters.AddWithValue("@Email", acc.Email);
            cmd.Parameters.AddWithValue("@Name", acc.Name);
            cmd.Parameters.AddWithValue("@FbReg", acc.FBReg);
            cmd.Parameters.AddWithValue("@TwitterReg", acc.TwitterReg);
            cmd.Parameters.AddWithValue("@ManualReg", acc.ManualReg);
            cmd.Parameters.AddWithValue("@EmailVerified", acc.EmailVerified);
            DataFunction objDf = new DataFunction();
            Int64 ifCreated = objDf.ReturnUpdateInsert(objDf.Connection(), cmd);
            return ifCreated;
        }

        public string getPasswordByEmail(string email)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getPasswordByEmail";
            cmd.Parameters.AddWithValue("@Email", email);
            DataFunction objDf = new DataFunction();
            string password = objDf.ReturnStringVal(objDf.Connection(), cmd);
            return password;

        }
        public Int64 getAccountIdByEmail(string email)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAccountIdByEmail";
            cmd.Parameters.AddWithValue("@Email", email);
            DataFunction objDf = new DataFunction();
            Int64 accId = objDf.ReturnUpdateInsert(objDf.Connection(), cmd);
            return accId;
        }
        public Account getaccountbyemail(string email)
        {
            Account a = new Account();
            a.Age = 12;
            return a;
        }

        public Int64 loginByEmail(Account acc)
        {
            DataFunction func = new DataFunction();

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
            param[0].Value = acc.Email;
            param[1] = new SqlParameter("@Password", SqlDbType.VarChar, 100);
            param[1].Value = acc.Password;

            Int64 AuthId = func.CheckResult(func.Connection(), "Authenticate", param);
            return AuthId;
        }


        public Int64 checkaccount(string email)
        {
            DataFunction func = new DataFunction();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@email", SqlDbType.VarChar, 50);
            param[0].Value = email;
            Int64 ifInserted = func.CheckResult(func.Connection(), "checkIfRegistered", param);
            return ifInserted;

        }
        public Int64 checkaccountThroughFbId(string fbId)
        {
            DataFunction func = new DataFunction();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@FbId", SqlDbType.BigInt);
            param[0].Value = Convert.ToInt64(fbId.Trim());
            Int64 ifInserted = func.CheckResult(func.Connection(), "CheckByFbId", param);
            return ifInserted;

        }

    }
}
