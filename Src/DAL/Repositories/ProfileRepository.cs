﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BLL;
using System.Threading.Tasks;
using DAL.Interfaces;
using System.Web.Configuration;

namespace DAL.Repositories
{
    public class ProfileRepository
    {

        public int SetLocation(Profile objProfile)
        {
            DataFunction func = new DataFunction();
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CountryCode", objProfile.CountryCode);
            cmd.Parameters.AddWithValue("@StateCode", objProfile.StateCode);
            cmd.Parameters.AddWithValue("@CityCode", objProfile.CityCode);
            cmd.Parameters.AddWithValue("@AccountId", objProfile.AccountId);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SetLocationManual";
            int ifInserted = func.ExecuteNonQueryWithParameters(func.Connection(), cmd);
            return ifInserted;
        }
        public bool CheckLocation(Int64 accountId)
        {
            DataFunction func = new DataFunction();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AccountId", SqlDbType.BigInt);
            param[0].Value = accountId;
            Int64 ifInserted = func.CheckResult(func.Connection(), "checkLocation", param);
            if (ifInserted == 0)
                return false;
            else return true;
        }
        public string getLocation(Int64 accId)
        {
            DataFunction func = new DataFunction();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AccountId", SqlDbType.BigInt);
            param[0].Value = accId;
            string location = func.getLocationByAccountId(func.Connection(), "getLocation", param);
            return location;
        }
        public Int64 UpdateProfile(Profile profile)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "UpdateProfile";
            cmd.Parameters.AddWithValue("@ProfilePic", profile.ProfilePic);
            cmd.Parameters.AddWithValue("@Description", profile.Description);
            cmd.Parameters.AddWithValue("@Name", profile.DisplayName);
            cmd.Parameters.AddWithValue("@UpdationDate", DateTime.Now);
            cmd.Parameters.AddWithValue("@SoundCloud", DBNull.Value);
            cmd.Parameters.AddWithValue("@Twitter", profile.Twitter);
            cmd.Parameters.AddWithValue("@Facebook", profile.Facebook);
            cmd.Parameters.AddWithValue("@Youtube", DBNull.Value);
            cmd.Parameters.AddWithValue("@SoundCloudFrame", profile.SoundCloudFrame);
            cmd.Parameters.AddWithValue("@YoutubeFrame", profile.YoutubeFrame);
            cmd.Parameters.AddWithValue("@AccountId", profile.AccountId);
            DataFunction df = new DataFunction();
            Int64 returnVal = df.ReturnUpdateInsert(df.Connection(), cmd);
            return returnVal;
        }
        public Profile ShowProfile(Int64 accId)
        {
            Profile obj = new Profile();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "LoadProfile";
            cmd.Parameters.AddWithValue("@AccountId", accId);
            DataFunction df = new DataFunction();
            obj = df.LoadProfileData(df.Connection(), cmd);
            return obj;
        }
        public List<Profile> loadTagIdForAccount(Int64 accId)
        {
            List<Profile> obj = new List<Profile>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getAccountTagsIds";
            cmd.Parameters.AddWithValue("@AccountId", accId);
            DataFunction df = new DataFunction();
            obj = df.loadTagIdForAccount(df.Connection(), cmd);
            return obj;
        }
        public int deleteTagsByAccount(Int64 accId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "deleteTagsByAccontId";
            cmd.Parameters.AddWithValue("@AccountId", accId);
            DataFunction df = new DataFunction();
            return (df.ExecuteNonQueryWithParameters(df.Connection(), cmd));
        }
        public int insertTagsByAccountId(Int64 accId, int tagId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "addTagByAccountId";
            cmd.Parameters.AddWithValue("@AccountId", accId);
            cmd.Parameters.AddWithValue("@TagId", tagId);
            DataFunction df = new DataFunction();
            return (df.ExecuteNonQueryWithParameters(df.Connection(), cmd));

        }
        public List<string> getTagsForProfile(Int64 accId)
        {
            DataFunction df = new DataFunction();
            List<string> lst = new List<string>();
            Profile obj = new Profile();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SelectTagsForProfile";
            cmd.Parameters.AddWithValue("@AccountId", accId);
            lst = df.FillListByColNameAndID(df.Connection(), "TagName", cmd);
            return lst;
        }

    }
}
