﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using DAL.Repositories;
using BLL;

namespace DAL
{
    public class DataFunction
    {
        SqlConnection con;
        SqlDataAdapter adp;
        SqlCommand cmd;
        DataSet ds;
        SqlDataReader reader;
        //DataTable dt;
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        public void dbConnection()
        {
            //    adp = new SqlDataAdapter();
            //    con = new SqlConnection(ConfigurationManager.ConnectionStrings
            //            ["dbConnectionString"].ConnectionString);
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private SqlConnection openConnection()
        {
            if (con.State == ConnectionState.Closed || con.State ==
                        ConnectionState.Broken)
            {
                con.Open();
            }
            return con;
        }


        public SqlConnection Connection()
        {
            con = new SqlConnection(connstr);
            return con;
        }
        public void CloseConnection(SqlConnection con)
        {
            con.Close();
        }
        public int ExecuteNonQuery(SqlConnection con, string SpName, SqlParameter[] param)
        {
            cmd = new SqlCommand();
            con.Open();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SpName;
            cmd = AddParameters(cmd, SpName, param);
            int rt = cmd.ExecuteNonQuery();
            con.Close();
            return rt;

        }
        public int ExecuteNonQueryWithParameters(SqlConnection con, SqlCommand cmd)
        {
            con.Open();
            cmd.Connection = con;
            int rt = cmd.ExecuteNonQuery();
            con.Close();
            return rt;
        }
        public SqlDataReader GetDDLData(SqlConnection con, string spName)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            con.Open();
            cmd.CommandText = spName;
            reader = cmd.ExecuteReader();
            //con.Close();
            return reader;

        }
        public SqlDataReader GetReader(SqlConnection con, string spName)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            con.Open();
            cmd.CommandText = spName;
            cmd.CommandType = CommandType.StoredProcedure;
            reader = cmd.ExecuteReader();
            //con.Close();
            return reader;

        }
        public int ExecuteNonQuertyNoPara(SqlConnection con, string spName)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            int ret = cmd.ExecuteNonQuery();
            con.Close();
            return ret;
        }
        private SqlCommand AddParameters(SqlCommand cmd, string spName, SqlParameter[] parma)
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            foreach (SqlParameter pm in parma)
            {
                if (pm.Value == null)
                    pm.Value = DBNull.Value;
                cmd.Parameters.Add(pm);
            }
            return cmd;
        }

        public Int64 CheckResult(SqlConnection con, string spName, SqlParameter[] param)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd = AddParameters(cmd, spName, param);
            Int64 ReturnValue = Convert.ToInt64(cmd.ExecuteScalar());
            con.Close();
            return ReturnValue;
        }
        public Int64 ReturnUpdateInsert(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            Int64 ReturnValue = Convert.ToInt64(cmd.ExecuteScalar());
            con.Close();
            return ReturnValue;
        }
        public string ReturnStringVal(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            string ReturnValue = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
            return ReturnValue;
        }
        public DataSet FillDataSetWithPara(SqlConnection con, string spName, SqlParameter[] param)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            adp = new SqlDataAdapter();
            ds = new DataSet();
            cmd = AddParameters(cmd, spName, param);
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            con.Close();
            return ds;
        }
        public DataSet FillDataSet(SqlConnection con, string spName)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            adp = new SqlDataAdapter();
            ds = new DataSet();
            adp.SelectCommand = cmd;
            adp.Fill(ds);
            con.Close();
            return ds;
        }
        public List<string> FillListByColNameAndID(SqlConnection con, string colName, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<string> lst = new List<string>();
            while (reader.Read())
            {
                lst.Add(Convert.ToString(reader[colName]));
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<string> FillListByColName(SqlConnection con, string spName, string colName)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spName;
            reader = cmd.ExecuteReader();
            List<string> lst = new List<string>();
            while (reader.Read())
            {
                lst.Add(Convert.ToString(reader[colName]));
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public Profile LoadProfileData(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            Profile obj = new Profile();
            while (reader.Read())
            {
                obj.AccountId = Convert.ToInt64(reader["AccountId"]);
                if (Convert.ToString(reader["ProfileType"]) == "")
                    obj.ProfileType = 2;// for Fan/Individual
                else
                    obj.ProfileType = Convert.ToInt32(reader["ProfileType"]);
                obj.Description = Convert.ToString(reader["Description"]);
                obj.DisplayName = Convert.ToString(reader["DisplayName"]);
                obj.CountryName = Convert.ToString(reader["CountryName"]);
                obj.CityName = Convert.ToString(reader["CityName"]);
                obj.ProfilePic = Convert.ToString(reader["ProfilePic"]);
                obj.Facebook = Convert.ToString(reader["Facebook"]);
                obj.SoundCloud = Convert.ToString(reader["Soundcloud"]);
                obj.Twitter = Convert.ToString(reader["Twitter"]);
                obj.Youtube = Convert.ToString(reader["Youtube"]);
                obj.SoundCloudFrame = Convert.ToString(reader["SoundcloudFrame"]);
                obj.YoutubeFrame = Convert.ToString(reader["YoutubeFrame"]);
            }
            con.Close();
            reader.Close();
            return obj;
        }
        //public List<Ads> LoadAdsUser(SqlConnection con, SqlCommand cmdPar)
        //{
        //    con.Open();
        //    cmd = new SqlCommand();
        //    cmd = cmdPar;
        //    cmd.Connection = con;
        //    reader = cmd.ExecuteReader();
        //    List<Ads> lst = new List<Ads>();
        //    while (reader.Read())
        //    {
        //        lst.Add(new Ads
        //        {
        //            AccountId = Convert.ToInt64(reader["AccountId"]),
        //            AdId = Convert.ToInt64(reader["AdId"]),
        //            AdIdStr = Convert.ToString(reader["AdId"]),
        //            Subject = Convert.ToString(reader["Subject"]),
        //            Description = Convert.ToString(reader["Description"]),
        //            PicPath = Convert.ToString(reader["ProfilePic"]),
        //            DisplayName = Convert.ToString(reader["BandName"]),
        //            Tags = Convert.ToString(reader["Tags"]),
        //            PostDate = Convert.ToString(reader["DatePosted"])
        //        });
        //    }
        //    con.Close();
        //    reader.Close();
        //    return lst;
        //}


        //public List<Ads> LoadAdsFiltered(SqlConnection con, SqlCommand cmdPar)
        //{
        //    con.Open();
        //    cmd = new SqlCommand();
        //    cmd = cmdPar;
        //    cmd.Connection = con;
        //    reader = cmd.ExecuteReader();
        //    List<Ads> lst = new List<Ads>();
        //    while (reader.Read())
        //    {
        //        lst.Add(new Ads
        //        {
        //            AccountId = Convert.ToInt64(reader["AccountId"]),
        //            AdId = Convert.ToInt64(reader["AdId"]),
        //            AdIdStr = Convert.ToString(reader["AdId"]),
        //            Subject = Convert.ToString(reader["Subject"]),
        //            Description = Convert.ToString(reader["Description"]),
        //            PicPath = Convert.ToString(reader["ProfilePic"]),
        //            DisplayName = Convert.ToString(reader["BandName"]),
        //            Tags = Convert.ToString(reader["Tags"]),
        //            PostDate = Convert.ToString(reader["DatePosted"])
        //        });
        //    }
        //    con.Close();
        //    reader.Close();
        //    return lst;
        //}


        public List<Ads> LoadCountryList(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    Country = Convert.ToString(reader["CountryName"]),
                    CountryCode = Convert.ToInt32(reader["CountryId"]),

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }

        public List<Ads> LoadStateList(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    State = Convert.ToString(reader["StateName"]),
                    StateCode = Convert.ToInt32(reader["StateId"]),

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Ads> LoadCitiesList(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    City = Convert.ToString(reader["CityName"]),
                    CityCode = Convert.ToInt32(reader["CityId"]),

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }

        public List<Ads> LoadObjectives(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    ObjId = Convert.ToInt32(reader["ObjId"]),
                    PostObjective = Convert.ToString(reader["PostObjective"]),

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Ads> LoadSearchBojectives(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    ObjId = Convert.ToInt32(reader["ObjId"]),
                    SearchObjective = Convert.ToString(reader["SearchObjective"]),

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Ads> loadTagsIdsDF(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    TagId = Convert.ToInt32(reader["TagId"]),
                    TagName = Convert.ToString(reader["TagName"]),


                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Profile> loadTagIdForAccount(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Profile> lst = new List<Profile>();
            while (reader.Read())
            {
                lst.Add(new Profile
                {
                    TagId = Convert.ToInt32(reader["TagId"]),
                    TagName = Convert.ToString(reader["TagName"])

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Ads> loadTagsIdCount(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            while (reader.Read())
            {
                lst.Add(new Ads
                {
                    TagId = Convert.ToInt32(reader["TagId"]),
                    TagName = Convert.ToString(reader["TagName"]),
                    TagCount = Convert.ToInt32(reader["TagCount"])

                });
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<MessageEntity> openMsgThread(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<MessageEntity> lst = new List<MessageEntity>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lst.Add(new MessageEntity
                    {
                        ConversationId = Convert.ToInt64(reader["ConversationId"]),
                        MsgBody = Convert.ToString(reader["Msg"]),
                        SentByAccountId = Convert.ToInt64(reader["SenderId"]),
                        ProfilePic = Convert.ToString(reader["ProfilePic"]),
                        MsgStatus = Convert.ToBoolean(reader["Status"]),
                        BandName = Convert.ToString(reader["BandName"]),
                        DisplayDate = Convert.ToString(reader["Date"]),
                    });
                }
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<MessageEntity> loadInbox(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<MessageEntity> lst = new List<MessageEntity>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lst.Add(new MessageEntity
                    {
                        ConversationId = Convert.ToInt64(reader["ConversationId"]),
                        MsgBody = Convert.ToString(reader["Msg"] == null? "Empty" : reader["Msg"]),
                        SentByAccountId = Convert.ToInt64(reader["SenderId"]),
                        MsgStatus = Convert.ToBoolean(reader["Status"] == null ? 1 : reader["Status"]),
                        BandName = Convert.ToString(reader["SentByAccName"] == null? "Sender" : reader["SentByAccName"]),
                        DisplayDate = Convert.ToString(reader["Date"]),
                        User1 = Convert.ToInt64(reader["User1"]==null? "":reader["User1"]),
                        User2 = Convert.ToInt64(reader["User2"] == null? "" : reader["User2"]),
                        UserName1 = Convert.ToString(reader["UserName1"] == null? "X" : reader["UserName1"]),
                        UserName2 = Convert.ToString(reader["UserName2"] == null? "X" : reader["UserName2"]),
                        ProfilePicUser1 = Convert.ToString(reader["ProfilePic1"] == null? "DF.jpg" : reader["ProfilePic1"]),
                        ProfilePicUser2 = Convert.ToString(reader["ProfilePic2"] == null? "DF.jpg" : reader["ProfilePic2"])
                    });
                }
            }
            con.Close();
            reader.Close();
            return lst;
        }


        public string getLocationByAccountId(SqlConnection con, string spName, SqlParameter[] parm)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd.Connection = con;
            adp = new SqlDataAdapter();
            ds = new DataSet();
            cmd = AddParameters(cmd, spName, parm);
            reader = cmd.ExecuteReader();
            string loc = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (Convert.ToString(reader["CountryCode"]) != "")
                        loc += Convert.ToString(reader["CountryCode"]) + "-" + Convert.ToString(reader["StateCode"]) + "-" + Convert.ToString(reader["CityCode"]);
                }
            }
            reader.Close();
            con.Close();
            return loc;

        }
        public List<MessageEntity> loadMsgsByAd(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<MessageEntity> lst = new List<MessageEntity>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lst.Add(new MessageEntity
                    {
                        ConversationId = Convert.ToInt64(reader["ConversationId"]),
                        MsgBody = Convert.ToString(reader["Msg"]),
                        SentByAccountId = Convert.ToInt64(reader["SenderId"]),
                        ProfilePic = Convert.ToString(reader["ProfilePic"]),
                        MsgStatus = Convert.ToBoolean(reader["Status"]),
                        BandName = Convert.ToString(reader["BandName"]),
                        DisplayDate = Convert.ToString(reader["Date"]),
                    });
                }
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public List<Ads> loadAdSnippets(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            List<Ads> lst = new List<Ads>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lst.Add(new Ads
                    {
                        AdId = Convert.ToInt64(reader["AdId"]),
                        Subject = Convert.ToString(reader["Subject"]),
                    });
                }
            }
            con.Close();
            reader.Close();
            return lst;
        }
        public MessageEntity getSendMsgattributes(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            MessageEntity msg = new MessageEntity();
            while (reader.Read())
            {
                msg.ProfilePic = Convert.ToString(reader["ProfilePic"]);
                msg.BandName = Convert.ToString(reader["BandName"]);
            }
            con.Close();
            reader.Close();
            return msg;
        }

        public SqlDataReader getReader(SqlConnection con, SqlCommand cmdPar)
        {
            con.Open();
            cmd = new SqlCommand();
            cmd = cmdPar;
            cmd.Connection = con;
            reader = cmd.ExecuteReader();
            return reader;
        }
        //public DataTable FillDataTable(SqlConnection con, string spName)
        //{
        //    //con.Open();
        //    //cmd = new SqlCommand();
        //    //cmd.Connection = con;
        //    //cmd.CommandType = CommandType.StoredProcedure;
        //    //cmd.CommandText = spName;
        //    //dt = new DataTable();
        //    //dt.Load(GetReader(con, spName));
        //    //adp = new SqlDataAdapter();
        //    //ds = new DataSet();
        //    //adp.SelectCommand = cmd;
        //    //adp.Fill(ds);
        //    //con.Close();
        //    //return ds;
        //}

    }
}
