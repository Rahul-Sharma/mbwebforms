﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace DAL
{
    public static class Redirect
    {

        public static string LogoutHome()
        {
            return "MBHome.aspx";
        }
        public static string GoToHome()
        {
            return "Default.aspx";
        }
        public static string GoToUserHome()
        {
            return "MeraBandHome.aspx";
        }
        public static string GoToEditProfilePage()
        {
            return "EditProfile.aspx";
        }
        public static string GoToCurrentProfilePage()
        {
            return "ProfileId.aspx";
        }
        public static string GoToUserProfilePage(Int64 accId)
        {
            return "ProfileView.aspx?Id="+accId;
        }
        public static string GoToLogin()
        {
            return "LoginSnippet.aspx";
        }
        public static string GotToLocationInterface()
        {
            return "SetLocationInterface.aspx";
        }
        public static string GoToLogout()
        {
            return "LogountInterface.aspx";
        }

    }
}
