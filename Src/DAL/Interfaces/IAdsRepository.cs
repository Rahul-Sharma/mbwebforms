﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
namespace DAL.Interfaces
{
    public interface IAdsRepository
    {
        Ads ViewAddByAccount(Int64 AccountId);
        List <Ads> ViewAddByLocation(string state, string city);
        Ads ViewAddByAdId(Int64 AdId);
        Int64 PostAdd(Ads Ad);
        List<Ads> ViewAdsByFilter(Ads Ad);
        List<string> LoadTags();
    }
}
