﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;

namespace DAL.Interfaces
{
    public interface IAccountRepository
    {
        void CreateAccount(Account account);
        Account GetAccountByEmail(string Email);
        bool CheckAccount(string Email);
    }
}
