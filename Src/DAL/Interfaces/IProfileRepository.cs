﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;

namespace DAL.Interfaces
{
    public interface IProfileRepository
    {
      
        Profile ShowProfile(Int64 accId);
        Profile UpdateProfile(Profile profile);
        int SetLocation(Profile objProfile);
    }
}
