﻿<%@ Page Title="Messages" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="Messages.aspx.cs" Inherits="Messages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Scripts/ajax.js"></script>
    <link href="Content/MsgBubble.css" rel="stylesheet" />
    <style type="text/css">
        #OuterShell {
            /*width: 400px;*/
            /*height: 64px;*/
            width: 100%;
            border: 1px;
            display: table;
        }

        #OuterMsgDiv {
            width: 750px;
            height: 500px;
            display: table-cell;
            float: left;
        }

        #OuterAdDiv {
            display: table-cell;
            float: left;
            height: auto;
            margin-left: 22px;
            width: 200px;
            text-align: center;
        }

        #ADHeader {
            width: 100%;
            display: table;
            height: 45px;
            text-decoration-color: white;
            text-align: center;
        }

        #AdListing {
            width: 90%;
            text-align: center;
            margin: 0 auto !important;
            padding-left: 5px;
            margin-left: 2%;
        }

        #AdTitles {
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            text-align: center;
            font-weight: bold;
            padding-top: 15px;
            height: 45px;
            font-size: larger;
            text-decoration-color: white;
            background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);
            color: white;
            height: 48px;
            width: 210px;
            text-decoration-color: white;
            text-align: center;
            margin-left: 4px;
        }

        #InnerMsgHeader12 {
            width: 100%;
            display: table;
            height: 50px;
            text-decoration-color: white;
            text-align: center;
        }

        #InnerMsgHolder {
            height: 400px;
            width: 100%;
            margin: 0 auto;
            text-align: center;
            color: white;
            background-color: white;
            padding-top: 10px;
            overflow-y: auto;
            margin-top: 5px;
            border-bottom-right-radius: 10px;
            border-bottom-left-radius: 10px;
            margin-top:15px;
        }

        #ReplyBox {
            width: 748px;
            margin: 0 auto;
            text-align: center;
            padding-top: 30px;
        }

        #Inbox12 {
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
            background: -moz-linear-gradient(#feda71, #febb4a);
            background: -o-linear-gradient(#feda71, #febb4a);
            background: linear-gradient(#feda71, #febb4a);
            width: 374px;
            border-top-left-radius: 10px;
            display: table-cell;
            text-align: center;
            font-weight: bold;
            float: left;
            padding-top: 15px;
            height: 50px;
            font-size: larger;
            text-decoration-color: white;
            color: white;
        }

            #Inbox12:hover {
                width: 374px;
            }

        #divInbox {
            /*background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);*/
            width: 374px;
            border-top-left-radius: 10px;
            display: table-cell;
            text-align: center;
            font-weight: bold;
            float: left;
            height: 50px;
            font-size: larger;
            text-decoration-color: white;
        }

        #divOutbox {
            transition: all 0.3s linear;
            /*background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);*/
            width: 374px;
            display: table-cell;
            text-align: center;
            font-weight: bold;
            float: left;
            height: 50px;
            border-top-right-radius: 10px;
            text-decoration-color: white;
        }

        #Sent12 {
            transition: all 0.3s linear;
            background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);
            width: 374px;
            display: table-cell;
            text-align: center;
            font-weight: bold;
            float: left;
            padding-top: 15px;
            font-size: larger;
            height: 50px;
            border-top-right-radius: 10px;
            text-decoration-color: white;
            color: white;
        }

            #Sent12:hover {
                background-image: linear-gradient(to top right, #ffffff 0%, #febb4a 100%);
                width: 374px;
            }

        #TxtIbocTAb {
            font-family: Verdana;
            font-size: large;
            padding-top: 5px;
        }

        .msgRow {
            width: 748px;
            margin-top: 7px;
            color: #2e88c4;
            padding: 0px;
            background: #2e88c4; /* Old browsers */
            background: -moz-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%, rgba(246, 242, 242, 0.95) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(232, 227, 227, 0.95)), color-stop(100%,rgba(246, 242, 242, 0.95))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='rgba(232, 227, 227, 0.95)', endColorstr='rgba(246, 242, 242, 0.95)',GradientType=0 ); /* IE6-9 */
            height: 70px;
        }

            .msgRow:hover {
                background: #2e88c4; /* Old browsers */
                background: -moz-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%, rgba(246, 242, 242, 0.95) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(232, 227, 227, 0.95)), color-stop(100%,rgba(246, 242, 242, 0.95))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* IE10+ */
                background: linear-gradient(to bottom, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='rgba(232, 227, 227, 0.95)', endColorstr='rgba(246, 242, 242, 0.95)',GradientType=0 ); /* IE6-9 */
            }

        #Msg {
            display: table;
            height: 60px;
        }

        #PicArea {
            display: table-cell;
            height: 60px;
            width: 22%;
            float: left;
        }

        #MsgShort {
            display: table-cell;
            height: 60px;
            float: left;
            width: 88%;
        }

        #MsgFooterleft {
            height: 10px;
            width: 22%;
            text-align: center;
            margin: 0 auto;
        }

        #MsgFooterRight {
            height: 10px;
            width: 88%;
            text-align: right;
        }
    </style>
    <style type="text/css">
        .ClickMsgs {
            width: 748px;
            text-align: center;
            /*width: 400px;
            height: 64px;*/
            margin: 0 auto;
            border: 1px;
            color: #ffffff;
            padding: 0px;
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c));
            background: -moz-linear-gradient(#AC822D, #91663c);
            background: -o-linear-gradient(#AC822D, #91663c);
            background: linear-gradient(#AC822D, #91663c);
           
            height: 73px;
            margin-top: 3px;
            margin-bottom: 3px;
        }

            .ClickMsgs :hover {
                
                 opacity: .6;
            }



        #MsgTop {
            display: table;
            /*width: 400;
            height: 48px;*/
            width: 748px;
            height: 51px;
        }

        #PicAreaMsg {
            display: table-cell;
            float: left;
            width: 150px;
            /*height: 48px;*/
            padding-top: 3px;
            height: 51px;
            text-align: center;
            vertical-align: middle;
        }

        #MsgContent {
            display: table-cell;
            float: left;
            /*height: 48px;*/
            height: 52px;
            text-align: left;
            width: 598px;
            padding-top: 8px;
        }

        #OuterMsgFooter {
            width: 748px;
            height: 20px;
            display: table;
        }

            #OuterMsgFooter:hover {
            }

        #BandNameMsg {
            display: table-cell;
            float: left;
            width: 150px;
            text-align: center;
            padding-bottom: 5px;
            height: 18px;
            color: #ffffff;
        }



        #DateMsg {
            display: table-cell;
            float: left;
            width: 598px;
            text-align: right;
            height: 18px;
            padding-bottom: 5px;
            color: white;
        }

        .divAdSnippet {
            transition: all 0.3s linear;
            background-image: linear-gradient(to top right, #ffffff 0%, #ffffff 100%);
            color: #febb4a;
            text-align: left;
            font-weight: bold;
        }

            .divAdSnippet:hover {
                background-image: linear-gradient(to top right, #ffffff 0%, #febb4a 100%);
                color: #ffffff;
                text-align: left;
                font-weight: bold;
            }

        .objSelection {
            margin: 0 auto;
            /*background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
             background: -moz-linear-gradient(#feda71, #febb4a);
             background: -o-linear-gradient(#feda71, #febb4a);
             background: linear-gradient(#feda71, #febb4a);*/
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            padding: 3px;
            padding-left: 5px;
            text-align: left;
            padding-top: 3px;
        }

            .objSelection:hover {
                background: #ff6a00;
                margin: 0 auto;
                text-align: left;
                color: white;
                padding-top: 3px;
            }
    </style>
     <div style="width: 100%; display: table; height:800px; margin-top:90px;">
      <div style="display: table-row">
        <div id="sideNavigation" runat="server" style="width: 20%; float:left;  display: table-cell;">
         <div>
            <a href="Messages.aspx" class="a-btn">
                <span id="BtnMsgTxt" runat="server" class="a-btn-text">Messages</span>
                <span class="a-btn-slide-text">Sent/Recieved</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>
        <div >
            <a href="ProfileId.aspx" class="a-btn">
                <span class="a-btn-text">Profile</span>
                <span class="a-btn-slide-text">View/Update</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>
        <div >
            <a href="MyAds.aspx" class="a-btn">
                <span id="TT" runat="server" class="a-btn-text">My Ads</span>
                <span class="a-btn-slide-text">View/Post</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>

        </div> 
        
        <div style="width:80%; float:left; display: table-cell; height:800px;">
           <div id="OuterShell">
        <div id="OuterMsgDiv">
            <div id="InnerMsgHeader12">

                <div id="divInbox">
                    <a href="#" id="inxLnk" onclick="ShowInbox();" style="width: 374px; display: table-cell; float: left;">
                        <div id="Inbox12"><span class="TxtIbocTAb"><strong>Inbox</strong></span></div>
                    </a>
                </div>
                <div id="divOutbox">
                    <a href="#" onclick="ShowSent();" style="width: 374px; display: table-cell; float: left;">
                        <div id="Sent12"><span class="TxtSentTab">Sent</span></div>
                    </a>
                </div>
            </div>


            <div id="InnerMsgHolder">

                <img id="imgLoad" src="Content/img/loading.gif" alt="Loading Messages. Please wait!" style="display: none;" />
                <asp:Label ID="lblAdMsgs" runat="server" Text="" Style="display: none;"></asp:Label>
                <asp:Label ID="lblMsgsInbox" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblMsgThread" runat="server" Text="" Style="display: none;"></asp:Label>
                <asp:Label ID="lblSent" runat="server" Style="display: none;" Text=""></asp:Label>
                <asp:HiddenField ID="HndCId" runat="server" />
            </div>
            <div id="ReplyBox"></div>
            <div id="Responsediv" style="display: none;">
                <asp:TextBox ID="txtreply" Style="display: none; width: 748px;" TextMode="MultiLine" Rows="3" required="required" placeholder="Type in your Message" runat="server" autofocus="autofocus"></asp:TextBox>
                <asp:Button ID="btnMsgReply" OnClientClick="EnableReply();" Style="margin-top: 2px;" runat="server" Text="Send Reply" OnClick="btnMsgReply_Click" />
                <asp:Button ID="btnReply" runat="server" Text="Send" OnClick="btnReply_Click" Style="display: none; margin-top: 4px;" />
            </div>
        </div>
        <div id="OuterAdDiv">
            <div id="ADHeader">
                <div id="AdTitles">View Messages by Ads</div>
            </div>
            <div id="AdListing">
                <asp:Label ID="lblAdSortLst" runat="server" Text="No Ads No Ad Msgs, Simple!"></asp:Label>
            </div>

        </div>
    </div>
            </div>
         </div>
       
</div>
  


    <script type="text/javascript">
        function EnableReply() {
            document.getElementById('<%=txtreply.ClientID%>').style.display = '';
            document.getElementById('<%=btnMsgReply.ClientID%>').style.display = 'none';
            document.getElementById('<%=btnReply.ClientID%>').style.display = '';
            return false;
        }

        function ShowInbox() {
            document.getElementById('<%=HndCId.ClientID%>').value = '';
            //So that function can't be called

            document.getElementById('Responsediv').style.display = 'none';
            document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = '';
            document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
            document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
            document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c))';
            document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#AC822D, #91663c)';
            document.getElementById('Inbox12').style.background = '--o-linear-gradient(#AC822D, #91663c)';
            document.getElementById('Inbox12').style.background = 'linear-gradient(#AC822D, #91663c)';
            document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
            document.getElementById('Sent12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
            document.getElementById('Sent12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
            document.getElementById('Sent12').style.background = 'linear-gradient(#feda71, #febb4a)';
        }
        function ShowSent() {
            document.getElementById('<%=HndCId.ClientID%>').value = '';
            //So that function can't be called

            document.getElementById('<%=lblSent.ClientID%>').style.display = '';
            document.getElementById('Responsediv').style.display = 'none';
            document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblSent.ClientID%>').style.display = '';
            document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
            document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c))';
            document.getElementById('Sent12').style.background = '-moz-linear-gradient(#AC822D, #91663c)';
            document.getElementById('Sent12').style.background = '--o-linear-gradient(#AC822D, #91663c)';
            document.getElementById('Sent12').style.background = 'linear-gradient(#AC822D, #91663c)';
            document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
            document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
            document.getElementById('Inbox12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
            document.getElementById('Inbox12').style.background = 'linear-gradient(#feda71, #febb4a)';
            document.getElementById('imgLoad').style.display = '';
            document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
            document.getElementById('Responsediv').style.display = 'none';
            MakeAjaxRequest("ajaxHandler.aspx?Cid=1&event=3");
            document.getElementById('imgLoad').style.display = 'none';
            if (ajaxOutput != "*") {
                document.getElementById('<%=lblSent.ClientID%>').innerHTML = ajaxOutput;
            }
            else {
                document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = '';
                document.getElementById('InnerMsgHolder').style.color = '#000000';
                document.getElementById('<%=lblSent.ClientID%>').innerHTML = 'No messages for this ad!';
            }
            document.getElementById('<%=lblSent.ClientID%>').style.display = '';
            document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
        }


        function OpenMsgThread(Cid) {
            document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
            document.getElementById('imgLoad').style.display = '';
            document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
            document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
            document.getElementById('Responsediv').style.display = '';
            if (Cid != -1 && Cid != '') {
                MakeAjaxRequest("ajaxHandler.aspx?Cid=" + Cid + "&event=1");
                if (ajaxOutput != "*") {
                    document.getElementById('imgLoad').style.display = 'none';
                    document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
                    document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
                    document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
                    document.getElementById('<%=lblMsgThread.ClientID%>').style.display = '';
                    document.getElementById('<%=btnMsgReply.ClientID%>').style.display = '';
                    document.getElementById('<%=lblMsgThread.ClientID%>').innerHTML = ajaxOutput;
                    document.getElementById('<%=HndCId.ClientID%>').value = Cid;
                }
                else {

                }
            }
            else {

            }
        }
        function ShowAdMsgs(entityId)
        {
            document.getElementById('<%=HndCId.ClientID%>').value = '';
            //So that function can't be called
            document.getElementById('imgLoad').style.display = '';
            document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
            document.getElementById('Responsediv').style.display = 'none';
            document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
            if (entityId != -1 && entityId != '') {
                MakeAjaxRequest("ajaxHandler.aspx?Cid=" + entityId + "&event=2");
                document.getElementById('imgLoad').style.display = 'none';
                if (ajaxOutput != "*") {
                    document.getElementById('<%=lblAdMsgs.ClientID%>').innerHTML = ajaxOutput;
                }
                else {
                    document.getElementById('InnerMsgHolder').style.color = '#000000';
                    document.getElementById('<%=lblAdMsgs.ClientID%>').innerHTML = 'No messages for this ad!';

                }
                document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
                document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = '';
                document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
                document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
                document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
                document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
                document.getElementById('Inbox12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
                document.getElementById('Inbox12').style.background = 'linear-gradient(#feda71, #febb4a)';
                document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
                document.getElementById('Sent12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
                document.getElementById('Sent12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
                document.getElementById('Sent12').style.background = 'linear-gradient(#feda71, #febb4a)';
            }
            else {

            }
        }

    </script>
    <script type="text/javascript">
        if (document.getElementById('<%= HndCId.ClientID%>').value != '')
            OpenMsgThread("<%=  HndCId.Value  %>");

        //For Inbox Highlighting
        document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c))';
        document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#AC822D, #91663c)';
        document.getElementById('Inbox12').style.background = '--o-linear-gradient(#AC822D, #91663c)';
        document.getElementById('Inbox12').style.background = 'linear-gradient(#AC822D, #91663c)';
    </script>


</asp:Content>

