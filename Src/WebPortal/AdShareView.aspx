﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="AdShareView.aspx.cs" Inherits="AdShareView" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="Content/AdStylesSearch.css" rel="stylesheet" />
    <link href="Content/AdsStyling.css" rel="stylesheet" />
    <style>
        .clickable {
            background: #111111;
            width: auto;
            vertical-align: middle;
            height: 24px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            float: left;
            padding: 3px;
            padding-right: 3px;
        }

        .clickable1 {
            width: auto;
            vertical-align: middle;
            height: 24px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            float: left;
            padding: 3px;
            padding-right: 3px;
        }


        .clickable:hover {
            background: #ff6a00;
            width: auto;
            vertical-align: middle;
            height: 24px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            float: left;
            color: white;
        }

        .clickableVerticalOutter {
            display: table;
            width: auto;
            margin-left: 2px;
            padding-left: 2px;
            margin-top: 4px;
        }

        .clickableVertical {
            background: #ff6a00;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table-cell;
            margin-left: 0px;
            float: left;
            margin-right: 2px;
        }

        .clickableVerticalSide {
            padding-left: 3px;
            background: #ffffff;
            color: black;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table-cell;
            float: left;
            margin-left: 0px;
            font-size: small;
        }

        .clickableVertical:hover {
            background: #111111;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            font-size: small;
        }


        .spacesY {
            width: 8px;
            text-align: left;
            color: white;
            height: 3px;
        }

        .spaces {
            width: 8px;
        }

        .TagsNew {
            color: white;
            vertical-align: middle;
            padding: 5px;
        }

        #TagCount {
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            margin-left: 0px;
        }

            #TagCount:hover {
                color: white;
            }

        #InsideShare {
            text-align: center;
            margin: 0 auto;
        }

        #ShareAdView {
            padding-top: 80px;
            text-align: center;
            margin: 0 auto;
        }
    </style>
    <div id="ShareAdView">

        <div id="InsideShare">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="AdId" OnItemDataBound="DataList1_ItemDataBound" Width="20%" OnDataBinding="DataList1_DataBinding" CellPadding="0" OnItemCommand="DataList1_ItemCommand">
                <HeaderTemplate>
                </HeaderTemplate>

                <ItemTemplate>
                    <div class="AdBoundary">
                        <div class="AdHeaderSubject">
                            <asp:Label ID="sub" runat="server" Text='<%# Eval("Subject") %>'></asp:Label></div>
                        <div class="PicDescriptionOutter">
                            <div class="PicDescPicArea">
                                <div class="PicDescPicAreaRighAlign">
                                    <asp:Image ID="Image7" runat="server" ImageUrl='<%#Eval("PicPath","Content/profilepic/{0}") %>' Width="45px" Height="45px" />
                                </div>
                                <br />
                                <a href="ProfileView.aspx?Id=<%# Eval("AccountId") %>" title="View Profile"><strong>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Label></strong></a>
                            </div>
                            <div class="PicDescDescArea">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                            </div>
                            <div class="PicDescShareArea">
                                <div class="ShareAreaRightAlign">
                                    <asp:HyperLink ID="fb" runat="server" Tilte="Share on Facebook" Target="_blank">
                                        <asp:Image ID="ImgFB" class="imgForShare" ImageUrl="~/Content/img/Facebook_icon.png" runat="server" Width="35px" Height="35px" /></asp:HyperLink>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HyperLink ID="tw" runat="server" Title="Share on Twitter" Target="_blank">
                                        <asp:Image ID="ImgTW" class="imgForShare" ImageUrl="~/Content/img/twitterlogo.png" runat="server" Width="35px" Height="35px" /></asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="PostButtonDiv">
                            <asp:Button ID="Button2" class="PostbtnStyle" runat="server" CommandName="PostMsg" CommandArgument='<%# Eval("AccountId") %>' Text="Post Reply" Style="font-weight: 500; font-size: x-large" />
                        </div>
                        <div class="TagDateOutter">
                            <div class="TagHorizontalList">
                                <asp:Label ID="TagsToAdd" runat="server" Text='<%# Eval("AdIdStr") %>'></asp:Label>
                            </div>
                            <div class="DateHorizontal">
                                <div class="DateRightAlign">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="color: black;" />
                </ItemTemplate>

                <FooterTemplate>
                </FooterTemplate>
            </asp:DataList>
        </div>
    </div>
    <asp:Button ID="btnDirectToAds" runat="server" Style="display: none;" Text="Button" OnClick="btnDirectToAds_Click" />
    <script type="text/javascript">
        function RDToPublicSearch() {
            __doPostBack('<%=btnDirectToAds.UniqueID%>', '');
        }
    </script>
</asp:Content>

