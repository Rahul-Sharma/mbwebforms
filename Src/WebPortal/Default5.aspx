﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default5.aspx.cs" Inherits="Default5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .AdBoundary {
            width: 700px;
            height: 245px;
            margin: 0 auto;
        }

        .AdHeaderSubject {
            width: 100%;
            font-size: x-large;
            color: #ff6a00;
            padding: 2px;
            height: auto;
        }

        .PicDescriptionOutter {
            width: 100%;
            height: 107px;
            display: table;
        }

        .PicDescPicArea {
            width: 20%;
            padding-top: 10px;
            float: left;
            display: table-cell;
        }

        .PicDescDescArea {
            width: 70%;
            padding-top: 10px;
            padding-bottom:3px;
            float: left;
            display: table-cell;

        }

        .PicDescShareArea {
            width: 10%;
             padding-top: 10px;
            float: left;
            display: table-cell;
        }
         .ShareAreaRightAlign {
            float: right;
        }
        .PostButtonDiv {
            width: 100%;
            margin: 0 auto;
            height: auto;
            text-align: center;
        }

        .TagDateOutter {
            width: 100%;
            height: auto;
            display: table;
        }

        .TagHorizontalList {
            width: 78%;
            float: left;
            display: table-cell;
        }
        .DateRightAlign {
            float: right;
        }
        .DateHorizontal {
            width: 22%;
            float: left;
            display: table-cell;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="AdBoundary">
            <div class="AdHeaderSubject"> <asp:Label ID="sub" runat="server" Text='<%# Eval("Subject") %>'></asp:Label></div>
            <div class="PicDescriptionOutter">
                <div class="PicDescPicArea">
                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Content/profilepic/02.jpg" Width="45px" Height="45px" /><br />
                   <a href="ProfileView.aspx?Id=<%# Eval("AccountId") %>"  title="View Profile"><strong><asp:Label ID="Label4" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Label></strong></a></div>
                <div class="PicDescDescArea">
                  <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                </div>
                <div class="PicDescShareArea"> 
                    <div class="ShareAreaRightAlign"> <asp:HyperLink ID="fb" runat="server" Tilte="Share on Facebook" Target="_blank"> <asp:Image ID="ImgFB" class="imgForShare" ImageUrl="~/Content/img/Facebook_icon.png" runat="server" Width="35px" Height="35px"/></asp:HyperLink>
                  <br /> <asp:HyperLink ID="tw" runat="server" Title="Share on Twitter" Target="_blank"> <asp:Image ID="ImgTW" class="imgForShare" ImageUrl="~/Content/img/twitterlogo.png" runat="server" Width="35px" Height="35px"/></asp:HyperLink>
               </div>
                    </div>
            </div>
            <div class="PostButtonDiv">
                <asp:Button ID="Button2" class="PostbtnStyle" runat="server" CommandName="PostMsg" CommandArgument='<%# Eval("AccountId") %>' Text="Post Reply" Style="font-weight: 500; font-size: x-large" />
                  </div>
            <div class="TagDateOutter">
                <div class="TagHorizontalList">
                    <asp:Label ID="TagsToAdd" runat="server" Text='<%# Eval("AdIdStr") %>'></asp:Label>
                        </div>
                <div class="DateHorizontal">
                    <div class="DateRightAlign">
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label></div>
                </div>
            </div>
        </div>



    </form>
</body>
</html>
<script type="text/javascript">
    function showRun() {
        alert('Hi');
    }

</script>
