﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BLL;
using DAL;
using DAL.Repositories;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class TestTry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToLogin());
        }
        if (!IsPostBack)
        {
            checkIfNameExist();
            loadTags();
            loadLocation();
            loadObjectives();
        }
    }

    private void checkIfNameExist()
    {

    }

    private void loadObjectives()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadObjectives();
        ddlObjective.DataSource = lst;
        ddlObjective.DataTextField = "PostObjective";
        ddlObjective.DataValueField = "ObjId";
        ddlObjective.DataBind();
    }

    private void loadLocation()
    {
        ProfileRepository objProRepo = new ProfileRepository();
        string[] locArr = Convert.ToString(Session[clsSession.Location]).Split(',');
        yourLocation.InnerText = Convert.ToString(locArr[2]) + ", " + Convert.ToString(locArr[0]);
    }

    //private void loadTags()
    //{
    //    AdsRepository adsObj = new AdsRepository();
    //    List<string> lst = new List<string>();
    //    lst = adsObj.LoadTags();
    //    int length = lst.Count;
    //    StringBuilder sb = new StringBuilder();
    //    int count = 0;
    //    foreach (string tag in lst)
    //    {
    //        // sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
    //        sb.Append(" <div id=\"Tags\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a><a id=\"" + tag.ToString() + "1\"  style=\"display:none;\" onclick=\"removetag(this.id)\" href=\"#\"><img src=\"Content/CloseTag.png\" alt=\"Remove this Tag\" /> </a> </div>&nbsp;&nbsp;");
    //        sb.Append("<div class=\"spaces\">&nbsp; </div>");
    //        count++;
    //    }
    //    TagAdd.InnerHtml += "    " + sb.ToString();
    //}
    private void loadTags()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadTagsIds();
        StringBuilder sb = new StringBuilder();
        int count = 1;
        foreach (Ads tag in lst)
        {
            sb.Append(" <div id=\"Tags\"  class=\"clickable\" float=\"left\"> <a id=\"" + tag.TagId + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div>");
            count++;
        }
        TagsToAdd.Text += "    " + sb.ToString();
    }
    protected void Button_Click(object sender, EventArgs e)
    {
        //Validate Ad Credentials
        if (!Regex.IsMatch(title.Value.Trim(), @"^[a-z,:()'!-_ A-Z./ 0-9 ]{1,100}$") || !Regex.IsMatch(description.Value.Trim(), @"^[a-z,:()'!-_ A-Z./ 0-9 ]{1,500}$"))
        {
            hdnValue.Value = "";
            Response.Write("<script>alert('Invalid Operation. Only Alphanumeric and some character allowed (./,-_)!')</script>");
            return;
        }
        if (!Regex.IsMatch(hdnValue.Value.Trim(), @"^[a-zA-Z-0-9]{1,500}$"))
        {
            hdnValue.Value = "";
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        AdsRepository objRepos = new AdsRepository();
        Ads objAds = new Ads();
        objAds.AccountId = Convert.ToInt64(Session[clsSession.AccId]);
        objAds.Subject = Convert.ToString(title.Value);
        objAds.Description = description.Value;
        objAds.ObjId = Convert.ToInt32(ddlObjective.SelectedValue);
        string[] arrLoc = Convert.ToString(Session[clsSession.LocationIds]).Split(',');
        objAds.Country = arrLoc[0];
        objAds.State = arrLoc[1];
        objAds.City = arrLoc[2];
        String[] tagsArray = hdnValue.Value.Split('-');
        string tags = "";
        for (int i = 0; i < tagsArray.Length; i++)
        {
            if (tagsArray[i] != "s")
            {
                if (tags == "")
                    tags += tagsArray[i];
                else
                    tags += "," + tagsArray[i];
            }
        }
        objAds.Tags = tags;
        string[] tagsArr = tags.Split(',');
        Int64 inserted = objRepos.PostAdd(objAds);

        // Tags to be inserted into Tags
        for (int i = 0; i < tagsArr.Length; i++)
        {
            objRepos.InsertTag(inserted, Convert.ToInt32(tagsArr[i]));
        }

        if (inserted > 0)
        {
            Response.Redirect("Confirmation.aspx?confirmId=1"); // 1 for Ad Post Success!
            //Response.Write("<script>alert('Ad Posted Succesfully!')</script>");
           // Direct to a Post Success Page!
        }
        else
        {
            Response.Write("<script>alert('Sorry, an internal error occured!')</script>");
            return;
        }
    }
}