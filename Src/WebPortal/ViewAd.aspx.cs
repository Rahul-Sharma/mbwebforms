﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.Repositories;
using DAL;
using BLL;
using System.Text;
using System.Text.RegularExpressions;

public partial class ViewAd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Convert.ToString(Session[clsSession.AccId]) != "")
        if (!IsPostBack)
        {
            loadAds();
            loadTags();
            LoadCountries();
            LoadObjectives();
            hdnLocationFilter.Value = "All";
            hdnObjectiveFilter.Value = "All";
            hdnTagfilter.Value = "All";
            //lblType1.Text = " All";
            //lblGenre1.Text = " All";
            //lblLocation1.Text = " All";
            if (!IsPostBack)
            {
                lblDefaultFilter.Visible = true;
                lblDefaultFilter.Text = "<h2>Filters:  <b>Type</b>: All, <b>Genre</b>: All, <b>Location</b>: All</h2>";
                //lblfilterDesc.Text = "<u><h2>Ad Type: " + hdnObjectiveFilter.Value + ", Genere: " + hdnTagfilter.Value + " and Location: " + hdnLocationFilter.Value + "</h2></u>";
            }
        }
    }

    private void LoadObjectives()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.SearchObjectives();
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        foreach (Ads Obj in lst)
        {
            sb.Append("<a href=\"#\" class=\"objSelection\"><div id=\" " + Obj.ObjId + "\"  onclick=\"objectiveFilter(this.id)\">" + Obj.SearchObjective + "</div></a>");
            //sb.Append(" <div style=\"display:table-cell; background-color: gray; height: 2px; width: 100%;\"></div>");
        }
        lblOjectives.Text += "    " + sb.ToString();
    }
    private void LoadCountries()
    {
        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
        lst = adRepo.LoadCountryListForAdSearch();
        ddlCountry.DataSource = lst;
        ddlCountry.DataTextField = "Country";
        ddlCountry.DataValueField = "CountryCode";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select Country---", "-1"));
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
        lst = adRepo.LoadStatesForSelectedCountry(Convert.ToInt32(ddlCountry.SelectedValue));
        ddlState.Items.Clear();
        ddlState.DataSource = lst;
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "StateCode";
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("--Select State---", "-1"));
    }
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
        lst = adRepo.LoadCitiesForSelectedState(Convert.ToInt32(ddlState.SelectedValue));
        ddlCity.Items.Clear();
        ddlCity.DataSource = lst;
        ddlCity.DataTextField = "City";
        ddlCity.DataValueField = "CityCode";
        ddlCity.DataBind();
        ddlCity.Items.Insert(0, new ListItem("--Select City---", "-1"));
    }
    private void loadAds()
    {
        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
        lst = adRepo.ViewAdsForPublicUser();
        DataList1.DataSource = lst;
        DataList1.DataBind();
        lbldefault.Visible = false;
        HyperLink1.Visible = false;

    }
    private void loadTags()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadSearchTagsDefault();
        StringBuilder sb = new StringBuilder();
        sb.Append("<span style=\"text-align:left; float:left; font-size:large;\">Genres!</span><br/><br/>");
        foreach (Ads tag in lst)
        {
            sb.Append(" <div class=\"clickableVerticalOutter\"><div id=\"Tags\" class=\"clickableVertical\" > <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div><div class=\"clickableVerticalSide\">" + "x" + tag.TagCount + "</div></div>");
            sb.Append("<div class=\"spacesY\">&nbsp; </div>");// sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            //sb.Append(" <div id=\"TagsVer"+tag.TagId+"" class=\"clickableVertical\"> <a id=\"" + tag.TagId + "\" href=\"Ads.aspx?tagCloud="+tag.TagId+"\" class=\"TagsNew\"> " + tag.TagName + "</a></div>&nbsp;&nbsp;");
            //sb.Append("<div class=\"spaces\">&nbsp; </div>");
        }
        if (sb.ToString() != "")
            lblTagVertical.Text = sb.ToString();
        else
            lblTagVertical.Text = "No Tags Here";
    }
    private void loadTagsByRegion()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadSearchTagsByRegion(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue));
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        int count = 0;
        sb.Append("<span style=\"text-align:left; float:left; font-size:large;\">Tagged!</span><br/><br/>");
        foreach (Ads tag in lst)
        {
            sb.Append(" <div class=\"clickableVerticalOutter\"><div id=\"Tags\" class=\"clickableVertical\" > <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div><div class=\"clickableVerticalSide\">" + "x" + tag.TagCount + "</div></div>");
            sb.Append("<div class=\"spacesY\">&nbsp; </div>");
            // sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            //sb.Append(" <div id=\"TagsVer"+tag.TagId+"" class=\"clickableVertical\"> <a id=\"" + tag.TagId + "\" href=\"Ads.aspx?tagCloud="+tag.TagId+"\" class=\"TagsNew\"> " + tag.TagName + "</a></div>&nbsp;&nbsp;");
            //sb.Append("<div class=\"spaces\">&nbsp; </div>");
            count++;
        }
        if (sb.ToString() != "")
            lblTagVertical.Text = sb.ToString();
        else
            lblTagVertical.Text = "No Tags Here";
    }
    private void loadTagsByRegionAndObjective()
    {
        if (!Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadSearchTagsByRegionAndObj(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(HdnObjId.Value));
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        int count = 0;
        sb.Append("<span style=\"text-align:left; float:left; font-size:large;\">Tagged!</span><br/><br/>");
        foreach (Ads tag in lst)
        {
            sb.Append(" <div class=\"clickableVerticalOutter\"><div id=\"Tags\" class=\"clickableVertical\" > <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div><div class=\"clickableVerticalSide\">" + "x" + tag.TagCount + "</div></div>");
            sb.Append("<div class=\"spacesY\">&nbsp; </div>");
            // sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            //sb.Append(" <div id=\"TagsVer"+tag.TagId+"" class=\"clickableVertical\"> <a id=\"" + tag.TagId + "\" href=\"Ads.aspx?tagCloud="+tag.TagId+"\" class=\"TagsNew\"> " + tag.TagName + "</a></div>&nbsp;&nbsp;");
            //sb.Append("<div class=\"spaces\">&nbsp; </div>");
            count++;
        }
        if (sb.ToString() != "")
            lblTagVertical.Text = sb.ToString();
        else
            lblTagVertical.Text = "No Tags Here";
    }
    private void loadTagsByObjective()
    {
        if (!Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadSearchTagsByObj(Convert.ToInt32(HdnObjId.Value));
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        int count = 0;
        sb.Append("<span style=\"text-align:left; float:left; font-size:large;\">Tagged!</span><br/><br/>");
        foreach (Ads tag in lst)
        {
            sb.Append(" <div class=\"clickableVerticalOutter\"><div id=\"Tags\" class=\"clickableVertical\" > <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div><div class=\"clickableVerticalSide\">" + "x" + tag.TagCount + "</div></div>");
            sb.Append("<div class=\"spacesY\">&nbsp; </div>");
            // sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            //sb.Append(" <div id=\"TagsVer"+tag.TagId+"" class=\"clickableVertical\"> <a id=\"" + tag.TagId + "\" href=\"Ads.aspx?tagCloud="+tag.TagId+"\" class=\"TagsNew\"> " + tag.TagName + "</a></div>&nbsp;&nbsp;");
            //sb.Append("<div class=\"spaces\">&nbsp; </div>");
            count++;
        }
        if (sb.ToString() != "")
            lblTagVertical.Text = sb.ToString();
        else
            lblTagVertical.Text = "No Tags Here";
    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        Label TagsToAdd = (Label)e.Item.FindControl("TagsToAdd");
        Label LBLSub = (Label)e.Item.FindControl("sub");
        Label LBLDescription = (Label)e.Item.FindControl("Label1");
        HyperLink LnkFB = (HyperLink)e.Item.FindControl("fb");
        HyperLink LnkTW = (HyperLink)e.Item.FindControl("tw");
        Button btnPostReply = (Button)e.Item.FindControl("Button2");
        Label lblNoMsg = (Label)e.Item.FindControl("Nomsg");
        string sub = "";
        Image DefaultPic = (Image)e.Item.FindControl("Image7");
        if (DefaultPic != null)
        {
            if (DefaultPic.ImageUrl.Contains("facebook"))
            {
                DefaultPic.ImageUrl = DefaultPic.ImageUrl.Substring(19);
            }
        }

        if (TagsToAdd != null)
        {
            if (LnkFB != null)
            {
                string title = "";
                string url;
                string dashtitle;
                sub = LBLSub.Text;
                if (LnkFB != null)
                {
                    //Facebook Share Open Graph Meta tags
                    title = LBLSub.Text;
                    dashtitle = title.Replace(' ', '*');
                    url = "http://www.meraband.com/adshareview.aspx?ShareEntityId=" + TagsToAdd.Text + "-" + dashtitle;
                    string summery = LBLDescription.Text;
                    string image = "http://www.meraband.com/content/img/img2.jpg";
                    string facebooklink = "http://www.facebook.com/sharer.php?s=100&p[title]=" + title;
                    facebooklink += "&p[summary]=" + summery;
                    facebooklink += "&p[url]=" + url;
                    facebooklink += "&p[images][0]=" + image;
                    LnkFB.NavigateUrl = facebooklink;
                    //Twitter Share Open Graph Meta tags
                    if (LnkTW != null)
                    {
                        string twitterlink = "http://www.twitter.com/share?url=";
                        twitterlink += url;
                        twitterlink += "&text=" + sub;
                        twitterlink += "&via=Meraband";
                        LnkTW.NavigateUrl = twitterlink;
                    }
                }
            }
            if (btnPostReply != null && lblNoMsg != null)
            {
                if (Convert.ToString(btnPostReply.CommandArgument).Trim() == Convert.ToString(Session[clsSession.AccId]))
                {
                    btnPostReply.Visible = false;
                    lblNoMsg.Text = "It's Your Ad!";
                }
                else
                {
                    btnPostReply.Visible = true;
                    lblNoMsg.Text = "";
                }
            }
            Int64 tagId = Convert.ToInt64(TagsToAdd.Text);
            AdsRepository adsObj = new AdsRepository();
            List<Ads> lst = new List<Ads>();
            lst = adsObj.LoadTagsIdsByAd(tagId);
            int length = lst.Count;
            StringBuilder sb = new StringBuilder();
            foreach (Ads tag in lst)
            {
                sb.Append("<div id=\"TagsNewS\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div>");
                sb.Append("<div class=\"clickable1\" float=\"left\">&nbsp;</div><div class=\"spaces\"> </div><div></div>");
            }
            TagsToAdd.Text = sb.ToString();
        }
    }
    protected void DataList1_DataBinding(object sender, EventArgs e)
    {

    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "PostMsg")
        {
            Int64 RecipeintId = Convert.ToInt64(e.CommandArgument);
            Int64 AdId = Convert.ToInt64(DataList1.DataKeys[e.Item.ItemIndex]);
            int MsgType = 1; // For Musician Ads
            // string UrlToRedirect = "SendMessage.aspx?EntityId=" + AdId + "&User=" + RecipeintId + "&CType=" + MsgType;
            if (Convert.ToString(Session[clsSession.AccId]) == "")
            {
                Response.Redirect(Redirect.GoToLogin());
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}', 'mywin', 'style=position:fixed; left=25, top=25, width=560, height=420, toolbar=no, scrollbars=yes');</script>", "SendMessage.aspx?EntityId=" + AdId + "&User=" + RecipeintId + "&CType=" + MsgType));
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Setting Location Filter

        if (ddlCountry.SelectedValue != "-1" && ddlState.SelectedValue != "-1" && ddlCity.SelectedValue != "-1")
        {
            lblDefaultFilter.Visible = true;
            lblDefaultFilter.Text = "Filters:- ";
            hdnLocationFilter.Value = ddlCity.SelectedItem.Text + ", " + ddlCountry.SelectedItem.Text;

            lblLocation1.Text = " <div class=\"filterDivs\">" + hdnLocationFilter.Value + "<a id=\"lblLocation\"  onclick=\"removeFilter(this.id)\" href=\"#\">&nbsp;&nbsp;<img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove Location\" /> </a> </div>";

            //hdnLocationFilter.Value = ddlCity.SelectedItem.Text + " ," + ddlCountry.SelectedItem.Text;
            LoadAdsByFilters();
            loadTagsByRegion();
        }
        else
        {
            Response.Write("<script>alert('You must select Country>> State and City');</script>");
            return;
        }
    }
    protected void LoadAdsByFilters()
    {
        try
        {
            if (!Regex.IsMatch(hdnTagfilter.Value.Trim(), @"^[a-z&;A-Z 0-9 ]{1,100}$") || !Regex.IsMatch(hdnLocationFilter.Value.Trim(), @"^[a-z, A-Z 0-9 ]{1,100}$") || !Regex.IsMatch(hdnObjectiveFilter.Value.Trim(), @"^[a-zA-Z() 0-9 ]{1,100}$"))
            {
                Response.Write("<script>alert('Invalid Operation!')</script>");
                return;
            }

            if (hdnTagfilter.Value != "All" && hdnLocationFilter.Value != "All" && hdnObjectiveFilter.Value != "All")
            {
                //All parameters apply
                showAdsByFilters();
            }
            else if (hdnTagfilter.Value == "All" && hdnLocationFilter.Value != "All" && hdnObjectiveFilter.Value != "All")
            {
                // Location and Objective applies
                showAdsByLocationAndObjective();
                loadTagsByRegionAndObjective();
            }
            else if (hdnTagfilter.Value != "All" && hdnLocationFilter.Value == "All" && hdnObjectiveFilter.Value != "All")
            {
                //Tag and Objective applies
                showAdsByTagAndObjective();
            }
            else if (hdnTagfilter.Value != "All" && hdnLocationFilter.Value == "All" && hdnObjectiveFilter.Value == "All")
            {
                //Only Tag Applies
                showAdsByTags();  // Show Ads top 20 only
            }
            else if (hdnTagfilter.Value != "All" && hdnLocationFilter.Value != "All" && hdnObjectiveFilter.Value == "All")
            {
                //Tag And Location Applies
                showAdsByLocationAndTags();  // Show Ads top 20 only
            }
            else if (hdnTagfilter.Value == "All" && hdnLocationFilter.Value == "All" && hdnObjectiveFilter.Value != "All")
            {
                //Only Objective Applies
                showAdsByObjective();  // Show Ads top 20 only
                loadTagsByObjective();
            }
            else if (hdnTagfilter.Value == "All" && hdnLocationFilter.Value != "All" && hdnObjectiveFilter.Value == "All")
            {
                //Only Location Applies
                showAdsByLocation();  // Show Ads top 20 only
            }
            else if (hdnTagfilter.Value == "All" && hdnLocationFilter.Value == "All" && hdnObjectiveFilter.Value == "All")
            {
                //Show All Ads
                loadAds();
                loadTags();
            }
            else
            {
                //Response.Write("<script>alert('Invalid Operation! Please reload the page!');</script>')</script>");
                string msg1 = "<script>alert('Invalid Operation! Please reload the page!');</script>";
                this.RegisterClientScriptBlock("msg",msg1);

            }
            // lblfilterDesc.Text = "<u>Filters</u>: <b>Type: </b>" + hdnObjectiveFilter.Value + " , <b>Genere: </b>" + hdnTagfilter.Value + "  <b>, Location: </b>" + hdnLocationFilter.Value;
        }
        catch (Exception ex)
        {
            Response.Redirect("Error.aspx?Id=" + ex.Message);
        }
    }

    private void showAdsByLocation()
    {
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByLocation(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }

    private void showAdsByObjective()
    {
        if (!Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByObjective(Convert.ToInt32(HdnObjId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }

    private void showAdsByTags()
    {
        if (!Regex.IsMatch(HdnTagId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByTag(Convert.ToInt32(HdnTagId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }
    protected void btnAdsByOjective_Click(object sender, EventArgs e)
    {
        lblDefaultFilter.Visible = true;
        lblDefaultFilter.Text = "Filters:- ";
        lblType1.Text = " <div class=\"filterDivs\">" + hdnObjectiveFilter.Value + "<a id=\"lblType\"  onclick=\"removeFilter(this.id)\" href=\"#\">&nbsp;&nbsp;<img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove Location\" /> </a> </div>";
        LoadAdsByFilters();
    }

    private void showAdsByTagAndObjective()
    {
        if (!Regex.IsMatch(HdnTagId.Value.Trim(), @"^[0-9]{1,4}$") || !Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByTagAndObjId(Convert.ToInt32(HdnTagId.Value), Convert.ToInt32(HdnObjId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }

    private void showAdsByLocationAndObjective()
    {
        if (!Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByLocAndObj(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(HdnObjId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }
    private void showAdsByLocationAndTags()
    {
        if (!Regex.IsMatch(HdnTagId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByLocAndTag(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(HdnTagId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }

    private void showAdsByFilters()
    {
        if (!Regex.IsMatch(HdnObjId.Value.Trim(), @"^[0-9]{1,4}$") || !Regex.IsMatch(HdnTagId.Value.Trim(), @"^[0-9]{1,4}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        List<Ads> lstAds = new List<Ads>();
        AdsRepository repo = new AdsRepository();
        lstAds = repo.getAdsByAllFilters(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(ddlState.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(HdnTagId.Value), Convert.ToInt32(HdnObjId.Value));
        if (lstAds.Count > 0)
        {
            DataList1.DataSource = lstAds;
            DataList1.DataBind();
            lbldefault.Visible = false;
            HyperLink1.Visible = false;


        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }
    }
    protected void btnAdsByTag_Click(object sender, EventArgs e)
    {
        lblDefaultFilter.Visible = true;
        lblDefaultFilter.Text = "Filters:- ";
        lblGenre1.Text = " <div class=\"filterDivs\">" + hdnTagfilter.Value + "<a id=\"lblGenre\"  onclick=\"removeFilter(this.id)\" href=\"#\">&nbsp;&nbsp;<img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove Location\" /> </a> </div>";
        LoadAdsByFilters();
    }
    private void showDefaultMsg()
    {
        string str = "<div id=\"DefaultMsg\"> " +
            "Sorry, there are no ads for these filters." +
            " <br />" +
            "Reset filter and search again with different combination" +
            "<br />" +
            "<br />" +
            "OR" +
            "<br /> " +
            "<br />" +
            "You can be the first person to post your requirement. <br/>" +
            "</div>";
        lbldefault.Visible = true;
        HyperLink1.Visible = true;
        lbldefault.Text = str.ToString();


    }
    protected void btnFilterRemove_Click(object sender, EventArgs e)
    {
        //Validate type of Hdn Filed
        string id = FilterId.Value;
        lblDefaultFilter.Visible = true;
        lblDefaultFilter.Text = "Filters:- ";
        if (id == "lblType")
        {
            //Refreshing/Loading Tags
            if (hdnLocationFilter.Value != "All")
            {
                loadTagsByRegion();
            }
            else
            {
                loadTags();
            }
            loadTags();
            lblType1.Text = " <div class=\"filterDivs\"> Type: All </a> </div>";
            hdnObjectiveFilter.Value = "All";
        }
        if (id == "lblGenre")
        {
            lblGenre1.Text = "<div class=\"filterDivs\"> Genre: All </a> </div>";
            hdnTagfilter.Value = "All";
        }
        if (id == "lblLocation")
        {
            lblLocation1.Text = " <div class=\"filterDivs\"> Location: All </a> </div>";
            hdnLocationFilter.Value = "All";
        }
        LoadAdsByFilters();
    }
}