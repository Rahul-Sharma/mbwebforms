﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BLL;
using DAL;
using DAL.Repositories;
using System.Data;
using System.Data.SqlClient;

public partial class Messages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
            Response.Redirect(Redirect.GoToHome());
        LoadIbox();
        LoadMyAdSnippets();
    }

    private void LoadIbox()
    {
        try
        {
            MessagesRepository msgObj = new MessagesRepository();
            List<MessageEntity> lstObj = new List<MessageEntity>();
            lstObj = msgObj.LoadInbox(Convert.ToInt64(Session[clsSession.AccId]));
            StringBuilder sb = new StringBuilder();
            foreach (MessageEntity msgAttr in lstObj)
            {

                sb.Append(" <a href=\"#\" id=\"" + msgAttr.ConversationId + "\" class=\"ClickMsgs\" onclick=\"return OpenMsgThread(this.id);\">");
                sb.Append("<div id=\"OuterShell\">");
                sb.Append("<div id=\"MsgTop\">");
                sb.Append("<div id=\"PicAreaMsg\">");
                if (msgAttr.User1 == Convert.ToInt64(Session[clsSession.AccId]))
                {
                    if (msgAttr.ProfilePicUser2.Trim().Contains("facebook"))
                        sb.Append("<img src=\"" + msgAttr.ProfilePicUser2 + "\" alt=\"" + msgAttr.UserName2 + "\"  Width=\"45px\" Height=\"45px\" />");
                    else
                        sb.Append("<img src=\"Content/profilepic/" + msgAttr.ProfilePicUser2 + "\" alt=\"" + msgAttr.UserName2 + "\"  Width=\"45px\" Height=\"45px\" />");
                }
                else
                {
                    if (msgAttr.ProfilePicUser2.Trim().Contains("facebook"))
                        sb.Append("<img src=\"" + msgAttr.ProfilePicUser1 + "\" alt=\"" + msgAttr.UserName1 + "\"  Width=\"45px\" Height=\"45px\" />");
                    else
                        sb.Append("<img src=\"Content/profilepic/" + msgAttr.ProfilePicUser1 + "\" alt=\"" + msgAttr.UserName1 + "\"  Width=\"45px\" Height=\"45px\" />");
                }
                // sb.Append("<asp:Image ID=\"Image1\" runat=\"server\" ImageUrl=\"~/Content/profilepic/"+msgAttr.ProfilePic+"\" Width=\"45px\" Height=\"45px\" />");
                sb.Append("</div>");
                sb.Append("<div id=\"MsgContent\">");
                sb.Append(msgAttr.MsgBody.Substring(0, 3) + "..........");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<div id=\"OuterMsgFooter\">");
                sb.Append("<div id=\"BandNameMsg\">");
                if (msgAttr.User1 == Convert.ToInt64(Session[clsSession.AccId]))
                {

                    sb.Append(msgAttr.UserName2.Length >= 12 ? msgAttr.UserName2.Substring(0, 12) : msgAttr.UserName2);
                }
                else
                {
                    sb.Append(msgAttr.UserName1.Length >= 12 ? msgAttr.UserName1.Substring(0, 12) : msgAttr.UserName1);
                }
                sb.Append("</div>");
                sb.Append("<div id=\"DateMsg\">");
                sb.Append(msgAttr.DisplayDate);
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</a>");

            }
            lblMsgsInbox.Text = sb.ToString();
        }
        catch (Exception ex)
        {
            Response.Redirect("Error.aspx?Id=" + ex.Message + " LoadMsg");
        }
    }
    private void LoadMyAdSnippets()
    {
        try
        {

            MessagesRepository msgObj = new MessagesRepository();
            List<Ads> lstObj = new List<Ads>();
            lstObj = msgObj.LoadAdsSnippets(Convert.ToInt64(Session[clsSession.AccId]));

            StringBuilder sb = new StringBuilder();
            foreach (Ads msgAttr in lstObj)
            {
                sb.Append("<div id=\" " + msgAttr.AdId + "\" class=\"objSelection\" onclick=\"ShowAdMsgs(this.id)\">" + msgAttr.Subject + "</div>");
                sb.Append("<hr style=\"color:black;\"/>");
            }
            lblAdSortLst.Text = sb.ToString();
        }
        catch (Exception ex)
        {
            Response.Redirect("Error.aspx?Id=" + ex.Message + " Load AdSnippet");
        }

    }
    protected void btnMsgReply_Click(object sender, EventArgs e)
    {

    }
    protected void btnReply_Click(object sender, EventArgs e)
    {
        // Validate Values///
        MessagesRepository msgrepo = new MessagesRepository();
        MessageEntity msg = new MessageEntity();
        msg.ConversationId = Convert.ToInt64(HndCId.Value);
        msg.SentByAccountId = Convert.ToInt64(Session[clsSession.AccId]);
        msg.MsgStatus = false; //Unread
        msg.MsgType = 1; // For Musician Ads
        msg.IpAddress = Convert.ToString(Request.ServerVariables["REMOTE_ADDR"]);
        msg.MsgBody = txtreply.Text.Trim();
        int inserted = msgrepo.SendReplyByConId(msg);
        if (inserted == -1)
        {
            //string script = "<script type=\"text/javascript\"> OpenMsgThread("+HndCId.Value+"); </script>";
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "myscript", script);
        }
    }

}