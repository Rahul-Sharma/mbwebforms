﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

public partial class LogountInterface : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Security.RemoveCache();
        Session.Abandon();
        Response.Redirect(Redirect.GoToHome());
    }
}