﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Confirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Request.QueryString["confirmId"]) == "1")
            lblConfirmMsg.Text = "<span style=\"font-size:xx-large; color:orange;\">Your Ad has been posted successfully! </span><br> <span style=\"font-size:large; color:black;\">Relax, we'll notify you as soon as somebody responds. <br>For maximum traction, we'll also post it to our respective Facebook and Twitter Channels (after content screening).</span>";
    }
}