﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using DAL.Repositories;

public partial class AdShareView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.GetPostBackEventReference(this, string.Empty);
        //Validating Input Values ie Id should alwyas be int64
        if (Request.QueryString["ShareEntityId"] != null)
        {
            string[] urlVal = Convert.ToString(Request.QueryString["ShareEntityId"]).Split('-');
            AdsRepository repObj = new AdsRepository();
            List<Ads> lst = new List<Ads>();
            lst = repObj.getAdsByAdId(Convert.ToInt64(urlVal[0]));
            DataList1.DataSource = lst;
            DataList1.DataBind();
        }
        else
        {

        }
    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label TagsToAdd = (Label)e.Item.FindControl("TagsToAdd");
        Label LBLSub = (Label)e.Item.FindControl("sub");
        Label LBLDescription = (Label)e.Item.FindControl("Label1");
        HyperLink LnkFB = (HyperLink)e.Item.FindControl("fb");
        HyperLink LnkTW = (HyperLink)e.Item.FindControl("tw");
        string sub = "";
        Image DefaultPic = (Image)e.Item.FindControl("Image1");
        Image proPic = (Image)e.Item.FindControl("Image7");
        if (proPic != null)
        {
            if (proPic.ImageUrl.Trim().Contains("facebook"))
                proPic.ImageUrl = proPic.ImageUrl.Trim().Substring(19);
        }
        if (DefaultPic != null)
        {
            DefaultPic.ImageUrl = "~/Content/MBandLogo.png";
        }

        if (TagsToAdd != null)
        {
            if (LnkFB != null && LnkTW != null)
            {
                string title = "";
                string url;
                string dashtitle;
                sub = LBLSub.Text;
                if (LnkFB != null)
                {
                    //Facebook Share Open Graph Meta tags
                    title = LBLSub.Text;
                    dashtitle = title.Replace(' ', '*');
                    url = "http://meraband.com/adshareview.aspx?ShareEntityId=" + TagsToAdd.Text + "-" + dashtitle;
                    string summery = LBLDescription.Text;
                    string image = "http://meraband.com/content/img/img2.jpg";
                    string facebooklink = "http://www.facebook.com/sharer.php?s=100&p[title]=" + title;
                    facebooklink += "&p[summary]=" + summery;
                    facebooklink += "&p[url]=" + url;
                    facebooklink += "&p[images][0]=" + image;
                    LnkFB.NavigateUrl = facebooklink;
                    //Twitter Share Open Graph Meta tags
                    string twitterlink = "http://twitter.com/share?url=";
                    twitterlink += url;
                    twitterlink += "&text=" + sub;
                    twitterlink += "&via=Meraband";
                    LnkTW.NavigateUrl = twitterlink;
                }
            }

            Int64 tagId = Convert.ToInt64(TagsToAdd.Text);
            AdsRepository adsObj = new AdsRepository();
            List<Ads> lst = new List<Ads>();
            lst = adsObj.LoadTagsIdsByAd(tagId);
            int length = lst.Count;
            StringBuilder sb = new StringBuilder();

            foreach (Ads tag in lst)
            {
                sb.Append("<div id=\"TagsNewS\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.TagId + "\" onclick=\"RDToPublicSearch();\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div>");
                sb.Append("<div id=\"TagsNewS\" class=\"clickable1\" float=\"left\">&nbsp;</div><div class=\"spaces\"> </div><div></div>");
            }
            TagsToAdd.Text = sb.ToString();
        }
    }
    protected void DataList1_DataBinding(object sender, EventArgs e)
    {

    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Response.Redirect(Redirect.GoToLogin());
        }
        else
        {
            if (e.CommandName == "PostMsg")
            {
                Int64 RecipeintId = Convert.ToInt64(e.CommandArgument);
                Int64 AdId = Convert.ToInt64(DataList1.DataKeys[e.Item.ItemIndex]);
                int MsgType = 1; // For Musician Ads
                ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}', 'mywin', 'style=position:fixed; left=25, top=25, width=560, height=420, toolbar=no, scrollbars=yes');</script>", "SendMessage.aspx?EntityId=" + AdId + "&User=" + RecipeintId + "&CType=" + MsgType));
            }
        }
    }
    protected void btnDirectToAds_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewAd.aspx");
    }
}