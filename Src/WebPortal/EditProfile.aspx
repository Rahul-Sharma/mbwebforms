﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="EditProfile.aspx.cs" Inherits="EditProfile" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="Content/Buttons.css" rel="stylesheet" />
    <style>
        #OuuterdivEdit {
            width: 900px;
            height: 1320px;
            margin-top: 90px;
            padding: 2px;
            margin: 0 auto;
            padding-top: 80px;
        }

        #EditHeader {
            width: 100%;
            padding-top: 10px;
            text-align: center;
            font-size: xx-large;
            text-underline-position: below;
            text-decoration: underline;
            margin-top: 3px;
        }

        #DivOutterDiv {
            width: 100%;
        }

        #DivPhotoUpload {
            display: table;
            width: 100%;
            margin: 0 auto;
        }

        #DivPhoto {
            padding-top: 12px;
            display: table-cell;
            float: left;
            width: 21%;
            vertical-align: bottom;
            padding: 4px;
            margin-top: 7px;
            margin-left: 4px;
        }

        #DivphotoPhoto {
            float: right;
            margin-right: 0px;
        }

        #DivUpload {
            display: table-cell;
            float: left;
            width: 77%;
            padding: 4px;
        }

        #divUploadHeader {
            width: 100%;
            float: left;
        }

        #divUploadControl {
            width: 100%;
            float: left;
        }

        #youtubediv {
            width: 100%;
            height: 210px;
            margin-top: 7px;
            display: table;
        }

        #youtubeHeader {
            width: 150px;
            float: left;
            text-align: center;
            border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            background-color: black;
            color: white;
            height: 35px;
            font-size: large;
            padding-top: 4px;
            display: table-cell;
        }

        #youtubeLeftHelp {
            float: left;
            display: table-cell;
        }

        #youtubecontent {
            width: 100%;
            height: auto;
            float: left;
            border: thin;
            border-color: black;
            border-width: 6px;
            border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            padding: 4px;
            margin-bottom: 4px;
        }

        #soundclouddiv {
            width: 100%;
            height: 210px;
            margin: 4px;
            margin-top: 7px;
            display: table;
        }

        #soundcloudHeader {
            width: 150px;
            float: left;
            display: table-cell;
            text-align: center;
            border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            background-color: black;
            color: white;
            height: 35px;
            font-size: large;
            padding-top: 4px;
        }

        #soundcloudLefthelp {
            float: left;
            display: table-cell;
        }

        #soundcloudContent {
            width: 100%;
            height: auto;
            float: left;
            border: thin;
            border-color: black;
            border-width: 6px;
            border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            padding: 4px;
            margin-bottom: 4px;
        }

        #TagsDiv {
            width: 100%;
            height: 210px;
            margin-top: 7px;
            display: table;
        }

        #TagHeader {
            width: 150px;
            float: left;
            display: table-cell;
            text-align: center;
            border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            background-color: black;
            color: white;
            height: 35px;
            font-size: large;
            padding-top: 4px;
        }

        #TagHelp {
            display: table;
            float: left;
            padding-left: 4px;
        }

        #TagContent {
            width: 100%;
            height: auto;
            float: left;
            border: thin;
            border-color: black;
            border-width: 6px;
            border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            padding: 0px;
            margin-bottom: 0px;
            padding-top: 5px;
            margin-left: 0px;
        }

        #divTopNamePic {
            width: 100%;
            display: table;
            margin: 0 auto;
            margin-top: 15px;
            margin-bottom: 15px;
        }

        #divTopLeft {
            width: 49%;
            display: table-cell;
            float: left;
        }

        #divTopRight {
            width: 48%;
            display: table-cell;
            float: left;
            margin-right: 5px;
        }

        #divTopLeftInner {
            float: right;
            width: 100%;
            padding-right: 4px;
        }

        #divTopRightInner {
            float: left;
            width: 100%;
            padding-left: 4px;
        }

        #SocialOutter {
            width: 100%;
            padding: 8px;
        }

        #Social {
            width: 150px;
            float: left;
            display: table-cell;
            text-align: center;
            border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            background-color: black;
            color: white;
            height: 35px;
            font-size: large;
            padding-top: 4px;
        }

        #SocialHelp {
            padding-left: 5px;
            float: left;
            display: table-cell;
        }

        #SocialContent {
            width: 100%;
            height: auto;
            float: left;
            border: thin;
            font-size: large;
            border-color: black;
            border-width: 6px;
            border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            padding: 4px;
            margin-bottom: 4px;
        }

        #FbLnk {
            width: 90%;
            padding-top: 5px;
        }

        #TwitterLnk {
            width: 90%;
            padding-top: 5px;
        }

        #Names {
            margin: 0 auto;
            padding-top: 10px;
            text-align: center;
        }

        .txtAreaEdit {
            width: 100%;
        }

        #UpdateProfileDiv {
            width: 100%;
            padding-top: 10px;
            text-align: center;
            font-size: xx-large;
            text-underline-position: below;
            text-decoration: underline;
            margin-top: 3px;
        }

        #DescriptionDiv {
            width: 100%;
            height: 210px;
            margin-top: 7px;
            display: table;
        }

        #DescriptionDivHeader {
            width: 150px;
            float: left;
            text-align: center;
            border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            background-color: black;
            color: white;
            height: 35px;
            font-size: large;
            padding-top: 4px;
            display: table-cell;
        }

        #DescriptionDivHelp {
            float: left;
            display: table-cell;
        }

        #DescriptionDivContent {
            width: 100%;
            height: auto;
            float: left;
            border: thin;
            border-color: black;
            border-width: 6px;
            border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
            -moz-border-radius-topright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            padding: 4px;
            margin-bottom: 4px;
        }
    </style>
    <style>
        .clickable {
            background: #ff6a00;
            width: auto;
            vertical-align: middle;
            height: 28px;
            margin: 4px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            float: left;
        }

            .clickable:hover {
                background: #111111;
                width: auto;
                vertical-align: middle;
                height: 28px;
                margin: 4px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table;
                float: left;
            }

        .TagsClose {
        }

        .spaces {
            width: 8px;
            display: table;
            float: left;
        }

        .TagsNew {
            text-align: center;
            color: white;
            vertical-align: middle;
            padding: 5px;
        }

        .filterDivs {
            width: auto;
            display: table;
            background: Grey;
            color: white;
            width: auto;
            vertical-align: middle;
            height: auto;
            float: left;
            text-align: center;
            padding: 4px;
            opacity: .6;
            margin-bottom: 2px;
            float: right;
            margin: 2px;
        }

        .filterDivsSpace {
            width: 2px;
            margin: 2px;
            float: right;
        }
    </style>
    <script type="text/javascript">
        var tags = '';
        var count = 0;

        function removeFilter(tagId) {
            var tagAdd;
            var elem = document.getElementById('filter' + String(tagId));
            elem.parentNode.removeChild(elem);
            //Removing element
            var arr = String(document.getElementById('<%=hdnValue.ClientID %>').value).split('-', 50);
            for (var i = 0; i < arr.length; i++) {
                alert(arr[i]);
                if (arr[i] == String(tagId)) {
                    arr[i] = 's';
                }
            }
            document.getElementById('<%=hdnValue.ClientID %>').value = '';
            for (var i = 0; i < arr.length; i++) {
                if (document.getElementById('<%=hdnValue.ClientID %>').value == '')
                    document.getElementById('<%=hdnValue.ClientID %>').value = arr[i];
                else
                    document.getElementById('<%=hdnValue.ClientID %>').value += '-' + arr[i];
            }
            document.getElementById('<%=HdnCount.ClientID %>').value = parseInt(document.getElementById('<%=HdnCount.ClientID %>').value) - 1;
        }

        function Show(atag) {
            count = document.getElementById('<%=HdnCount.ClientID %>').value;
            if (parseInt(count) > 4) {
                alert('You can not add more than 5 tags!');
                return false;
            }
            var arr = String(document.getElementById('<%=hdnValue.ClientID %>').value).split('-', 50);
            for (var i = 0; i < arr.length; i++)
            {
                if (arr[i] == String(atag)) {
                    alert('You already have this tag! Remove any tag to add new one!');
                    return false;
                }
            }
            var tagName = document.getElementById(atag).innerHTML;
            document.getElementById('<%=lblAddedTags.ClientID %>').innerHTML += "<div  id=\"filter" + atag + "\" class=\"filterDivs\">" + tagName + "<a id=\"" + atag + "\"  onclick=\"removeFilter(this.id)\" href=\"#\"><img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove \" /> </a> </div>";
            tags = document.getElementById('<%=hdnValue.ClientID %>').value;
            if (tags == '')
                tags += String(atag);
            else
                tags += '-' + String(atag);
            document.getElementById('<%=hdnValue.ClientID %>').value = tags;
            count = parseInt(count) + 1;
            document.getElementById('<%=HdnCount.ClientID %>').value = count;
        }

        function removetag(atag) {
            var removeid = String(atag).substring(0, String(atag).length - 1);
            document.getElementById(String(removeid)).style.background = '#111111';
            document.getElementById(atag).style.display = 'none';
            tags = tags.replace(String(removeid), 's');
            document.getElementById('<%=hdnValue.ClientID %>').value = tags;
            count = parseInt(count) - 1;
        }

        function EnableSoundCloudFrame() {
            if (document.getElementById('<%=chkSoundCloud.ClientID %>').checked == true) {

                document.getElementById('<%=lblSoundFrame.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtsoundcloudframe.ClientID%>').style.display = '';
            }
            else {
                document.getElementById('<%=lblSoundFrame.ClientID %>').style.display = '';
                document.getElementById('<%=txtsoundcloudframe.ClientID%>').style.display = 'none';
            }
        }

        function EnableYoutubeFrame() {
            if (document.getElementById('<%=chkYoutube.ClientID %>').checked == true) {

                document.getElementById('<%=lblYoutubeFrame.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtYoutubeFrame.ClientID%>').style.display = '';
            }
            else {

                document.getElementById('<%=lblYoutubeFrame.ClientID %>').style.display = '';
                document.getElementById('<%=txtYoutubeFrame.ClientID%>').style.display = 'none';
            }
        }
    </script>
    <asp:HiddenField ID="HdnCount" runat="server" />
    <asp:HiddenField ID="HdnType" runat="server" />
    <asp:HiddenField ID="HdnContainer" runat="server" />
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="hdnFunc" runat="server" />
    <div id="OuuterdivEdit">

        <div id="demoProfileHeader" style="padding-top: 2px; float: right; width: 100%;">
            <asp:HyperLink ID="HyperDemoProfile" Target="_blank" NavigateUrl="DemoProfilePage.aspx" runat="server">(Click Here To See A Demo Profile!)</asp:HyperLink>
        </div>

        <div id="EditHeader">
            Edit Your Profile 
        </div>

        <div id="Names">
            <asp:Label ID="Name" runat="server" Text="Artist/Band/Fan Name: "></asp:Label>
            <input runat="server" id="title" title="title" type="text" placeholder="Edit Your Name" required="required" autofocus="autofocus" />
        </div>

        <div id="divTopNamePic">
            <div id="divTopLeft">
                <div id="divTopLeftInner">
                    <asp:Image ID="picDefault" Style="float: right;" ImageUrl="~/Content/profilepic/04.jpg" Width="90px" Height="90px" runat="server" />

                </div>
            </div>
            <div id="divTopRight">
                <div id="divTopRightInner">
                    <asp:FileUpload ID="picUpload" runat="server" /><br />
                    Upload Picture(JPG only! max-size: 100kb)
                </div>
            </div>

        </div>
        <hr />
        <div id="TagsDiv">
            <div id="TagHeader">
                Select a few tags
            </div>
            <div id="TagHelp">

                <asp:Label ID="lblAddedTags" Style="float: right;" Width="100%" runat="server" Text=""></asp:Label>
            </div>
            <div id="TagContent">
                <asp:Label ID="TagsToAdd" Width="100%" runat="server" Text=""></asp:Label>
            </div>
        </div>

        <div id="DescriptionDiv" runat="server">
            <div id="DescriptionDivHeader">
                About Yourself
            </div>
            <div id="DescriptionDivHelp">
            </div>
            <div id="DescriptionDivContent">


                <textarea runat="server" class="txtAreaEdit" id="txtDescription" title="Souncloud" rows="5" placeholder="Enter something about you...."></textarea>

                <%--<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/18181767"></iframe>
                --%>
            </div>
        </div>

        <div id="soundclouddiv" runat="server">
            <div id="soundcloudHeader">
                Sounds
            </div>
            <div id="soundcloudLefthelp">
                <asp:CheckBox ID="chkSoundCloud" runat="server" Text="Enter New" onchange="EnableSoundCloudFrame();" />
            </div>
            <div id="soundcloudContent">
                <asp:Label ID="lblSoundFrame" runat="server"></asp:Label>

                <textarea runat="server" class="txtAreaEdit" style="display: none;" id="txtsoundcloudframe" title="Souncloud" rows="5" placeholder="Embbed a Soundcloud link of your Music/Cover etc.(in form of <frame>...</frame>  Embbed Link)"></textarea>

                <%--<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/18181767"></iframe>
                --%>
            </div>
        </div>

        <div id="youtubediv">

            <div id="youtubeHeader">
                Latest Video
            </div>
            <div id="youtubeLeftHelp">
                <asp:CheckBox ID="chkYoutube" runat="server" Text="Enter New" onchange="EnableYoutubeFrame();" />

            </div>

            <div id="youtubecontent">

                <asp:Label ID="lblYoutubeFrame" runat="server"></asp:Label>
                <textarea runat="server" class="txtAreaEdit" style="display: none;" id="txtYoutubeFrame" title="Youtube" rows="5" placeholder="Embbed a Youtube link of your latest Music/Cover etc.(in form of <frame>...</frame> Embbed Link)"></textarea>

                <%--<iframe width="420" height="315" src="//www.youtube.com/embed/EKXMK8SyUFo" frameborder="0" allowfullscreen></iframe>
                --%>
            </div>

        </div>

        <div id="SocialOutter">

            <div id="Social">
                Social
            </div>
            <div id="SocialHelp">
                This will help you promote yourself!
            </div>
            <div id="SocialContent">
                <div id="FbLnk">
                    Facebook Page: https://facebook.com/
                    <input runat="server" id="txtFBId" title="title" type="text" placeholder="UserId Or UserName" autofocus="autofocus" />
                </div>
                <div id="TwitterLnk">
                    Twitter Page: https://twitter.com/
                    <input runat="server" id="txtTWId" title="title" type="text" placeholder="Handle" autofocus="autofocus" />
                </div>
            </div>
        </div>

        <div id="UpdateProfileDiv">
            <hr />
            <asp:Button ID="btnUpdateProfile" runat="server" Width="175px" class="btn" Text="Update & View Profile" OnClick="btnUpdateProfile_Click" />
        </div>


    </div>

</asp:Content>

