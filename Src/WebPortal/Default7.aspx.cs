﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default7 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataClass data = new DataClass();
            DataList1.DataSource = data.FirstTenRecords(333,167,2656,64);
            DataList1.DataBind();
        }

    }
}