﻿<%@ Page Title="My Profile" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="ProfileId.aspx.cs" Inherits="ProfileId" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="Content/CommonStyle.css" rel="stylesheet" />
      <link href="content/buttons.css" rel="stylesheet" />
    <style>
        #ProfileOutterMost {
            width: 890px;
            height: 990px;
           
            margin-left: 18%;
            background-color:white;
            margin-top:0px;
         
        }

        #ProHeader {
            width: 100%;
            height: auto;
        }

        #ProContentOutterMost {
            width: 100%;
            height: auto;
        }

        #ProTopContentTable {
            width: 100%;
            height: auto;
            display: table;
            height: 200px;
            z-index: 9999;
             background-image: url(../../Content/img/music-equalizer-background.jpg);

 
        }

        #ProContentPicArea {
            width: 18%;
            display: table-cell;
            float: left;
            padding: 4px;
            margin-top:4px;
        }

        #ProContentRight {
            width: 80%;
            display: table-cell;
            float: left;
            padding: 4px;
            vertical-align: bottom;
        }

        #ProMsgAndTags {
            width: 100%;
            display: table;
            margin-top: -9px;
             

        }

        #ProMsg {
            width: 18%;
            display: table-cell;
            float: left;
            padding-left: 0px;
            padding-top:0px;
            margin-left:3px;
        }

        .MsgBtn {
            transition: all 0.3s linear;
            background-color:black;
            color:white;
            margin: 0 auto;
            color: white;
            width: auto;
            vertical-align: middle;
            font-family: Verdana;
            height: 33px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
        }
        
        .MsgBtn:hover {
            transition: all 0.3s linear;
            background-color:darkgray;
            color:white;
            margin: 0 auto;
            color: white;
            width: auto;
            vertical-align: middle;
            font-family: Verdana;
            height: 33px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
        }


        #ProTags {
            width: 80%;
            display: table-cell;
            float: right;
            padding-right:4px;
        }

        #ContentTop {
            height: 67%;
            float:right;
            margin-top:5px;
        }

        #ContentBottom {
            height: 30%;
            font-size: xx-large;
            font-weight: 900;
            /*color: #ff6a00;*/
            color:white;
            font-family:'HP PSG';
            vertical-align: baseline;
            float: left;
            padding-top: 130px;
            padding-left:10px;
        }

        #description {
            width: 860px;
            height:60px;
        margin:4px;
        margin-top:10px;
        }
        #descriptionHeader {
         width:150px;
        float:left;
        text-align:center;
         border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
            background-color:black;
            color:white;
            height:35px;
            font-size:large;
           
            padding-top:4px;
        }
        #descriptionText {
            width:100%;
            height:auto;
            float:left;
            border: solid;
            border-color: black;
            border-width: 6px;
             border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
                         border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
              
            padding:4px;
            margin-bottom:4px;
        }
        #youtubediv {
        width:860px;
        height:210px;
        margin:4px;
 margin-top:7px;
               }
        #youtubeHeader {
        width:150px;
        float:left;
        text-align:center;
         border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
            background-color:black;
            color:white;
            height:35px;
            font-size:large;
           
            padding-top:4px;

        }
        #youtubecontent {
            width:100%;
            height:auto;
            float:left;
            border:solid ;
            border-color: black;
            border-width: 6px;
             border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
                         border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
              
            padding:4px;
            margin-bottom:4px;
        }
        #soundclouddiv {
          width:860px;
        height:210px;
        margin:4px;
 margin-top:7px;
        }
        #soundcloudHeader {
             width:150px;
        float:left;
        text-align:center;
         border-top-left-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -webkit-border-top-left-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
            background-color:black;
            color:white;
            height:35px;
            font-size:large;
           
            padding-top:4px;
        }
        #soundcloudContent {
             width:100%;
            height:auto;
            float:left;
            border: solid;
            border-color: black;
            border-width: 6px;
             border-bottom-left-radius: 5px;
            -moz-border-radius-bottomleft: 5px;
            -webkit-border-bottom-left-radius: 5px;
                         border-bottom-right-radius: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-bottom-right-radius: 5px;
            border-top-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-bottom-right-radius:5px;
              
            padding:4px;
            margin-bottom:4px;
        }
        .imgbor {
            border: solid;
            border-color: white;
            border-width: 6px;
              border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            margin-top:4px;
        }
         .filterDivs {
             width: auto;
             display: table;
             background: Black;
             color:white;
             width: auto;
             vertical-align: middle;
             height: auto;
             float: left;
             text-decoration-color: black;
             text-align: center;
             padding: 5px;
             opacity: .6;
             float:right;
             margin-top:6px;
            
             
         }
         .filterDivs:hover {
             width: 90px;
             display: table;
             background: Black;
             color:white;
            
             vertical-align: middle;
             height: auto;
             float: left;
             text-decoration-color: black;
             text-align: center;
             padding: 5px;
             opacity: .6;
             float:right;
             margin-top:6px;
         }
        .filterDivsSpace {
        width:2px;
        margin:2px;
        float:right;

        }
        #EnableEdit {
        padding-top:10px;
        position:fixed;
        width:95%;
        float:left;
        padding-left:10px;
        }
        #ShowEdit {
          padding-top:10px;
        position:fixed;
        width:95%;
        float:left;
        padding-left:10px;
        }

    </style>
    <script type="text/javascript"> 
        function HideEdit() {
            document.getElementById('EnableEdit').style.display = 'none';
            document.getElementById('ShowEdit').style.display = '';
            document.getElementById('<%=chk.ClientID %>').checked = false;
        }
        function ShowEdit() {
            document.getElementById('EnableEdit').style.display = '';
            document.getElementById('ShowEdit').style.display = 'none';
            document.getElementById('<%=chkEdit.ClientID %>').checked = false;
        }
    </script>
    <br />
    <br />
    <br />
     <div id="EnableEdit"><asp:Button ID="btnGotToEdit" class="btn"  runat="server" Text="Edit Profile" OnClick="btnGotToEdit_Click" />
         <asp:CheckBox ID="chk" runat="server" Text="Hide this" onclick="HideEdit();"/>
     </div>
   <div id="ShowEdit" style="display:none;"> 
          <asp:CheckBox ID="chkEdit" runat="server" Text="Show Edit" onclick="ShowEdit();"/>
       </div>
   
    <div id="ProfileOutterMost">
       
        <div id="ProHeader">
        </div>
        <div id="ProContentOutterMost">
            <div id="ProTopContentTable">
                <div id="ProContentPicArea">
                    <%-- Pic Goes Here--%>
                    <asp:Image ID="proImg" runat="server" CssClass="imgbor" Width="150px" Height="150px" ImageUrl="~/Content/profilepic/02.jpg" />
                </div>
                <div id="ProContentRight">
                    <div id="ContentTop">
                       
                        <asp:HyperLink ID="hyperFB" runat="server" Target="_blank"><img src="Content/img/fbIcon.png" alt="Facebook" /></asp:HyperLink>
                        <asp:HyperLink ID="hyperTwitter" runat="server" Target="_blank"><img src="Content/img/tweeticon.png" alt="Twitter" /></asp:HyperLink>
                    
                       
                    </div>
                    <div id="ContentBottom">
                        <asp:Label ID="lblBandName" runat="server" Text=""></asp:Label>

                    </div>

                </div>

            </div>
            <div id="ProMsgAndTags">
                <div id="ProMsg">
                    <asp:Button ID="btnSendMessage" Style="margin-top: 0px;" Width="150px" class="btn" runat="server" Text="Send Message!" />
                </div>
                <div id="ProTags">
                    <asp:Label ID="lblTags" runat="server" Text=""></asp:Label>
                </div>
        
            </div>
            <div id="description">
                <div id="descriptionHeader">
                     About
                </div>
                <div id="descriptionText">
                   <asp:Label ID="lblDescription" runat="server" ></asp:Label>
                </div>
               
            </div>
             <div id="soundclouddiv">
                <div id="soundcloudHeader">
                    Sounds
                </div>
                <div id="soundcloudContent">
                    <asp:Label ID="lblSoundcloudFrame" runat="server"></asp:Label>
                    <%--<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/18181767"></iframe>
   --%>             </div>
            </div>
            <div id="youtubediv">
                <div id="youtubeHeader">
                    Latest Video
                </div>
                <div id="youtubecontent">
                    <asp:Label ID="lblYoutubeFrame" runat="server"></asp:Label>
                    <%--<iframe width="420" height="315" src="//www.youtube.com/embed/EKXMK8SyUFo" frameborder="0" allowfullscreen></iframe>
                --%></div>

            </div>
           
        </div>
    </div>
</asp:Content>

