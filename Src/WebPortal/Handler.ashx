﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;

public class Handler : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string startQstring = context.Request.QueryString["start"];
        string nextQstring = context.Request.QueryString["next"];
        string accId = context.Request.QueryString["AccId"];
        string countryCode = context.Request.QueryString["countrycode"];
        string stateCode = context.Request.QueryString["stateCode"];
        string cityCode = context.Request.QueryString["cityCode"];
        //null check
        if ((!string.IsNullOrWhiteSpace(startQstring)) && (!string.IsNullOrWhiteSpace(nextQstring)))
        {
            //convert string to int
            int start = Convert.ToInt32(startQstring);
            int next = Convert.ToInt32(nextQstring);
            Int64 AccountId = Convert.ToInt64(accId);
            int CountryCode = Convert.ToInt32(countryCode);
            int StateCode = Convert.ToInt32(stateCode);
            int CityCode = Convert.ToInt32(cityCode);
            //setting content type
            context.Response.ContentType = "text/plain";
            DataClass data = new DataClass();
            //writing response
            context.Response.Write(data.GetAjaxContent(start, next, AccountId, CountryCode, StateCode, CityCode));
        }
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}
