﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendMessage.aspx.cs" Inherits="SendMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Send Message</title>
    <link href="Content/style.css" rel="stylesheet" />
    <style type="text/css">
        #OuterMsgGrid {
            width: 100%;
            text-align: center;
            float: none;
            padding: 2px;
        }
        #RecepientDiv {
            width: 100%;
            padding: 3px;
            height: 40px;
            vertical-align: central;
        }

        #Image1 {
            vertical-align: middle;
        }

        .PostbtnStyle {
            transition: all 0.3s linear;
            background-image: linear-gradient(to top right, #feda71 0%, #FDAD18 100%);
            margin: 0 auto;
            color: white;
            width: auto;
            vertical-align: middle;
            font-family: Verdana;
            height: 33px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
        }

        #btnSent {
            padding: 4px;
        }

        #MsgBodyDiv {
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        #TxtmsgBody {
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            height: 100%;
            width: 100%;
            font-size: large;

        }
        .classBk {
            background: #e8e3e3; /* Old browsers */
            background: -moz-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%, rgba(246, 242, 242, 0.95) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(232, 227, 227, 0.95)), color-stop(100%,rgba(246, 242, 242, 0.95))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(232, 227, 227, 0.95) 0%,rgba(246, 242, 242, 0.95) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='rgba(232, 227, 227, 0.95)', endColorstr='rgba(246, 242, 242, 0.95)',GradientType=0 ); /* IE6-9 */
        }  
  </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="classBk">
            <asp:Label ID="lblDefault" runat="server" Text="That was your very own ad. There's no point in messaging yourself" Visible="false"></asp:Label>
            <div id="OuterMsgGrid" runat="server">
                <div id="RecepientDiv">
                    To :
                    <asp:Image ID="imgRecipient" runat="server" Width="32px" Height="32px" />
                    &nbsp;
            <asp:HyperLink ID="hlnRecipient" runat="server"></asp:HyperLink>
                </div>
                <div id="MsgBodyDiv">
                    <asp:TextBox ID="txtMsgBody" class="TxtmsgBody" TextMode="MultiLine" Rows="8" Height="290px" Width="500px" runat="server"></asp:TextBox>

                </div>
                <div id="btnSent">
                    <asp:Button ID="btnSendMessage" class="PostbtnStyle" runat="server" Text="Send Message" OnClick="btnSendMessage_Click" />

                </div>
            </div>
        </div>
    </form>
</body>
</html>
