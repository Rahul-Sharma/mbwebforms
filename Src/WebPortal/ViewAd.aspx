﻿<%@ Page Title="Find Musicians" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="ViewAd.aspx.cs" Inherits="ViewAd" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="content/buttons.css" rel="stylesheet" />

    <link href="Content/AdsStylingpublic.css" rel="stylesheet" />
    <link href="Content/AdStylesSearch.css" rel="stylesheet" />

    <link href="Content/DropDownCSS.css" rel="stylesheet" />
    <link href="Content/css/Tags.css" rel="stylesheet" />
    <style type="text/css">
        /*
 *  STYLE 5
 */

     


       

        .clickable1 {
            width: auto;
            vertical-align: middle;
            height: 24px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            float: left;
            padding: 3px;
            padding-right: 3px;
        }


      

        .clickableVerticalOutter {
            display: table;
            width: auto;
            margin-left: 2px;
            padding-left: 2px;
            margin-top: 4px;
        }

        .clickableVertical {
            background: #ff6a00;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table-cell;
            margin-left: 0px;
            float: left;
            margin-right: 2px;
        }

        .clickableVerticalSide {
            padding-left: 3px;
            color: black;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table-cell;
            float: left;
            margin-left: 0px;
        }

        .clickableVertical:hover {
            background: #111111;
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
        }


        .spacesY {
            width: 8px;
            text-align: left;
            color: white;
            height: 3px;
        }

        .spaces {
            width: 8px;
        }

        .TagsNew {
            color: white;
            vertical-align: middle;
            padding: 5px;
        }

        #TagCount {
            width: auto;
            vertical-align: middle;
            height: 23px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
            display: table;
            margin-left: 0px;
        }

            #TagCount:hover {
                color: white;
            }

        .Title {
            width: 100%;
            font-size: x-large;
            color: #ff6a00;
            font-weight: 500;
            text-align: left;
            margin-bottom: 3px;
            text-anchor: middle;
            padding-top: 4px;
        }

        .headTitle {
            font-size: large;
            color: #808080;
            margin: 0 auto;
        }

        .adListDIV {
            margin: 0 auto;
            text-align: center;
        }

        .BandName {
            color: #2c2a2a;
            text-align: center;
            font-weight: 100;
            text-anchor: middle;
        }

        .auto-style5 {
            width: 60%;
            height: 400px;
            border-color: #808080;
            padding-right: 3px;
        }

        .tagsn {
            margin-top: 3px;
            margin-bottom: 0px;
            margin-left: 5px;
        }

        .imgForShare {
            width: 45px;
            height: 45px;
        }

        /*#adstag {
            border: 2px solid #808080;
            width: 80%;
            margin: 0 auto;
        }*/

        .Share {
            width: 10%;
            text-align: center;
        }

        .Bandpic {
            width: 20%;
            text-align: left;
        }

        .post {
            font-family: Verdana;
            font-weight: 400;
            text-align: center;
            vertical-align: top;
            width: 70%;
            color: #808080;
            /*height:200px;*/
        }

        .btnPost {
            text-align: center;
            margin-top: 3px;
        }

        .PostbtnStyle {
            transition: all 0.3s linear;
            /*background-image: linear-gradient(to top right, #111111 0%, #111111 100%);*/
            /*background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);*/
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
            background: -moz-linear-gradient(#feda71, #febb4a);
            background: -o-linear-gradient(#feda71, #febb4a);
            background: linear-gradient(#feda71, #febb4a);
            margin: 0 auto;
            color: white;
            width: auto;
            vertical-align: middle;
            font-family: Verdana;
            height: 33px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
        }

            .PostbtnStyle:hover {
                transition: all 0.3s linear;
                background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);
                width: auto;
                color: white;
                vertical-align: middle;
                font-family: Verdana;
                border-radius: 5px;
                height: 33px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
            }

        .AdList {
            color: #2c2a2a;
            text-align: center;
            font-weight: bold;
            height: 150px;
        }

        #MainBarFirst {
            width: 100%;
            text-align: left;
            padding-top: 4px;
            padding-left: 2px;
        }

        #MainBarSecond {
            width: 100%;
            display: table;
            padding-top: 4px;
            padding-left: 2px;
        }

        #MainBarSecondLeft {
            width: 75%;
            float: left;
            display: table-cell;
            text-align: left;
        }

        #MainBarSecondRight {
            width: 25%;
            float: left;
            display: table-cell;
            text-align: right;
        }

        #OuterMain {
            width: 100%;
            display: table;
        }

        #TagsByLoc {
            float: right;
            display: table-cell;
            width: 180px;
        }

        #TagBarTitle {
            width: 100%;
            text-align: left;
        }

        #TagsVertical {
            width: 100%;
            float: left;
            padding-top: 18px;
        }

        #lblTagVertical {
            float: left;
        }
    </style>
    <style type="text/css">
        #MainStag {
            margin: 0 auto;
            text-align: center;
            width: 100%;
        }

        .ddls {
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            height: 20px;
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
            background: -moz-linear-gradient(#feda71, #febb4a);
            background: -o-linear-gradient(#feda71, #febb4a);
            background: linear-gradient(#feda71, #febb4a);
        }

            .ddls li {
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                height: 20px;
                color: black;
                background-color: #feda71;
                background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
                background: -moz-linear-gradient(#feda71, #febb4a);
                background: -o-linear-gradient(#feda71, #febb4a);
                background: linear-gradient(#feda71, #febb4a);
            }

        #AdsOuterMain {
            width: 94%;
            height: auto;
            display: table;
            margin: 0 auto;
            margin-top: 80px;
        }

        #FirstObjectiveList {
            width: 21%;
            display: table-cell;
            float: left;
            padding-right: 50px;
            text-align: center;
        }

        #SectionFirst {
            width: 100%;
            text-align: left;
            height: 40px;
            margin: 0 auto;
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
            background: -moz-linear-gradient(#feda71, #febb4a);
            background: -o-linear-gradient(#feda71, #febb4a);
            background: linear-gradient(#feda71, #febb4a);
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            padding-right: 3px;
        }

        #Firstheader {
            width: 94%;
            padding-top: 4px;
            text-align: center;
            margin: 0 auto;
        }

        #SectionSecond {
            width: 75%;
            display: table-cell;
            float: left;
        }

        #AdHeaderDiv {
            width: 100%;
            height: 40px;
            text-align: left;
            margin: 0 auto;
            /*background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
             background: -moz-linear-gradient(#feda71, #febb4a);
             background: -o-linear-gradient(#feda71, #febb4a);
             background: linear-gradient(#feda71, #febb4a);*/
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            padding-left: 25px;
        }

        #AdContentDic {
            width: 100%;
            height: auto;
            display: table;
            margin: 0 auto;
            padding-top: 15px;
        }


        #FilterDesc {
            float: left;
            text-align: left;
            font-size: x-large;
        }




        #AdLstDiv {
            width: 76%;
            display: table-cell;
            float: left;
            height: auto;
            height: 900px;
            margin: 0 auto;
            overflow-y: auto;
            padding-left: 30px;
        }

            #AdLstDiv::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                box-shadow:inset 0 0 6px rgba(0,0,0,0.3);
                    -moz-box-shadow:inset 0 0 6px rgba(0,0,0,0.3);
                background-color: #F5F5F5;
            }

            #AdLstDiv::-webkit-scrollbar {
                width: 10px;
                background-color: #F5F5F5;
            }

            #AdLstDiv::-webkit-scrollbar-thumb {
                background-color: #F90;
                background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
           background-image:-moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
                 }


        #TagLstDiv {
            width: 20%;
            display: table-cell;
            float: right;
            height: auto;
            padding-left: 5px;
        }

        #SearchBarDiv {
            width: 100%;
            height: auto;
            padding: 5px;
            margin: 0 auto;
        }

        #CountryDiv {
            margin: 0 auto;
            width: 100%;
            padding: 4px;
        }

        #StateDiv {
            width: 100%;
            padding: 4px;
        }

        #CityDiv {
            width: 100%;
            padding: 4px;
        }

        #BtnSearchDiv {
            width: auto;
            padding: 4px;
        }

        #TitileDiv {
            width: 100%;
            color: black;
            text-align: left;
            padding-left: 7px;
            padding-top: 7px;
        }

        #SmartSearchTitle {
            width: 100%;
            color: #ffffff;
            text-align: center;
            padding: 2px;
            font-size: xx-large;
        }

        .ddl  {
            padding: 8px 7px;
            /* Styles */
            background: #fff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 0 1px 0 rgba(0,0,0,0.2);
            cursor: pointer;
            outline: none;
            border:0px;
           
            transition: all 0.3s ease-out;
                 }

       

        .objSelection {
            margin: 0 auto;
            /*background: -webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a));
             background: -moz-linear-gradient(#feda71, #febb4a);
             background: -o-linear-gradient(#feda71, #febb4a);
             background: linear-gradient(#feda71, #febb4a);*/
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            font-size: x-small;
            margin-top: 2px;
            margin-bottom: 2px;
            height: 20px;
            text-decoration: underline;
            width: 100%;
            padding-bottom: 8px;
            padding-top: 8px;
            color: black;
            text-align: center;
            text-overflow: clip;
            font-weight: bold;
        }

            .objSelection:hover {
                background: #ff6a00;
                color: white;
            }

        #DefaultMsg {
            text-align: center;
            font-size: large;
            padding: 45px;
        }

        .filterDivs {
            width: auto;
            display: table;
            background: #B6B6B4;
            vertical-align: middle;
            height: auto;
            float: left;
            text-decoration-color: black;
            text-align: center;
            padding: 3px;
            opacity: .4;
        }

        .txtspace {
            line-height: 16px;
            text-align:center;
        }

        .filterLbl {
            float: left;
        }
    </style>
    <script src="Scripts/Jquery1.9.js"></script>
    <script type="text/javascript">
        function objectiveFilter(id) {
            document.getElementById('<%=HdnObjId.ClientID%>').value = id;
            document.getElementById('<%=hdnObjectiveFilter.ClientID%>').value = document.getElementById(id).innerHTML;
            __doPostBack('<%=btnAdsByOjective.UniqueID%>', "");
        }
        function ShowAdbyTag(id) {
            document.getElementById('<%=HdnTagId.ClientID%>').value = id;
            document.getElementById('<%=hdnTagfilter.ClientID%>').value = document.getElementById(id).innerHTML;
            __doPostBack('<%=btnAdsByTag.UniqueID%>', "");
        }
        function removeFilter(id) {
            document.getElementById('<%=FilterId.ClientID%>').value = id;
            __doPostBack('<%=btnFilterRemove.UniqueID%>', "");
        }
        function dothis() {
            alert('hi');
        }
    </script>

    <asp:HiddenField ID="HdnObjId" runat="server" />
    <asp:HiddenField ID="hdnObjectiveFilter" runat="server" />

    <asp:HiddenField ID="FilterId" runat="server" />

    <asp:HiddenField ID="hdnLocationFilter" runat="server" />

    <asp:HiddenField ID="HdnTagId" runat="server" />
    <asp:HiddenField ID="hdnTagfilter" runat="server" />
    <asp:Button ID="btnAdsByOjective" runat="server" Text="Button" OnClick="btnAdsByOjective_Click" Style="display: none;" />
    <asp:Button ID="btnAdsByTag" runat="server" Text="Button" OnClick="btnAdsByTag_Click" Style="display: none;" />
    <asp:Button ID="btnFilterRemove" runat="server" Text="Button" Style="display: none;" OnClick="btnFilterRemove_Click" />
    <%-- Main Outer Div--%>
    <div id="AdsOuterMain">
        <%-- 1st Top Ad Header Div--%>

        <div id="FirstObjectiveList">
            <div id="SectionFirst">
                <div id="SmartSearchTitle">
                    Let's Search?
                
                </div>

            </div>
            <div id="SearchBarDiv">
                <div id="CountryDiv">
                    <asp:DropDownList class="ddl" ID="ddlCountry" runat="server" Width="200px" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" BackColor="Transparent">
                        <asp:ListItem Value="-1" Text="---Select Country"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div id="StateDiv">
                    <asp:DropDownList class="ddl" ID="ddlState" runat="server" Width="200px" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" BackColor="Transparent">
                        <asp:ListItem Value="-1" Text="---Select Country"></asp:ListItem>
                        <asp:ListItem Value="-1" Text="---Select State"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div id="CityDiv">
                    <asp:DropDownList ID="ddlCity" class="ddl" runat="server" Width="200px" AppendDataBoundItems="true" BackColor="Transparent">
                        <asp:ListItem Value="-1" Text="---Select Country"></asp:ListItem>
                        <asp:ListItem Value="-1" Text="---Select City"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div id="BtnSearchDiv">
                    <asp:Button ID="btnSearch" class="btn" runat="server" Text="Play Ads :)" Width="100px" Height="25px" OnClick="btnSearch_Click" />
                </div>
            </div>
            <div style="padding: 4px; font-size: xx-large; color: #ff6a00;">Choose Your Filter</div>
            <div id="Firstheader">
                <asp:Label ID="lblOjectives" runat="server" Text=""></asp:Label>
            </div>
        </div>


        <div id="SectionSecond">
            <div id="AdHeaderDiv">
                <div id="TitileDiv">

                    <div id="FilterDesc">
                        <asp:Label ID="lblDefaultFilter" class="filterLbl" Visible="false" runat="server"></asp:Label><asp:Label ID="lblType1" runat="server"></asp:Label><div style="float: left; display: table;">&nbsp;</div>
                        <asp:Label ID="lblGenre1" runat="server"></asp:Label><div style="float: left; display: table;">&nbsp;</div>
                        <asp:Label ID="lblLocation1" runat="server"></asp:Label>

                    </div>

                </div>


            </div>



            <div id="AdContentDic">

                <div id="AdLstDiv">
                    <p style="margin: 0 auto; text-align: center;">
                        <asp:Label ID="lbldefault" Visible="false" runat="server" Text=""></asp:Label>
                    </p>
                    <asp:DataList ID="DataList1" runat="server" DataKeyField="AdId" OnItemDataBound="DataList1_ItemDataBound" Width="70%" OnDataBinding="DataList1_DataBinding" CellPadding="0" OnItemCommand="DataList1_ItemCommand">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="AdBoundary">
                                <div class="AdHeaderSubject">
                                    <asp:Label ID="sub" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                                </div>
                                <div class="PicDescriptionOutter">
                                    <div class="PicDescPicArea">
                                        <div class="PicDescPicAreaRighAlign">
                                            <asp:Image ID="Image7" runat="server" ImageUrl='<%#Eval("PicPath","Content/profilepic/{0}") %>' Width="45px" Height="45px" />
                                        </div>
                                       
                                        <a href="ProfileView.aspx?Id=<%# Eval("AccountId") %>" title="View Profile"><strong>
                                            <asp:Label ID="Label4" class="txtspace" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Label></strong></a>
                                    </div>
                                    <div class="PicDescDescArea">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                    </div>
                                    <div class="PicDescShareArea">
                                        <div class="ShareAreaRightAlign">
                                            <asp:HyperLink ID="fb" runat="server" Tilte="Share on Facebook" Target="_blank" onclick="dothis();">
                                                <asp:Image ID="ImgFB" class="imgForShare" ImageUrl="~/Content/img/Facebook_icon.png" runat="server" Width="35px" Height="35px" />
                                            </asp:HyperLink>
                                            <br />
                                            <br />
                                            <br />
                                            <asp:HyperLink ID="tw" runat="server" Title="Share on Twitter" Target="_blank">
                                                <asp:Image ID="ImgTW" class="imgForShare" ImageUrl="~/Content/img/twitterlogo.png" runat="server" Width="35px" Height="35px" />
                                            </asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="PostButtonDiv">
                                    <asp:Label ID="Nomsg" runat="server"></asp:Label>
                                    <asp:Button ID="Button2" class="PostbtnStyle" runat="server" CommandName="PostMsg" CommandArgument='<%# Eval("AccountId") %>' Text="Post Reply" Style="font-weight: 300; font-size: large" />
                                </div>
                                <div class="TagDateOutter">
                                    <div class="TagHorizontalList">
                                        <asp:Label ID="TagsToAdd" runat="server" Text='<%# Eval("AdIdStr") %>'></asp:Label>
                                    </div>
                                    <div class="DateHorizontal">
                                        <div class="DateRightAlign">
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="background-color: gray; height: 2px; width: 100%;"></div>
                        </ItemTemplate>


                    </asp:DataList>
                    <asp:HyperLink ID="HyperLink1" Style="padding-left: 320px; font-size: larger;" Visible="false" runat="server" NavigateUrl="PostAd.aspx" Text="Post Free Ad!"></asp:HyperLink>
                </div>

                <div id="TagLstDiv">

                    <asp:Label ID="lblTagVertical" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

