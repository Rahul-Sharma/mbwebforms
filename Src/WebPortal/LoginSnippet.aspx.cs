﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using DAL.Repositories;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;


public partial class LoginSnippet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) != "")
            Response.Redirect(Redirect.GoToUserHome());
        if (!IsPostBack)
        {

            Random randomNum = new Random();
            Session[clsSession.Seed] = randomNum.Next().ToString();
            btnLogin.Attributes.Add("onclick", "javascript:return md5authLogin(" + Session[clsSession.Seed] + ");");
            btnRegister.Attributes.Add("onclick", "javascript:return md5authRegister();");
            //  btnReset.Attributes.Add("onclick", "javascript:return ValidateResetEmail();");
        }

    }
    protected string MySHA512(string passwd)
    {
        SHA512 sha512 = new System.Security.Cryptography.SHA512Managed();
        byte[] sha512Bytes = System.Text.Encoding.Default.GetBytes(passwd);
        byte[] cryString = sha512.ComputeHash(sha512Bytes);
        string sha512Str = string.Empty;
        for (int i = 0; i < cryString.Length; i++)
        {
            sha512Str += cryString[i].ToString("X");
        }
        return sha512Str;
    }

    private string generateMd5Hash(string pswd)
    {
        MD5CryptoServiceProvider encryptor = new MD5CryptoServiceProvider();
        UTF8Encoding encoder = new UTF8Encoding();

        byte[] encryptedValueBytes = encryptor.ComputeHash(encoder.GetBytes(pswd));
        StringBuilder encryptedValueBuilder = new StringBuilder();
        for (int i = 0; i < encryptedValueBytes.Length; i++)
        {
            encryptedValueBuilder.Append(encryptedValueBytes[i].ToString("x2"));
        }
        string encryptedValue = encryptedValueBuilder.ToString();
        return encryptedValue;
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            OuterLogin.Attributes.CssStyle.Add("display", "");
            OuterRegister.Attributes.CssStyle.Add("display", "none");
            //Perform Validations
            if (txtLoginPassword.Text == "" || txtLoginPassword.Text == "")
            {
                Response.Write("<script>alert('Email & Password can not be blank!')</script>");
                return;
            }
            if (!Regex.IsMatch(txtLoginEmail.Text, @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"))
            {
                Response.Write("<script>alert('Email format is invalid!')</script>");
                return;
            }
            if (!Regex.IsMatch(txtLoginPassword.Text, @"^[a-zA-Z0-9]{1,100}$"))
            {
                Response.Write("<script>alert('Invalid Password Code!')</script>");
                return;
            }
           
            string strCode = hdncode.Value;
            AccountRepository accRepo = new AccountRepository();
            string passdServer = accRepo.getPasswordByEmail(txtLoginEmail.Text);
            passdServer = generateMd5Hash(passdServer + Convert.ToString(Session[clsSession.Seed])); // Adding seed to server password!
            if (passdServer == txtLoginPassword.Text)
            {
                Session[clsSession.AccId] = Convert.ToString(accRepo.getAccountIdByEmail(txtLoginEmail.Text));
                Response.Redirect(Redirect.GotToLocationInterface());
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        OuterLogin.Attributes.CssStyle.Add("display", "none");
        OuterRegister.Attributes.CssStyle.Add("display", "");
        //Perform Validations
        if (!Regex.IsMatch(txtRegEmail.Text, @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"))
        {
            Response.Write("<script>alert('Email format is invalid!')</script>");
            return;
        }
        if (txtRegPassword.Text == "")
        {
            Response.Write("<script>alert('Password can not be blank!')</script>");
            return;
        }
        if (!Regex.IsMatch(txtRegPassword.Text, @"^[a-zA-Z0-9]{1,100}$"))
        {
            Response.Write("<script>alert('Invalid Password Code. Password can only contain Alphabets and Numbers!')</script>");
            return;
        }
        //Check if email alrady registered
        AccountRepository objAccRep = new AccountRepository();
        Int64 accId = objAccRep.checkaccount(txtRegEmail.Text);
        if (accId == 0)
        {
            //Create New Account
            Account acc = new Account();
            acc.AccountId = Random15.generateNum();
            acc.Email = txtRegEmail.Text;
            string nameSlice = txtRegEmail.Text;
            acc.Name = nameSlice.Substring(0, nameSlice.LastIndexOf('@')).ToLower();
            acc.Password = generateMd5Hash(txtRegPassword.Text);
            acc.FBReg = false;
            acc.TwitterReg = false;
            acc.ManualReg = true;
            acc.EmailVerified = false;
            Int64 inserted = objAccRep.createAccountByEmail(acc);
            if (inserted != -1)
            {
                if (inserted == acc.AccountId)
                {
                    Session[clsSession.AccId] = inserted; // Created AccountId
                    Response.Redirect(Redirect.GotToLocationInterface()); // Redirecting to Set Location Page
                }
                else
                {
                    //Throw new application error
                }
            }
        }
        else
        {
            lblerror.Text = "This email is already registered with us. If you've forgot password click on reset password.";
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        //Perform Validations
        if (!Regex.IsMatch(txtRegPassword.Text, @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"))
        {
            Response.Write("<script>alert('Email format is invalid!')</script>");
            return;
        }
        // Sending mail
    }
}