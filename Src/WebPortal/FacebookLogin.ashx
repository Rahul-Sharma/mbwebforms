﻿<%@ WebHandler Language="C#" Class="FacebookLogin" %>

using System;
using System.Web;
using Facebook;
using BLL;
using DAL.Repositories;
using System.Data;
using System.Data.SqlClient;

public class FacebookLogin : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
       
            var accessToken = context.Request["accessToken"];
            context.Session["AccessToken"] = accessToken;
            //Fetching Email and other required fields
            var client = new FacebookClient(accessToken);
            dynamic result = client.Get("me", new { scope = "name,id,email,gender,username" });
            string name = Convert.ToString(result.name);
            string id = Convert.ToString(result.id);
            string email = Convert.ToString(result.email);
            //string birthday = result.birthday;
            string gender = Convert.ToString(result.gender);
            string username = result.username;
            string picture = "https://graph.facebook.com/" + id + "/picture?width=140&height=110";
      
            // Checking through Fb Id as email might not be available

            AccountRepository objAccRep = new AccountRepository();
            Int64 accId = objAccRep.checkaccountThroughFbId(id);

            if (accId != 0)
            {
                try
                {
                    context.Session[clsSession.AccId] = accId;
                    context.Response.Redirect("SetLocationInterface.aspx");
                }
                catch (Exception)
                {
                    //******For Handling Theard was aborted exception!
                    context.Response.Redirect("SetLocationInterface.aspx");
                }
                // context.Response.Redirect("MBHome.aspx?email=" + email + "&id=" + id + "&gender=" + gender + "&location=" + location + "&username=" + username);
            }
            ////Checking if this Account is registred with Meraband
            //AccountRepository objAccRep = new AccountRepository();
            //Int64 accId  = objAccRep.checkaccount(email);
            //if (accId != 0)
            //{
            //    context.Session[clsSession.AccId] = accId;
            //    context.Response.Redirect("MBHome.aspx?email=" + email + "&id=" + id + "&gender=" + gender + "&location=" + location+"&username="+username);
            //}
            //If not registred; perform Registration; generate GUID(AccId) and then redirect
            else
            {
                Account objAcc = new Account();
                //Setting Account Object fields
                if (gender == "" || gender == null)
                    objAcc.Gender = "";
                else
                    objAcc.Gender = gender;
                objAcc.Location = "";
                objAcc.FbId = Convert.ToInt64(id);
                objAcc.Name = name;
                if (username == "" || username == null)
                    objAcc.FBUserName = "";
                else
                    objAcc.FBUserName = username;
                objAcc.FBReg = true;
                objAcc.TwitterReg = false;
                objAcc.ManualReg = false;
                if (email == "" || email == null)
                    objAcc.Email = "";
                else
                    objAcc.Email = email;
                objAcc.EmailVerified = true;
                objAcc.ProfilePic = picture;
                //Creating AccountId through FBId and of length 15
                string strNUm = Convert.ToString(Random15.generateNum());
                strNUm = strNUm.Substring(0, 3);
                strNUm += Convert.ToString(id) + Convert.ToString(Random15.generateNum()).Substring(1, 7);
                strNUm = strNUm.Substring(0, 15);
                objAcc.AccountId = Convert.ToInt64(strNUm);
                //Creating Account!
                if (createAccountfunc(objAcc, objAcc.AccountId) != -1)
                {
                    try
                    {
                        context.Session[clsSession.AccId] = objAcc.AccountId;
                        context.Response.Redirect("SetLocationInterface.aspx");
                    }
                    catch (Exception)
                    {
                        //******For Handling Theard was aborted exception!
                        context.Response.Redirect("SetLocationInterface.aspx");
                    }
                }
                // Duplicacy being not handled atm

            }

       
        //If Registred, GetAccount Id from email address and save AccId in Session
        // context.Response.Redirect("MBHome.aspx?email=" + email + "&id=" + id + "&gender=" + gender + "&location=" + location);
    }

    //private Int64 generateAccId(Int64 fbId)
    //{
    //    Random rd = new Random();
    //    int num = rd.Next(200900, 893100);
    //    Random random = new Random(DateTime.Now.Millisecond + num);
    //    string NewCodeId = random.Next(1000000, 9999999).ToString().PadLeft(6, '0');
    //    NewCodeId = Convert.ToString(fbId) + NewCodeId;
    //    return (Convert.ToInt64(fbId));
    //}
    private int createAccountfunc(Account acc, Int64 generateId)
    {
        AccountRepository accRepo = new AccountRepository();
        return (accRepo.createaccount(acc));
    }
    public bool IsReusable
    {
        get { return false; }
    }

}