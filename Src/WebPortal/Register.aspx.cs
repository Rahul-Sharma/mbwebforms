﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DAL.Repositories;
using BLL;


public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void submit1_Click(object sender, EventArgs e)
    {
        Account accObj = new Account();
        accObj.Email = txt_Email.Text;
        accObj.Password = txt_Password.Text;
        AccountRepository repObj = new AccountRepository();
        Int64 IfAuth = repObj.loginByEmail(accObj);
        if (IfAuth != 0)
        {
            Session[clsSession.AccId] = IfAuth;
            Response.Redirect("SetLocationInterface.aspx");
        }
        else
            Response.Write("<script>alert('Invalid Login!')</script>");
    }
}