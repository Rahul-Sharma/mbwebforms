﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BLL;
using DAL;
using DAL.Repositories;


public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        loadTags();
    }
    private void loadTags()
    {
        AdsRepository adsObj = new AdsRepository();
        List<string> lst = new List<string>();
        lst = adsObj.LoadTags();
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        int count = 0;
        foreach (string tag in lst)
        {
            if (count == 7)
            {
                TagsToAdd.Text += "    " + sb.ToString();
                return;
            }// sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            sb.Append(" <div id=\"Tags\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a><a id=\"" + tag.ToString() + "1\"  style=\"display:none;\" onclick=\"removetag(this.id)\" href=\"#\"><img src=\"Content/CloseTag.png\" alt=\"Remove this Tag\" /> </a> </div>&nbsp;&nbsp;");
            sb.Append("<div class=\"spaces\">&nbsp; </div>");
            count++;
        }
        TagsToAdd.Text += "    " + sb.ToString();
    }
}