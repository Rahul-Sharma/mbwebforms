﻿<%@ WebHandler Language="C#" Class="FBHandler" %>

using System;
using System.Web;
using Facebook;
public class FBHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        string id = "";
        string name = "";
        string email = "";
        try
        {
            var accessToken = context.Request["accessToken"];

            context.Session["AccessToken"] = accessToken;
            var client = new FacebookClient(accessToken);
            dynamic result = client.Get("me", new { fields = "name,id,email" });
             name = Convert.ToString(result.name);
             id = Convert.ToString(result.id);
             email = Convert.ToString(result.email);
            //context.Response.Redirect("MBHOME.aspx?email=" + id + email + name);
           
        }
        //******For Exception of type thread being aborted
        catch (Exception ex)
        {
            context.Response.Redirect("MBHOME.aspx?email=" + id + email + name);
        }


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}