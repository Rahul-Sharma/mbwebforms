﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DAL.Repositories;
using System.Collections;
using BLL;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
public partial class AjaxHandlerAds : System.Web.UI.Page
{
    string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader reader;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["EntityId"] != null)
        {
            GetEditAdDetails(Convert.ToInt64(Request.QueryString["EntityId"]));
        }
    }
    private void GetEditAdDetails(Int64 p)
    {
        con = new SqlConnection(connstr);
        cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "SELECT [Subject], [Description], ObjectiveId FROM Ads WHERE AdId=@AdId";
        cmd.Parameters.AddWithValue("@AdId", p);
        con.Open();
        reader = cmd.ExecuteReader();
        string returnValStr = "*";
        if (reader.HasRows)
        {
            reader.Read();
            returnValStr = Convert.ToString(reader["Subject"]) + "-" + Convert.ToString(reader["Description"]) + "-" + Convert.ToString(reader["ObjectiveId"]);
        }
        Response.Write(returnValStr);
        Response.End();

    }
}