﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginSnippet.aspx.cs" Inherits="LoginSnippet" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Flat Login</title>
    <link href="Content/Buttons.css" rel="stylesheet" />
    <script src="Scripts/Jquery1.9.js"></script>
    <link href="Content/log.css" rel="stylesheet" />
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS
  ================================================== -->

    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
 
    <div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '138186586379417', // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        // Additional initialization code here
        FB.Event.subscribe('auth.authResponseChange', function (response) {
            if (response.status === 'connected') {
                // the user is logged in and has authenticated your
                // app, and response.authResponse supplies
                // the user's ID, a valid access token, a signed
                // request, and the time the access token 
                // and signed request each expire
               
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;

                // TODO: Handle the access token
                // Do a post to the server to finish the logon
                // This is a form post since we don't want to use AJAX
                var form = document.createElement("form");
                form.setAttribute("method", 'post');
                form.setAttribute("action", '/FacebookLogin.ashx');

                var field = document.createElement("input");
                field.setAttribute("type", "hidden");
                field.setAttribute("name", 'accessToken');
                field.setAttribute("value", accessToken);
                form.appendChild(field);

                document.body.appendChild(form);
                form.submit();

            } else if (response.status === 'not_authorized') {
                // the user is logged in to Facebook, 
                // but has not authenticated your app
            } else {
                // the user isn't logged in to Facebook.
            }
        });
    };

    // Load the SDK Asynchronously
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
</script>


  
    <form id="Form1" runat="server">
        <div id="OuuterMostLogReg">
            <div id="FacebookLoginDiv">Sign In/Register With Your Facebook Account </div>
            <br />
            <div class="fb-login-button" size="xlarge" ></div>
           
            
            <div id="EmailOption">
                <h1>OR</h1>
            </div>
            <div id="radioOption">

                <input id="Radio1" type="radio" name="hh" title="Sign In" onchange="ShowLogin();" />
                Sign In (or)
                <input id="Radio2" type="radio" name="hh" title="Sign Up" onchange="ShowRegister();" />
                Register
                with Email
            </div>

            <div id="OuterLogin" runat="server" style="display: none;">

                <div id="signUp">
                    <span class="titlRegLog">Sign In</span>
                    <p class="comment-form-author">
                        <label for="Email">
                            Email
                            
                        </label>
                        <asp:TextBox ID="txtLoginEmail" runat="server"></asp:TextBox>
                    </p>
                    <p class="comment-form-email">
                        <label for="Password">
                            Password 
                        </label>
                        <asp:TextBox ID="txtLoginPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </p>



                    <br />
                    <p style="text-align: center;">
                        <asp:Button ID="btnLogin" runat="server" Text="Sign In" class="btn" OnClick="btnLogin_Click" />
                    </p>
                </div>
            </div>

            <div id="OuterRegister" style="display: none;" runat="server">

                <div id="Reg">
                    <span class="titlRegLog">Register</span>
                    <p class="comment-form-author">
                        <label for="Email">
                            Email
                        </label>
                        <asp:TextBox ID="txtRegEmail" runat="server"></asp:TextBox>
                    </p>
                    <p class="comment-form-email">
                        <label for="Password">
                            Password 
                        </label>
                        <asp:TextBox ID="txtRegPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </p>
                    <br />
                    <p style="text-align: center;">
                        <asp:Button ID="btnRegister" class="btn" runat="server" Text="Register" OnClick="btnRegister_Click" />
                    </p>
                </div>

            </div>
            <div id="errorDiv">
                <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
            </div>
            <div class="OuterForgotPassword" style="display: none;">
                <div id="Reset">

                    <p class="comment-form-author">
                        <label for="Email">
                            Email
                        </label>
                        <asp:TextBox ID="txtResetEmail" runat="server" CssClass="toupper"></asp:TextBox>
                    </p>
                    <p class="comment-form-email">
                        <label for="Password">
                            Password 
                        </label>
                        <asp:TextBox ID="txtResetPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </p>



                    <br />
                    <p style="text-align: center; padding-right: 100px">
                        <asp:Button ID="btnReset" runat="server" Text="Sign In" CssClass="rnd btncontent" />
                    </p>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdncode" runat="server" />
        <script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    </form>
</body>
</html>
<script type="text/javascript">
    function ShowLogin() {
        document.getElementById('OuterLogin').style.display = '';
        document.getElementById('OuterRegister').style.display = 'none';
    }
    function ShowRegister() {
        document.getElementById('OuterLogin').style.display = 'none';
        document.getElementById('OuterRegister').style.display = '';
    }
</script>
<style>
    #OuuterMostLogReg {
        width: 100%;
        margin: 0 auto;
        text-align: center;
    }

    .titlRegLog {
        font-size: x-large;
        font: xx-large;
        text-align: left;
    }

    body {
        background: #1a1a1a;
        color: white;
        font-family: 'Roboto';
    }

    #radioOption {
        padding: 10px;
        font-size:x-large;
    }

    #FacebookLoginDiv {
        width: 100%;
        text-align: center;
        margin: 0 auto;
        padding-top: 40px;
        font-size:x-large;
    }

    #RegisterPop {
    }

    #MiddleVertical {
        width: 1px;
        background-color: black;
    }

    #OuterLogin {
    }

    #OuterRegister {
    }
</style>




<script src="Scripts/md5.js"></script>
<script type="text/javascript">
    function md5authLogin(seed) {
        if (document.getElementById('<%=txtLoginEmail.ClientID %>').value != '') {
            if (!checkEmail(document.getElementById('<%=txtLoginEmail.ClientID %>'))) {
                alert('Please provide a valid email address');
                document.getElementById('<%=txtLoginEmail.ClientID %>').focus;
                return false;
            }
            else {
                var rexp = /^\w+$/;
                var password = document.getElementById('<%=txtLoginPassword.ClientID%>').value;
                if (password.length == 0) {
                    alert("Please enter password!");
                    document.getElementById('<%=txtLoginPassword.ClientID%>').focus();
                    return false;
                }
                var hash = hex_md5(password);
                document.getElementById('<%=hdncode.ClientID%>').value = hash;
                hash = hex_md5(hash + seed);
                document.getElementById('<%=txtLoginPassword.ClientID%>').value = hash;
                return true;
            }
        }
        else {
            alert('Email can not be blank!');
            document.getElementById('<%=txtLoginEmail.ClientID %>').focus;
            return false;
        }
    }
    function ValidateResetEmail() {


    }


    function md5authRegister() {

        if (document.getElementById('<%=txtRegEmail.ClientID %>').value != '') {
            if (!checkEmail(document.getElementById('<%=txtRegEmail.ClientID %>'))) {
                alert('Please provide a valid email address');
                document.getElementById('<%=txtRegEmail.ClientID %>').focus;
                return false;
            }
            else {
                var rexp = /^\w+$/;
                var password = document.getElementById('<%=txtRegPassword.ClientID%>').value;
                if (password.length == 0) {
                    alert("Please enter password!");
                    document.getElementById('<%=txtRegPassword.ClientID%>').focus();
                    return false;
                }
            }
        }
        else {
            alert('Email can not be blank!');
            document.getElementById('<%=txtRegEmail.ClientID %>').focus;
            return false;
        }

    }
    function checkEmail(val) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(val.value)) {

            return false;
        }
        else
            return true;
    }

</script>
