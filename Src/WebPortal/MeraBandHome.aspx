﻿<%@ Page Title="Meraband Home" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="MeraBandHome.aspx.cs" Inherits="Default3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp;<link href="Content/AdsStyle.css" rel="stylesheet" />
    <link href="Content/AdsStyling.css" rel="stylesheet" />
    <script src="Scripts/Jquery1.9.js"></script>
        <style type="text/css">
            .txtspace {
        line-height:16px;
        }
              .ddl {
             padding:8px 7px;
             /* Styles */
             background: #fff;
             border-radius: 5px;
             -moz-border-radius: 5px;
             -webkit-border-radius: 5px;
             box-shadow: 0 1px 0 rgba(0,0,0,0.2);
             cursor: pointer;
             outline: none;
             transition: all 0.3s ease-out;
           
         }
            .clickable {
                background: #ff6a00;
                width: auto;
                vertical-align: middle;
                height: 24px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table;
                float: left;
                padding: 3px;
                padding-right: 3px;
            }

            .clickable1 {
                width: auto;
                vertical-align: middle;
                height: 24px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table;
                float: left;
                padding: 3px;
                padding-right: 3px;
            }


            .clickable:hover {
                background: #111111;
                width: auto;
                vertical-align: middle;
                height: 24px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table;
                float: left;
            }

            .clickableVerticalOutter {
                display: table;
                width: auto;
                margin-left: 2px;
                padding-left: 2px;
                margin-top: 4px;
            }

            .clickableVertical {
                background: #ff6a00;
                width: auto;
                vertical-align: middle;
                height: 23px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table-cell;
                margin-left: 0px;
                float: left;
                margin-right: 2px;
            }

            .clickableVerticalSide {
                padding-left: 3px;
                color: black;
                width: auto;
                vertical-align: middle;
                height: 23px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table-cell;
                float: left;
                margin-left: 0px;
            }

            .clickableVertical:hover {
                background: #111111;
                width: auto;
                vertical-align: middle;
                height: 23px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
                display: table;
            }


            .spacesY {
                width: 8px;
                text-align: right;
                color: white;
                height: 5px;
            }

            .spaces {
                width: 8px;
            }

            .TagsNew {
                text-align: center;
                color: white;
                vertical-align: middle;
                padding: 5px;
                font-size: small;
                padding-top: 7px;
            }

            .Title {
                 width: 100%;
                   font-size: x-large;
                   color: #ff6a00;
                   font-weight: 500;
                   text-align: left;
                   margin-bottom: 3px;
                   text-anchor: middle;
                   padding-top:4px;
            }

            .headTitle {
                font-size: large;
                color: #808080;
                margin: 0 auto;
            }

            .adListDIV {
                margin: 0 auto;
                text-align: center;
                overflow-y: scroll;
                height: 670px;
                width: 810px;
                float: left;
                margin-bottom: 4px;
                padding-right: 0px;
                margin-right: 0px;
            }

            .BandName {
                color: #2c2a2a;
                text-align: center;
                font-weight: 100;
                text-anchor: middle;
            }

            .auto-style5 {
                width: 60%;
                height: 400px;
                border-color: #808080;
                text-align:justify;
            }

            .tagsn {
                margin-top: 3px;
                margin-bottom: 0px;
                margin-left: 5px;
            }

            .imgForShare 
                width: 45px;
                height: 45px;
            }

            /*#adstag {
            border: 2px solid #808080;
            width: 80%;
            margin: 0 auto;
        }*/

            .Share {
                width: 10%;
                text-align: center;
                flex-item-align: center;
            }

            .Bandpic {
                width: 20%;
                text-align: left;
            }

            .post {
                font-family: Verdana;
                font-weight: 400;
                text-align: center;
                vertical-align: top;
                width: 70%;
                color: #808080;
                /*height:200px;*/
            }

            .btnPost {
                text-align: center;
                margin-top: 3px;
            }

            .PostbtnStyle {
                transition: all 0.3s linear;
                background-image: linear-gradient(to top right, #feda71 0%, #FDAD18 100%);
                margin: 0 auto;
                color: white;
                width: auto;
                vertical-align: middle;
                font-family: Verdana;
                height: 33px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
            }

                .PostbtnStyle:hover {
                    transition: all 0.3s linear;
                    background-image: linear-gradient(to top right, #feda71 0%, #febb4a 100%);
                    width: auto;
                    color: white;
                    vertical-align: middle;
                    font-family: Verdana;
                    border-radius: 5px;
                    height: 33px;
                    -moz-border-radius: 5px;
                    -webkit-border-radius: 5px;
                    text-decoration-color: white;
                    text-align: center;
                }

            .AdList {
                color: #2c2a2a;
                text-align: center;
                font-weight: bold;
                height: 150px;
                width: 100%;
            }

            #MainBarFirst {
                width: 100%;
                text-align: left;
                padding-top: 4px;
                padding-left: 2px;
            }

            #MainBarSecond {
                width: 100%;
                display: table;
                padding-top: 4px;
                padding-left: 2px;
            }

            #MainBarSecondLeft {
                width: 75%;
                float: left;
                display: table-cell;
                text-align: left;
            }

            #MainBarSecondRight {
                width: 25%;
                float: left;
                display: table-cell;
                text-align: right;
                text-decoration:underline;
            }

            #OuterMain {
                width: 100%;
                display: table;
                padding-left:20px;
            }

            #TagsByLoc {
                float: right;
                display: table-cell;
                width: 180px;
            }

            #TagBarTitle {
                width: 100%;
                text-align: left;
                padding-top:15px;
            }

            #TagsVertical {
                width: 100%;
                float: left;
                padding-top: 40px;
            }

            #lblTagVertical {
                float: left;
            }

            #DefaultMsg {
                text-align: center;
                font-size: large;
                padding: 45px;
            }
        </style>
     <div style="width: 100%; display: table; height:600px; margin-top:50px; ">
      <div style="display: table-row">
        <div id="sideNavigation" runat="server" style="width: 20%; float:left;  display: table-cell;">
         <div>
            <a href="Messages.aspx" class="a-btn">
                <span id="BtnMsgTxt" runat="server" class="a-btn-text">Messages</span>
                <span class="a-btn-slide-text">Sent/Recieved</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>
        <div >
            <a href="ProfileId.aspx" class="a-btn">
                <span class="a-btn-text">Profile</span>
                <span class="a-btn-slide-text">View/Update</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>
        <div >
            <a href="MyAds.aspx" class="a-btn">
                <span id="TT" runat="server" class="a-btn-text">My Ads</span>
                <span class="a-btn-slide-text">View/Post</span>
                <span class="a-btn-icon-right"><span></span></span>
            </a>
        </div>

        </div> 
        
        <div style="width:80%; float:left; display: table-cell; height:800px;">
          <div id="OuterMain">
    <div id="adstag">
     <div id="MainBar">
         <div id="MainBarFirst"><span class="AdList">Latest Ads in your region: </span> <asp:Label ID="lblLocation" runat="server"></asp:Label></div>
         <div id="MainBarSecond">
             <div id="MainBarSecondLeft">See Ads by nearby regions : 
 
                 <asp:dropdownlist id="ddllocalcities" class="ddl" runat="server" AutoPostBack="true" appenddatabounditems="true" causesvalidation="true" Width="150px"  forecolor="Black" OnSelectedIndexChanged="ddllocalcities_SelectedIndexChanged">
              
           </asp:dropdownlist>

             </div>
             <div id="MainBarSecondRight"><a href="ViewAd.aspx" style="color:white;">Extended Search</a></div>
         </div>
     </div>
         <div  class="adListDIV">
             <asp:Label ID="lbldefault" runat="server" Visible="false"></asp:Label>
             <br />
             <asp:HyperLink NavigateUrl="~/PostAd.aspx" ID="HyperLink1" runat="server" Visible="false">Post Your Ad!</asp:HyperLink>
  <asp:DataList ID="DataList1" runat="server"  DataKeyField="AdId" OnItemDataBound="DataList1_ItemDataBound"  OnDataBinding="DataList1_DataBinding" CellPadding="0" OnItemCommand="DataList1_ItemCommand">
<HeaderTemplate>

</HeaderTemplate>

<ItemTemplate>
          <div class="AdBoundary">
            <div class="AdHeaderSubject"> <asp:Label ID="sub" runat="server" Text='<%# Eval("Subject") %>'></asp:Label></div>
            <div class="PicDescriptionOutter">
                <div class="PicDescPicArea">
                    <div class="PicDescPicAreaRighAlign">
                    <asp:Image ID="Image7" runat="server" ImageUrl='<%#Eval("PicPath","Content/profilepic/{0}") %>' Width="45px" Height="45px" />
                        </div><br />
                   <a href="ProfileView.aspx?Id=<%# Eval("AccountId") %>"  title="View Profile"><strong><asp:Label ID="Label4" class="txtspace" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Label></strong></a></div>
                <div class="PicDescDescArea">
                  <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                </div>
                <div class="PicDescShareArea"> 
                    <div class="ShareAreaRightAlign"> <asp:HyperLink ID="fb" runat="server" Tilte="Share on Facebook" Target="_blank"> <asp:Image ID="ImgFB" class="imgForShare" ImageUrl="~/Content/img/Facebook_icon.png" runat="server" Width="35px" Height="35px"/></asp:HyperLink>
                  <br /> <br /><br /><asp:HyperLink ID="tw" runat="server" Title="Share on Twitter" Target="_blank"> <asp:Image ID="ImgTW" class="imgForShare" ImageUrl="~/Content/img/twitterlogo.png" runat="server" Width="35px" Height="35px"/></asp:HyperLink>
               </div>
                    </div>
            </div>
            <div class="PostButtonDiv">
                <asp:Button ID="Button2" class="PostbtnStyle" runat="server" CommandName="PostMsg" CommandArgument='<%# Eval("AccountId") %>' Text="Post Reply" Style="font-weight: 500; font-size: x-large" />
                  </div>
            <div class="TagDateOutter">
                <div class="TagHorizontalList">
                    <asp:Label ID="TagsToAdd" runat="server" Text='<%# Eval("AdIdStr") %>'></asp:Label>
                        </div>
                <div class="DateHorizontal">
                    <div class="DateRightAlign">
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label></div>
                </div>
            </div>
        </div>
      <div style="background-color:gray; height:2px; width:100%; " ></div>
</ItemTemplate>
    
<FooterTemplate>
   
</FooterTemplate>
        </asp:DataList>
   </div>
    </div>
 <div id="TagsByLoc">
     <div id="TagBarTitle">Tagged (Regional!)</div>
     <div id="TagsVertical"><asp:Label ID="lblTagVertical" runat="server"></asp:Label></div>
 </div>
</div>
            </div>
         </div>
       
</div>
  
    <asp:HiddenField ID="HdnTags" runat="server" />
    <asp:Button ID="BtnShowTagged" runat="server" style="display:none;" Text="Button" OnClick="BtnShowTagged_Click" />
    <script type="text/javascript">



        function ShowAdbyTag(tagId) {

            document.getElementById('<%=HdnTags.ClientID%>').value = tagId;

            __doPostBack('<%=BtnShowTagged.UniqueID%>', "");
        }

        function CheckDDlChange() {
          
           
            
        }

</script>
    

</asp:Content>

