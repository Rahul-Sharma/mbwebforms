﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="SetLocationInterface.aspx.cs" Inherits="Default123" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <div>
        <link href="Content/Buttons.css" rel="stylesheet" />

        <link href="Content/CommonStyle.css" rel="stylesheet" />
        <style type="text/css">
            body {
            }

            .main {
                text-align: center;
                font: 12pt Arial;
                width: 100%;
                height: auto;
                width: 100%;
                margin: 0 auto;
                margin-top: 190px;
            }

            .contents {
                padding: 10px;
            }

                .contents p span {
                   
                    float: left;
                    margin-left: 0px;
                    width: 130px;
                    color: gray;
                    font-weight: bold;
                }

                .contents p select {
                    float: left;
                    margin-left: 90px;
                }

                .contents p {
                    clear: both;
                    overflow: hidden;
                }

            #MainTitle {
                width: 100%;
            }

            #MainTitle {
                width: 100%;
                height: auto;
            }

            #HeaderForLoc {
                height: 60px;
            }

            #MainTitleone {
                font: 800;
                height: 42px;
                font-weight: bolder;
                font-size: x-large;
                margin-bottom: 2px;
            }

            #SubTitle {
                font: 600;
                height: 15px;
                margin-top: 4px;
                margin-bottom: 3px;
            }

            #ddlCountry {
                margin-top: 10px;
                padding: 7px;
            }

            #ddlState {
                padding: 7px;
            }

            #ddlCity {
                padding: 7px;
            }

            #btnAddLocation {
                padding: 7px;
            }

            #SuggestLocs {
                padding-top: 20px;
            }

            #SuggestCountry {
                padding-top: 8px;
            }

            #SuggestCity {
                padding-top: 8px;
            }

            #SuggestState {
                padding-top: 8px;
            }
            #suggText {
            padding-top:9px;
            font:200;
            font-size:small;
            }
            #SuggBtnDiv {
            padding-top:12px;
            }
            .ddl {
            padding: 12px 15px;
            /* Styles */
            background: #fff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 0 1px 0 rgba(0,0,0,0.2);
            cursor: pointer;
            outline: none;
            transition: all 0.3s ease-out;
        }
        </style>
        <div class="main">

            <div id="MainTitle">
                <div id="LocPanel" runat="server">
                <div id="addLocDiv" runat="server" style="display: inline;" >
                    <div id="HeaderForLoc">
                        <div id="MainTitleone">Please Set Your Location: Once in a life time pain!</div>
                        <div id="SubTitle">This helps us to serve you with better and most relevant content.</div>
                    </div>
                    <div id="ddlCountry">
                        <asp:DropDownList ID="ddlCountryList" runat="server" CssClass="ddl" width="250px" AppendDataBoundItems="true" CausesValidation="true" OnSelectedIndexChanged="ddlCountryList_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Value="-1" Text="Select Country"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id="ddlState">
                        <asp:DropDownList ID="ddlStateCity" runat="server" CssClass="ddl" width="250px" AppendDataBoundItems="true" CausesValidation="true" OnSelectedIndexChanged="ddlStateCity_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Value="-1" Text="Select State/Sub-Region"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id="ddlCity">
                        <asp:DropDownList ID="ddlCityList" AppendDataBoundItems="true" CssClass="ddl" width="250px" CausesValidation="true" runat="server">
                            <asp:ListItem Value="-1" Text="Select City"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id="btnAddLocation">
                        <asp:Button ID="btnAddLoc" class="btn" runat="server" Width="180px" Text="Mark My Territory" CausesValidation="true" OnClick="btnAddLoc_Click" />
                    </div>
                </div>
                    </div>
                <div id="SuggestLocs">
                    <asp:CheckBox ID="chkSuggest" runat="server"  Text="Check this box if you're not seeing your Country, State and City Above" OnCheckedChanged="chkSuggest_CheckedChanged" AutoPostBack="True" />
                </div>
                  <div id="SuggestPanel" runat="server">
                <div id="SuggLovDiv" style="display: none;" runat="server">
                    <div id="suggText" runat="server">
                        Once you suggest this location, we'll shortly add this location to our database. 
                    </div>
                    <div id="SuggestCountry">
                        <asp:DropDownList ID="ddlSuggestCountry" runat="server" CssClass="ddl" width="250px" OnSelectedIndexChanged="ddlSuggestState_SelectedIndexChanged" CausesValidation="true" AutoPostBack="true">
                              <asp:ListItem Value="-1" Text="Please Select Your Country"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                   
                    <div id="SuggestState">
                       <asp:DropDownList ID="ddlSuggestState" runat="server" CssClass="ddl" width="250px" AppendDataBoundItems="true" >
                           <asp:ListItem Value="-1" Text="Then Select Your State/Sub-Region"></asp:ListItem>
                       </asp:DropDownList>
                    </div>
                    <div id="SuggestCity">
                        <asp:TextBox ID="suggCityTxt" placeholder="Enter Your City Name (Please Spell Correctly!)" runat="server"></asp:TextBox>
                       </div>
                    <div id="SuggBtnDiv">
                        <asp:Button ID="Button1" class="btn" runat="server" Width="180px" Text="Sketch New Territory" OnClick="Button1_Click" />
                        
                    </div>
                </div>
                      </div>
            </div>
        </div>
       



    </div>
</asp:Content>

