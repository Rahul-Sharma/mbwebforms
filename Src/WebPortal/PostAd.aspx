﻿<%@ Page Title="Post Ad" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="PostAd.aspx.cs" Inherits="TestTry" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <br />
    <br />
    <br />
    <br />
    <script type="text/javascript">
        var tags = '';
        var count = 0;
        function removeFilter(tagId) {
            var tagAdd;
            var elem = document.getElementById('filter' + String(tagId));
            elem.parentNode.removeChild(elem);
            var arr = String(document.getElementById('<%=hdnValue.ClientID %>').value).split('-', 50);
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == String(tagId)) {
                    arr[i] = 's';
                }
            }
            document.getElementById('<%=hdnValue.ClientID %>').value = '';
            for (var i = 0; i < arr.length; i++) {
                if (document.getElementById('<%=hdnValue.ClientID %>').value == '')
                    document.getElementById('<%=hdnValue.ClientID %>').value = arr[i];
                else
                    document.getElementById('<%=hdnValue.ClientID %>').value += '-' + arr[i];
            }
            count = parseInt(count) - 1;
        }

        function Show(atag) {
            if (parseInt(count) > 4) {
                alert('You cant add more than 5 tags!');
                return false;
            }
            var arr = String(document.getElementById('<%=hdnValue.ClientID %>').value).split('-', 50);
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == String(atag)) {
                    alert('You already have this tag! Remove any tag to add new one!');
                    return false;
                }
            }
            var tagName = document.getElementById(atag).innerHTML;
            document.getElementById('<%=lblAddedTags.ClientID %>').innerHTML += "<div  id=\"filter" + atag + "\" class=\"filterDivs\">" + tagName + "<a id=\"" + atag + "\"  onclick=\"removeFilter(this.id)\" href=\"#\"><img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove \" /> </a> </div>";
            tags = document.getElementById('<%=hdnValue.ClientID %>').value;
            if (tags == '')
                tags += String(atag);
            else
                tags += '-' + String(atag);
            document.getElementById('<%=hdnValue.ClientID %>').value = tags;
            count = parseInt(count) + 1;
        }
        function SetTags() {
            if (parseInt(count) == 0) {
                alert('You must select at least 1 tag for your ad!');
                return false;
            }
            if (document.getElementById('<%=ddlObjective.ClientID %>').value == "-1") {
                 alert('Please, you must select an objective for your Ad!');
                 return false;
             }

         }
    </script>
    <link href="Content/css/PageWise/PostAd.css" rel="stylesheet" />
    <link href="Content/css/Tags.css" rel="stylesheet" />
    <style type='text/css'>
        body {
            background-image: url(../../Content/img/music-equalizer-background.jpg);
        }
    </style>
    <style type="text/css">
       
    </style>
    <asp:HiddenField ID="hdnValue" runat="server"></asp:HiddenField>
    <div id="PostOuter">
        <div id="PostHeader">
            Post Your Ad   (It's Free!)
        </div>
        <div id="PostTitle">
            Your
            Title:&nbsp;
            <input runat="server" id="title" title="title" type="text" width="350px" class="txtAreaFull"  placeholder="Enter a catchy yet relevant title" required="required" autofocus="autofocus" />
        </div>
        <div id="PostDescription">
            Description: 
 <textarea runat="server" id="description" title="Description" rows="5"  width="350px" class="txtAreaFull" placeholder="A bit of detail" required="required"></textarea>

        </div>
        <div id="PostLocation">
            Your Location : <label runat="server" style="color:#ff6a00;" id="yourLocation"></label>
        </div>
        <div id="PostObjective">Objective: 
            <asp:DropDownList ID="ddlObjective" runat="server" class="ddl" AppendDataBoundItems="true" CausesValidation="true">
                <asp:ListItem Value="-1" Text="------------Please Select Meta Objective :-) "></asp:ListItem>
            </asp:DropDownList>

        </div>
        <div id="PostAddedTags">
            <asp:Label ID="lblAddedTags" runat="server" Text=""></asp:Label></div>

        <div id="PostTagLstTitle">Select Tags &nbsp; (Click on tag names to add them to your ad)</div>
        <div id="PostTagLst">
            <asp:Label ID="TagsToAdd" runat="server" Text=""></asp:Label></div>

        <div id="PostButton">
            <asp:Button ID="Button" runat="server" Text="Post AD" OnClick="Button_Click" CssClass="btn" OnClientClick="return SetTags();"></asp:Button>

        </div>
    </div>

</asp:Content>
