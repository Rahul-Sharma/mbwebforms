﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using DAL.Repositories;
using System.Text;
using System.Text.RegularExpressions;

public partial class ProfileView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Id"] != null)
        {
            panelPro.Visible = true;
            divMsgNotfound.Visible = false;
            LoadProfile();
        }
        else
        {
            panelPro.Visible = false;
            divMsgNotfound.Visible = true;
        }

    }
    private void LoadProfileTag()
    {

        ProfileRepository objPro = new ProfileRepository();
        List<string> lst = new List<string>();
        lst = objPro.getTagsForProfile(Convert.ToInt64(Request.QueryString["Id"]));
        StringBuilder sb = new StringBuilder();
        foreach (string tagName in lst)
        {
            sb.Append("<div class=\"filterDivs\">" + tagName + "</div>&nbsp;");
            sb.Append("<div class=\"filterDivsSpace\">&nbsp;&nbsp;</div>");
        }
        lblTags.Text = sb.ToString();
    }
    private void LoadProfile()
    {
        if (!Regex.IsMatch(Convert.ToString(Request.QueryString["Id"]).Trim(), @"^[0-9]{1,18}$"))
        {
            panelPro.Visible = false;
            divMsgNotfound.Visible = true;
            return;
        }
        //lblTags.Text = "<div class=\"filterDivs\"> </div>";
        Profile objPro = new Profile();
        ProfileRepository repoPro = new ProfileRepository();
        objPro = repoPro.ShowProfile(Convert.ToInt64(Request.QueryString["Id"]));
        if (objPro.DisplayName == null)
        {
            panelPro.Visible = false;
            divMsgNotfound.Visible = true;
        }
        else
        {
            panelPro.Visible = true;
            divMsgNotfound.Visible = false;
            if (objPro.ProfilePic.Contains("facebook"))
                proImg.ImageUrl = objPro.ProfilePic;
            else
                proImg.ImageUrl = "~/Content/profilepic/" + objPro.ProfilePic;
            lblBandName.Text = objPro.DisplayName;
            //if (objPro.ProfileType == 1)
            //{
            //    lblBandName.Text = objPro.BandName;
            //}
            //else
            //{
            //    lblBandName.Text = objPro.DisplayName;
            //}
            lblBandName.Text += ". " + objPro.CityName + ", " + objPro.CountryName;
            lblDescription.Text = objPro.Description;
            lblSoundcloudFrame.Text = objPro.SoundCloudFrame;
            lblYoutubeFrame.Text = objPro.YoutubeFrame;
            if (objPro.Facebook == "")
                hyperFB.Visible = false;
            else
                hyperFB.NavigateUrl = "http://www.facebook.com/" + objPro.Facebook;
            if (objPro.Twitter == "")
                hyperTwitter.Visible = false;
            hyperTwitter.NavigateUrl = "http://www.twitter.com/" + objPro.Twitter;
            LoadProfileTag();
        }
    }
    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Response.Redirect(Redirect.GoToHome());
        }
        else
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}', 'mywin', 'style=position:fixed; left=25, top=25, width=560, height=420, toolbar=no, scrollbars=yes');</script>", "SendMessage.aspx?EntityId=" + "-1" + "&User=" + Convert.ToInt64(Request.QueryString["Id"]) + "&CType=2"));

    }
}