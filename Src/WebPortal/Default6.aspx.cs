﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BLL;
using DAL.Repositories;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Services;

public partial class Default6 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Repeater1.DataSource = GetAdsData(1);
            Repeater1.DataBind();
        }
        
    }
    public static DataSet GetAdsData(int pageIndex)
    {
        string query = "[getAdsByLocaleUser1]";
        SqlCommand cmd = new SqlCommand(query);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
        cmd.Parameters.AddWithValue("@PageSize", 3);
        cmd.Parameters.AddWithValue("@Country", "India");
        cmd.Parameters.AddWithValue("@State","State of Karnataka");
        cmd.Parameters.AddWithValue("@City", "Bangalore Urban");
        cmd.Parameters.AddWithValue("@AccountId", 333);
        cmd.Parameters.Add("@PageCount", SqlDbType.Int, 6).Direction = ParameterDirection.Output;
        return LoadAds(cmd);
    }
    private static DataSet LoadAds(SqlCommand cmd)
    {
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        using (SqlConnection con = new SqlConnection(connstr))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                sda.SelectCommand = cmd;
                using (DataSet ds = new DataSet())
                {
                    sda.Fill(ds, "Ads");
                    DataTable dt = new DataTable("PageCount");
                    dt.Columns.Add("PageCount");
                    dt.Rows.Add();
                    dt.Rows[0][0] = cmd.Parameters["@PageCount"].Value;
                    ds.Tables.Add(dt);
                    return ds;
                }
            }
        }
    }
   

    [WebMethod]
    public static string GetCustomers(int pageIndex)
    {
        return GetAdsData(pageIndex).GetXml();
    }

}