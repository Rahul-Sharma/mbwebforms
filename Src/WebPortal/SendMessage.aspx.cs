﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using DAL.Repositories;
using System.Text.RegularExpressions;

public partial class SendMessage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToLogin());
        }
        else
        {
            if (Convert.ToString(Session[clsSession.AccId]) == Convert.ToString(Request.QueryString["User"]))
            {
                lblDefault.Visible = true;
                OuterMsgGrid.Visible = false;
            }
            else
                LoadMsgAttributes();
        }

    }
    private void LoadMsgAttributes()
    {
        MessageEntity msg = new MessageEntity();
        MessagesRepository msgrepo = new MessagesRepository();
        msg = msgrepo.getSendMsgAttributes(Convert.ToInt64(Request.QueryString["User"]));
        imgRecipient.ImageUrl = "Content/profilepic/" + msg.ProfilePic;
        hlnRecipient.Text = msg.BandName;
    }
    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        //if (!Regex.IsMatch(txtMsgBody.Text, @"^[a-zA-Z0-9,.//\\ ()]{1,1000}$"))
        //{
        //    Response.Write("<script>alert('Invalid Message Content Code!')</script>");
        //    return;
        //}
        if (txtMsgBody.Text == "")
        {
            Response.Write("<script>alert('Message can not be blank!);</script>");
            return;
        }
        MessagesRepository msgrepo = new MessagesRepository();
        MessageEntity msg = new MessageEntity();
        msg.SentByAccountId = Convert.ToInt64(Session[clsSession.AccId]);
        msg.RecipientAccountId = Convert.ToInt64(Request.QueryString["User"]);
        msg.MsgStatus = false;
        msg.MsgType = 1; // For Musician Ads
        msg.IpAddress = Convert.ToString(Request.ServerVariables["REMOTE_ADDR"]);
        msg.MsgBody = txtMsgBody.Text.Trim();
        msg.MsgEntity = Convert.ToInt64(Request.QueryString["EntityId"]);
        int inserted = msgrepo.SendMessage(msg);
        if (inserted == -1)
        {
            string close = @"<script type='text/javascript'>
                                window.returnValue = true;
                                window.close();
                                </script>";
            base.Response.Write(close);
        }
    }
}