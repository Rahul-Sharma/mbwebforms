﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class DemoProfilePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadProfileTag();
    }

    private void LoadProfileTag()
    {

        string[] strGenre = new string[] {"Rock", "Rock Alternative", "Grunge" };
        StringBuilder sb = new StringBuilder();
        foreach (string tagName in strGenre)
        {
            sb.Append("<div class=\"filterDivs\">" + tagName + "</div>&nbsp;");
            sb.Append("<div class=\"filterDivsSpace\">&nbsp;&nbsp;</div>");
        }
        lblTags.Text = sb.ToString();
    }
}