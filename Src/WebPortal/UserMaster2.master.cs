﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class UserMaster2 : System.Web.UI.MasterPage
{
    string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader reader;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            HomeLnk.HRef = Redirect.GoToHome();
            lnkAuth.InnerText = "Login/Signup";
            lnkAuth.HRef = Redirect.GoToLogin();
            LogPic.Visible = false;
        }
        else
        {
            if (!IsPostBack)
            {
                con = new SqlConnection(connstr);
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select Name, ProfilePic from Profile where AccountId=@AccountId";
                cmd.Parameters.AddWithValue("@AccountId", Convert.ToString(Session[clsSession.AccId]));
                con.Open();
                reader = cmd.ExecuteReader();
                string[] str = new string[6];
                if (reader.HasRows)
                {
                    reader.Read();
                    str = Convert.ToString(reader["Name"]).Split(' ');
                    if (str[0] != "the" && str[0] != "The")
                        lblName.Text = "Hi " + str[0];
                    if (Convert.ToString(reader["ProfilePic"]).Contains("facebook"))
                        imgUser.ImageUrl = Convert.ToString(reader["ProfilePic"]);
                    else
                        imgUser.ImageUrl = "Content/profilepic/" + Convert.ToString(reader["ProfilePic"]);
                }
                LogPic.Visible = true;

            }

            Security.RemoveCache();


        }


    }
}

