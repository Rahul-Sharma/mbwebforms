﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta property="og:image" content="http://meraband.com/content/img/converto.jpg"/>
    <meta property="og:type" content="Community"/>
    <meta property="og:site_name" content="Meraband"/>
    <meta property="og:url" content="http://meraband.com"/>
    <meta property="og:title" content="Find Musicians/Gear, Share, Start a new band, Gigs and much more!"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Meraband</title>
    <script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <meta name="description" content="" />
    <!--[if lte IE 8]>
  <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/explorer.css" />
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>             
  <![endif]-->
    <link href="../../Content/style.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/csspopup.js" type="text/javascript"></script>

    <link href="../../Content/00000000006982a6a10fdf25cd942ded921e8277a8.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../Content/MBAngleS.css?v=2" />
       <style>
        .fftter {
        vertical-align:baseline;
        margin-bottom:0px;
        color:white;
        background-color:black;
        width:100%;
        display:table;
        height:42px;
        padding-top:5px;
        padding-bottom:5px;

        }
        .fftterLeft {
        float:left;
        display:table-cell;
        width:48%;
        }
        .fftterLeftInner {
        float:left;
        display:table;
        text-align:left;
        }
        .divFB {
        display:table-cell;
        float:left;
        }
        .divTW {
            display:table-cell;
        float:left;
        }
        .fftterRight {
                float:left;
        display:table-cell;
        width:48%;
        }
        .fftterRightInner {
            float:right;
            color:#ff6a00;
        }

        .bodyImage {
        background-image: url(../../Content/themes/Background.png);
        background-repeat:repeat-y;
        height:750px;

        }
    </style>
    <style type="text/css" media="screen">
        #blanket
        {
            background-color: #111;
            opacity: 0.65;
            position: absolute;
            z-index: 9001;
            top: 0px;
            left: 0px;
            width: 100%;
        }
        #popUpDiv
        {
            position: absolute;
            background-color: #ffffff;
            width: 600px;
            z-index: 9002;
            left: 93px;
            border: 3px solid #a1a1a1;
            padding: 10px 10px;
            background: #ffffff;
            border-radius: 25px;
            -moz-border-radius: 25px; /* Firefox 3.6 and earlier */
            box-shadow: 5px 5px 2px;
            top: 69px;
              margin: 0 0 0 20%;
        }
        #case_studies #Top1
        {
            background-image: url(../../Content/img/img1.jpg);
        }
        #case_studies #Top1 .contentWrapper
        {
            background-image: url();
        }
        #case_studies #Top1 .abstract
        {
            margin-left: 10px;
        }
        #case_studies #Top2
        {
            background-image: url(../../Content/img/img3.jpg);
        }
        #case_studies #Top2 .contentWrapper
        {
            background-image: url();
        }
        #case_studies #Top2 .abstract
        {
            margin-left: 370px;
        }
        #case_studies #Top3
        {
            background-image: url(../../Content/img/UTADS.jpg);
        }
        #case_studies #Top3 .contentWrapper
        {
            background-image: url();
        }
        #case_studies #Top3 .abstract
        {
            margin-left: 50px;
        }
        #case_studies #Top4
        {
            background-image: url(../../Content/img/LonelyGuitar.jpg);
        }
        #case_studies #Top4 .contentWrapper
        {
            background-image: url();
        }
        #case_studies #Top4 .abstract
        {
            margin-left: 240px;
        }
        #case_studies #Top5
        {
            background-image: url();
        }
        #case_studies #Top5 .contentWrapper
        {
            background-image: url();
        }
        #case_studies #Top5 .abstract
        {
            margin-left: 340px;
        }
    </style>

    <script src="../../Scripts/MBAngular.js" type="text/javascript"></script>

    <script src="../../Scripts/MB.js" type="text/javascript"></script>

    <script type="text/javascript">
        MBand.twitter_user = "Bond_Vagabond";
  </script> 

    <script type="text/javascript">
        $(document).ready(function () { MBand.init("home"); });
  </script>

</head>
<body class="home">

    <form id="form1" runat="server">
    <!-- ClickTale Top part -->

    <script type="text/javascript">
        var WRInitTime = (new Date()).getTime();
</script>

    <!-- ClickTale end of Top part -->
    <header class="global">
  <div class="contentWrapper">
    <h1 id="siteHeader"><a href="Defaul.aspx"><span style="color:White">Mera Band</span></span></a></h1>
    <nav class="global">
      <ul>
        <li><a href="MeraBandHome.aspx" runat="server" class="home current">Home</a></li>
        <li><a href="PostAd.aspx" >Post Your Ad</a></li>
        <li><a href="ViewAd.aspx">Find Musicians</a></li>
        <li><a href="FindGear.aspx">Find Gear</a></li>
        <li><a href="Wordpress">Blog</a></li>
        <li><a href="Contact.aspx">Contact</a></li>
        <li><a id="logLink" href="#" onclick="ShowLogin()" runat="server">SignUp/Login</a></li>
      </ul>
    </nav>
  </div>
</header>
    <div id="blanket" style="display: none;">
    </div>
    <div id="popUpDiv"  style="display: none;">
        <div>
            <img src="../../Content/close-icon.png" onclick="popup('popUpDiv')" style="float: right" />
        </div>
       
                             <div style=" margin:0 auto; text-align:center;">  <div class="fb-login-button" data-show-faces="true" data-width="20" data-max-rows="1" data-height="40"></div>
                          </div> 
           <div style=" margin-top:8px; width:auto;">
               In Beta Version we're supporting only Facbook Registration/Login<br />
              <span style="padding-left:35px;"> More options will be put up soon.</span>
               </div> 
          

    </div>
    <div style="clear: both">
    </div>
    <!-- /header.global -->
    <section id="page">
    <div id="message">
      <div class="contentWrapper">
        <p>
	<span style="color:White;">Live</span> Play <span style="color:White;">Engage</span></p>

        &nbsp;</div>
    </div><!-- /#message -->
    <div id="case_studies">
        
          <section id="Top1">
            <div class="contentWrapper innerScroll">
              <div class="abstract">
                <header>
                  
                    <h2>Most Sophisticated Platform for Musicians</h2>
                  
                </header>
                <div class="body">
                  <p>Find Musicians/Gear, Post Free Ads, Communicate, Share, Start a new band!</p>
                  
                  <a href="Default.aspx" class="more">Learn More</a>
                  
                </div> 
              </div>
            </div>
          </section>
        
          <section id="Top2">
            <div class="contentWrapper innerScroll">
              <div class="abstract">
                <header>
                  
                    <h2>
	Broadcast & Multiply Communication </h2>

                  
                </header>
                <div class="body">
                  <p>
	Post you Ad here and we'll post it to our twitter/facebook channels >> Taking your content to the most relevant people!    
                  <a href="LoginSnippet.aspx" class="more">Learn More</a>
                  
                </div> 
              </div>
            </div>
          </section>
        
          <section id="Top3">
            <div class="contentWrapper innerScroll">
              <div class="abstract">
                <header>
                  
                    <h2>
	Search Just What You Need</h2>

                  
                </header>
                <div class="body">
                  <p>
	Search through all possible locations, genre and our smart search objectives.</p>

                  
                  <a href="ViewAd.aspx" class="more">Learn More</a>
                  
                </div> 
              </div>
            </div>
          </section>
        
          <section id="Top4">
            <div class="contentWrapper innerScroll">
              <div class="abstract">
                <header>
                  
                    <h2>
	Showcase Yourself! </h2>

                  
                </header>
                <div class="body">
                  <p>
                      Promote/Showcase your skills to the world with simple profiles
	</p>
                  
                  <a href="DemoProfilePage.aspx" class="more">Learn More</a>
                  
                </div> 
              </div>
            </div>
          </section>
        
          <section id="Top5">
            <div class="contentWrapper innerScroll">
              <div class="abstract">
                <header>
                  
                    <h2>
Coming Soon</h2>

                  
                </header>
                <div class="body">
                  <p>
	Buy/Sell Music Gear, Band Diaries, Music Player and much more in most sophisticated way possible!</p>

                  
                  <a href="#" class="more">Learn More</a>
                  
                </div> 
              </div>
            </div>
          </section>
        
    </div><!-- /#case_studies --> 
  
  </section>
    <aside id="secondary_content">
    

    <!-- /#page -->
  <footer class="fftter">
                   <div class="fftterLeft">
                        <div class="fftterLeftInner">
                    <div style="text-align:left; padding-top:5px; font-size:large; font:bold; float:left; color:#ff6a00; display:table-cell;">
                        Follow Us 

                    </div>
                      <div style="text-align:left; padding-left:4px; font-size:large; float:left;  vertical-align:middle; display:table-cell;">  
                           <a href="http://twitter.com/#!/Meraband" >
                              <img src="Content/img/twitterlogo.png" width="32" height="30"  />

                           </a>

                      </div>

                      <div style="text-align:left; padding-left:4px; font-size:large; float:left; vertical-align:middle;  display:table-cell;"> 
          
         <a href="http://www.facebook.com/pages/Meraband" ><img src="Content/img/Facebook_icon.png" width="32" height="31" /></a>
               </div>    </div>
                   </div>
                   <div class="fftterRight">
                        <div class="fftterRightInner">
© MeraBand 2013
                   </div>
                   </div>
  
</footer>
</aside>
    </form>
</body>
</html>

<script type="text/javascript">
    function ShowLogin() {
        var url = 'LoginSnippet.aspx';
        window.location.href = url;
    }
</script>






<script type="text/javascript">stLight.options({ publisher: "18cd8222-ef97-4f5a-9a0d-037ac3474cc4", doNotHash: true, doNotCopy: true, hashAddressBar: false });</script>
<script>
    var options = { "publisher": "18cd8222-ef97-4f5a-9a0d-037ac3474cc4", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0 }, "chicklets": { "items": ["facebook", "twitter", "pinterest", "email", "sharethis"] } };
    var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>