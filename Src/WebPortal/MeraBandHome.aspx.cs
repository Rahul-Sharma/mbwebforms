﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BLL;
using DAL;
using DAL.Repositories;


public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.GetPostBackEventReference(this, string.Empty);
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToLogin());
        }
        if (!IsPostBack)
        {
            string[] locationVals = Convert.ToString(Session[clsSession.LocationIds]).Split(',');
            // 0 index = country
            // 1 index = state
            // 2 index = city
            loadAds(Convert.ToInt32(locationVals[0]), Convert.ToInt32(locationVals[1]), Convert.ToInt32(locationVals[2]));
            loadLocalCities();

            //  loadTags();
        }
    }

    private void loadAds(int country, int state, int city)
    {
        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
       // string[] locationVals = Convert.ToString(Session[clsSession.LocationIds]).Split(',');
        lst = adRepo.ViewAdsByLocaleUser(city, state, country, Convert.ToInt64(Session[clsSession.AccId]));
        if (lst.Count > 0)
        {
            DataList1.DataSource = lst;
            DataList1.DataBind();
            loadTags(city, state, country, Convert.ToInt64(Session[clsSession.AccId]));
            //Replacing the same array elements with location strings
            string[] locationVals = Convert.ToString(Session[clsSession.Location]).Split(',');
            lblLocation.Text = locationVals[2] + ", " + locationVals[0];
            lbldefault.Visible = false;
        }
        else
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
            showDefaultMsg();
        }

    }
    private void showDefaultMsg()
    {
        string str = "<div id=\"DefaultMsg\"> " +
            "Sorry, there are no ads for your region." +
            " <br />" +
            "<br /> " +
            "<br />" +
            "but you can be the first person to post your requirement <br/>" +
            "</div>";
        lbldefault.Visible = true;
        HyperLink1.Visible = true;
        lbldefault.Text = str.ToString();


    }
    private void loadTags(int city, int state, int country, Int64 accId)
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadTagsByLocationAndAccount(city, state, country, accId);
        int length = lst.Count;
        StringBuilder sb = new StringBuilder();
        int count = 0;
        foreach (Ads tag in lst)
        {
            sb.Append(" <div class=\"clickableVerticalOutter\"><div id=\"Tags\" class=\"clickableVertical\" > <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div><div class=\"clickableVerticalSide\">" + "x" + tag.TagCount + "</div></div>");
            sb.Append("<div class=\"spacesY\">&nbsp; </div>");// sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");

            // sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
            //sb.Append(" <div id=\"TagsVer"+tag.TagId+"\" class=\"clickableVertical\"> <a id=\"" + tag.TagId + "\" href=\"Ads.aspx?tagCloud="+tag.TagId+"\" class=\"TagsNew\"> " + tag.TagName + "</a></div>&nbsp;&nbsp;");
            //sb.Append("<div class=\"spaces\">&nbsp; </div>");
            count++;
        }
        lblTagVertical.Text += "    " + sb.ToString();
    }
    private void loadLocalCities()
    {
        AdsRepository repoAds = new AdsRepository();
        string[] locationVals = Convert.ToString(Session[clsSession.LocationIds]).Split(',');
        List<Ads> lst = repoAds.getCitiesForAds(Convert.ToInt32(locationVals[0]),Convert.ToInt32(locationVals[1]));
        ddllocalcities.DataSource = lst;
        ddllocalcities.DataTextField = "City";
        ddllocalcities.DataValueField = "CityCode";
        ddllocalcities.DataBind();
        ddllocalcities.Items.Insert(0, new ListItem("Choosee---", "-1"));
    }
    //private void loadAds()
    //{
    //    AdsRepository adsObj = new AdsRepository();
    //    List<string> lst = new List<string>();
    //    lst = adsObj.LoadTags();
    //    int length = lst.Count;
    //    StringBuilder sb = new StringBuilder();

    //    foreach (string tag in lst)
    //    {
    //        sb.Append("<div id=\"adstag\"><br /> <table class=\"auto-style1\"> <table class=\"auto-style1\"><tr><td colspan=\"2\" class=\"Title\"><span onclick=\"redirectToAd();\"></span>Wanted a lead singer (female) for progressive rock act</span></td>");
    //        sb.Append("<td class=\"Share\" rowspan=\"4\"> <br /><asp:Image ID=\"Image3\" class=\"imgForShare\" ImageUrl=\"~/Content/img/Facebook_icon.png\" runat=\"server\" />");
    //        sb.Append("<asp:Image ID=\"Image5\" class=\"imgForShare\" ImageUrl=\"~/Content/img/twitterlogo.png\"runat=\"server\" /><asp:Image ID=\"Image4\" class=\"imgForShare\" ImageUrl=\"~/Content/img/googleplus.png\" runat=\"server\" /></td>");
    //        sb.Append("</tr><tr><td class=\"Bandpic\" style=\"text-align: center\"><asp:Image ID=\"Image1\" Width=\"80px\" Height=\"80px\" runat=\"server\" ImageUrl=\"~/Content/profilepic/matt.png\" />");
    //        sb.Append("</td><td class=\"post\"><span class=\"auto-style5\">&nbsp;You are an artist. Most importantly original Style!</span><br class=\"auto-style5\" />");
    //        sb.Append("<span class=\"auto-style5\">Stage experience required, though, not necessary until you can convince us! Message us for further details  </span></td></tr>");
    //        sb.Append("<tr><td class=\"BandName\"><a href=\"ProfileView.aspx?id=4\" class=\"BandName\" title=\"View Profile\"><strong>Culture Amaglation</strong></a></td>");
    //        sb.Append(" <td class=\"btnPost\"><asp:Button ID=\"Button1\" class=\"PostbtnStyle\" runat=\"server\" Text=\"Post Reply\" Style=\"font-weight: 700; font-size: x-large\" /></td>");
    //        sb.Append("</tr><tr><td></td></tr><tr><td class=\"tagsn\" colspan=\"2\"><asp:Label ID=\"TagsToAdd\" runat=\"server\" Text=\"\"></asp:Label></td></tr></table><br /></div>");
    //    }
    //}
    //private void loadTags()
    //{
    //    AdsRepository adsObj = new AdsRepository();
    //    List<string> lst = new List<string>();
    //    lst = adsObj.LoadTags();
    //    int length = lst.Count;
    //    StringBuilder sb = new StringBuilder();
    //    int count = 0;
    //    foreach (string tag in lst)
    //    {
    //        if (count == 4)
    //        {
    //            TagsToAdd.Text += "    " + sb.ToString();
    //            return;
    //        }// sb.Append(" <div id=\"Tags\" runat=\"server\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a> </div>&nbsp;&nbsp;");
    //        sb.Append(" <div id=\"Tags\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.ToString() + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.ToString() + "</a><a id=\"" + tag.ToString() + "1\"  style=\"display:none;\" onclick=\"removetag(this.id)\" href=\"#\"><img src=\"Content/CloseTag.png\" alt=\"Remove this Tag\" /> </a> </div>&nbsp;&nbsp;");
    //        sb.Append("<div class=\"spaces\">&nbsp; </div>");
    //        count++;
    //    }
    //    TagsToAdd.Text += "    " + sb.ToString();
    //}
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {


        Label TagsToAdd = (Label)e.Item.FindControl("TagsToAdd");
        Label LBLSub = (Label)e.Item.FindControl("sub");
        Label LBLDescription = (Label)e.Item.FindControl("Label1");
        HyperLink LnkFB = (HyperLink)e.Item.FindControl("fb");
        HyperLink LnkTW = (HyperLink)e.Item.FindControl("tw");
        Image ImgProfile = (Image)e.Item.FindControl("Image7");

        if (ImgProfile != null)
        {
            if (ImgProfile.ImageUrl.Contains("facebook"))
            {
                ImgProfile.ImageUrl = ImgProfile.ImageUrl.Substring(19);
            }

            //else if (ImgProfile.ImageUrl == "")
            //    ImgProfile.ImageUrl = "Content/img/Default.jpg";

        }


        //if (ImgProfile != null)
        //{
        //    if (ImgProfile.ImageUrl.Length == 19)
        //        ImgProfile.ImageUrl = "~/Content/Img/Default.jpg";
        //}
        //string sub = "";
        //Image DefaultPic = (Image)e.Item.FindControl("Image1");
        //if (DefaultPic != null)
        //{
        //    DefaultPic.ImageUrl = "~/Content/MBandLogo.png";
        //}
        string sub = "";
        if (TagsToAdd != null)
        {
            if (LnkFB != null && LnkTW != null)
            {
                string title = "";
                string url;
                string dashtitle;
                sub = LBLSub.Text;
                if (LnkFB != null)
                {
                    //Facebook Share
                    title = LBLSub.Text;
                    dashtitle = title.Replace(' ', '*');
                    url = "http://meraband.com/adshareview.aspx?ShareEntityId=" + TagsToAdd.Text + "-" + dashtitle;
                    string summery = LBLDescription.Text;
                    string image = "http://www.meraband.com/content/img/img2.jpg";
                    string facebooklink = "http://www.facebook.com/sharer.php?s=100&p[title]=" + title;
                    facebooklink += "&p[summary]=" + summery;
                    facebooklink += "&p[url]=" + url;
                    facebooklink += "&p[images][0]=" + image;
                    LnkFB.NavigateUrl = facebooklink;
                    //Twitter Share
                    string twitterlink = "http://twitter.com/share?url=";
                    twitterlink += url;
                    twitterlink += "&text=" + sub;
                    twitterlink += "&via=Meraband";
                    LnkTW.NavigateUrl = twitterlink;
                }
            }

            Int64 tagId = Convert.ToInt64(TagsToAdd.Text);
            AdsRepository adsObj = new AdsRepository();
            List<Ads> lst = new List<Ads>();
            lst = adsObj.LoadTagsIdsByAd(tagId);
            int length = lst.Count;
            StringBuilder sb = new StringBuilder();

            foreach (Ads tag in lst)
            {
                sb.Append("<div id=\"TagsNewS\" class=\"clickable\" float=\"left\"> <a id=\"" + tag.TagId + "\" onclick=\"ShowAdbyTag(this.id);\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div>");
                sb.Append("<div id=\"TagsNewS\" class=\"clickable1\" float=\"left\">&nbsp;</div><div class=\"spaces\"> </div><div></div>");
            }
            TagsToAdd.Text = sb.ToString();
        }
    }
    protected void DataList1_DataBinding(object sender, EventArgs e)
    {

    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "PostMsg")
        {

            Int64 RecipeintId = Convert.ToInt64(e.CommandArgument);
            Int64 AdId = Convert.ToInt64(DataList1.DataKeys[e.Item.ItemIndex]);
            int MsgType = 1; // For Musician Ads
            // string UrlToRedirect = "SendMessage.aspx?EntityId=" + AdId + "&User=" + RecipeintId + "&CType=" + MsgType;
            if (Convert.ToString(Session[clsSession.AccId]) == "")
            {
                Response.Redirect(Redirect.GoToLogin());
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}', 'mywin', 'style=position:fixed; left=25, top=25, width=560, height=420, toolbar=no, scrollbars=yes');</script>", "SendMessage.aspx?EntityId=" + AdId + "&User=" + RecipeintId + "&CType=" + MsgType));

        }
    }
    protected void BtnShowTagged_Click(object sender, EventArgs e)
    {
        //Validate HdnTags and other values

        List<Ads> lst = new List<Ads>();
        AdsRepository adRepo = new AdsRepository();
        string[] locationVals = Convert.ToString(Session[clsSession.LocationIds]).Split(',');
        if (ddllocalcities.SelectedValue != "-1")
        {
            lst = adRepo.ViewAdsByAccountandTag(Convert.ToInt32(locationVals[0]), Convert.ToInt32(locationVals[1]), Convert.ToInt32(ddllocalcities.SelectedValue), Convert.ToInt64(Session[clsSession.AccId]), Convert.ToInt32(HdnTags.Value));
        }
        else
        {
            lst = adRepo.ViewAdsByAccountandTag(Convert.ToInt32(locationVals[0]), Convert.ToInt32(locationVals[1]), Convert.ToInt32(locationVals[2]), Convert.ToInt64(Session[clsSession.AccId]), Convert.ToInt32(HdnTags.Value));
        }
        DataList1.DataSource = lst;
        DataList1.DataBind();
    }
    protected void ddllocalcities_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddllocalcities.SelectedValue != "-1")
        {
            string[] locationVals = Convert.ToString(Session[clsSession.LocationIds]).Split(',');

            loadAds(Convert.ToInt32(locationVals[0]), Convert.ToInt32(locationVals[1]), Convert.ToInt32(ddllocalcities.SelectedValue));

        }
        else
        {
            Response.Write("<script>alert('Invalid City Selection!')</script>");
            return;
        }

    }
}