﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DAL.Repositories;
using BLL;
using System.Text;
using System.Text.RegularExpressions;

public partial class MyAds : System.Web.UI.Page
{
    AdsRepository repoAds;
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.GetPostBackEventReference(this, string.Empty);
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToHome());
        }
        if (!IsPostBack)
        {
            loadMyAds();
            loadObjectives();
        }

    }
    private void loadObjectives()
    {
        repoAds = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = repoAds.LoadObjectives();
        ddlObjective.DataSource = lst;
        ddlObjective.DataTextField = "PostObjective";
        ddlObjective.DataValueField = "ObjId";
        ddlObjective.DataBind();
    }
    private void loadMyAds()
    {
        StringBuilder sb = new StringBuilder();
        repoAds = new AdsRepository();
        List<Ads> lstMyAds = repoAds.ViewAddByAccount(Convert.ToInt64(Session[clsSession.AccId]));
        string title = "";
        string url;
        string dashtitle;
        string summery;
        string image;
        string facebooklink;
        string twitterlink;
        int adReplyCount = 0;
        foreach (Ads AdStr in lstMyAds)
        {
            adReplyCount = Convert.ToInt32(repoAds.getAdReplyCount(AdStr.AdId));
            //******Converting value returned in int64 to int32
            sb.Append("<div class=\"OuterPostDiv\">");
            if (adReplyCount > 1)
                sb.Append("        <div class=\"ReplyCountDiv\">99+<br />");
            else
                sb.Append("        <div class=\"ReplyCountDiv\">" + adReplyCount + "<br />");
            sb.Append("   Reply </div>");
            sb.Append("        <div class=\"ReplyCountDiv\">0<br />");
            sb.Append("   Share</div>");
            sb.Append(" <div class=\"AdDiv\">");
            sb.Append(" <div class=\"SubjectPostDiv\">" + AdStr.Subject + " </div>");
            sb.Append("  <div class=\"InnerContentDiv\">");
            sb.Append("  <div class=\"DescriptionDiv\">" + AdStr.Description + "</div>");
            sb.Append("  <div class=\"SocialLinks\">");
            //facebook Share Button
            //Facebook Share
            title = AdStr.Subject;
            dashtitle = title.Replace(' ', '*');
            url = "http://meraband.com/adshareview.aspx?ShareEntityId=" + AdStr.AdId + "-" + dashtitle;
            summery = AdStr.Description;
            image = "http://meraband.com/content/img/fb.png";
            facebooklink = "http://www.facebook.com/sharer.php?s=100&amp;p%5Btitle%5D=" + title;
            facebooklink += "&amp;p[summary]=" + summery;
            facebooklink += "&amp;p[url]=" + url;
            facebooklink += "&amp;&p[images][0]=" + image;
            sb.Append("<asp:HyperLink ID=\"fb\" runat=\"server\" Tilte=\"Share on Facebook\" NavigateUrl=\"" + facebooklink + "\" Target=\"_blank\"> <asp:Image ID=\"ImgFB\" class=\"imgForShare\" ImageUrl=\"~/Content/img/Facebook_icon.png\" runat=\"server\" Width=\"40px\" Height=\"40px\"/></asp:HyperLink>");
            //Twitter Share 
            twitterlink = "http://twitter.com/share?url=";
            twitterlink += url;
            twitterlink += "&text=" + AdStr.Subject;
            twitterlink += "&via=Meraband";
            sb.Append("<asp:HyperLink ID=\"tw\" runat=\"server\" Title=\"Share on Twitter\" NavigateUrl=\"" + twitterlink + "\" Target=\"_blank\"> <asp:Image ID=\"ImgTW\" class=\"imgForShare\" ImageUrl=\"~/Content/img/twitterlogo.png\" runat=\"server\" Width=\"40px\" Height=\"40px\"/></asp:HyperLink>");
            sb.Append("</div></div>");
            sb.Append("<div class=\"dateDiv\">" + AdStr.PostDate);
            sb.Append("</div>");
            sb.Append(" <div class=\"LowerDiv\">");
            sb.Append("<div class=\"btnDiv\">");
            sb.Append("  <input id=\"Button1\" type=\"button\" class=\"PostbtnStyle\" value=\"Edit\" onclick=\"DoEdit(" + AdStr.AdId + ");\"/>");
            sb.Append(" <input id=\"Button2\" type=\"button\" class=\"PostbtnStyle\" value=\"Delete\"  onclick=\"DoDelete(" + AdStr.AdId + ");\"/>");
            sb.Append("</div></div></div><div style=\"background-color:black; height:1px;\"></div></div>");

        }
        if (sb.ToString() != "")
            lblMyads.Text = sb.ToString();
        else
            lblMyads.Text = "There are no ads to be shown here";
    }
    protected void Button_Click(object sender, EventArgs e)
    {
        //Edit
        if (!Regex.IsMatch(HdnVal.Value.Trim(), @"^[0-9]{1,18}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        //Title and Description inserted as string in stored procedure
        repoAds = new AdsRepository();
        Ads objAds = new Ads();
        objAds.AdId = Convert.ToInt64(HdnVal.Value);
        objAds.Subject = title.Text;
        objAds.Description = description.Text;
        objAds.ObjId = Convert.ToInt32(ddlObjective.SelectedValue);
        Int64 ifInserted = repoAds.UpdateAd(objAds);
        if (ifInserted > 0)
        {
            loadMyAds();
            Response.Write("<script>alert('Ad Upated & Republished Successfully');</script>");

        }

    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        //Delete
        if (!Regex.IsMatch(HdnVal1.Value.Trim(), @"^[0-9]{1,18}$"))
        {
            Response.Write("<script>alert('Invalid Operation!')</script>");
            return;
        }
        repoAds = new AdsRepository();
        Int64 ifDeleted = repoAds.DeleteAd(HdnVal1.Value);
        if (ifDeleted > 0)
        {
            loadMyAds();
            Response.Write("<script>alert('Ad Deleted Successfully');</script>");

        }
    }
}