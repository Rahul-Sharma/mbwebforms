﻿<%@ Page Title="My Ads" Language="C#" MasterPageFile="~/UserMaster2.master" AutoEventWireup="true" CodeFile="MyAds.aspx.cs" Inherits="MyAds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Scripts/ajax.js"></script>
    <script src="Scripts/popup.js"></script>
    <script type="text/javascript">
        function DoEdit(id) {
            popup('popUpDiv');
            document.getElementById('imgLoad').style.display = '';
            document.getElementById('PostOuter').style.display = 'none';
            MakeAjaxRequest("ajaxHandlerads.aspx?EntityId=" + id);
                if (ajaxOutput != "*") {
                    document.getElementById('imgLoad').style.display = 'none';
                    document.getElementById('PostOuter').style.display = '';
                    var arr= ajaxOutput.split("-");
                    document.getElementById('<%=title.ClientID%>').value = arr[0];
                    document.getElementById('<%=description.ClientID%>').value = arr[1];
                    document.getElementById('<%=ddlObjective.ClientID%>').selectedIndex = arr[2];
                    document.getElementById('<%=HdnVal.ClientID%>').value = id;
                }
                else {
                    document.getElementById('PostOuter').style.display = 'none';
                    document.getElementById('Responsediv').style.display = 'none';
                    popup('popUpDiv');
                }
            }
        function DoDelete(id) {
            if (confirm('Do you really want to delete this ad?')) {
                document.getElementById('<%=HdnVal1.ClientID%>').value = id;
                __doPostBack('<%=btnDel.UniqueID%>', "");
            }
            }
            function CloseEdit() {
                document.getElementById('<%=title.ClientID%>').value = '';
                document.getElementById('<%=description.ClientID%>').value = '';
                document.getElementById('<%=ddlObjective.ClientID%>').selectedIndex = 0;
                popup('popUpDiv');
            }
    </script>
    <style type="text/css">
        .OuterPostDiv {
            width: 70%;
            margin: 0 auto;
            display: table;
        }

        .ReplyCountDiv {
            width: 9%;
            display: table-cell;
            float: left;
            font-size: x-large;
            text-align: center;
            background-color: #ff6a00;
            color: white;
            margin-left:3px;
            vertical-align:central;
            padding-top:4px;
            padding-bottom:4px;
        }

        .AdDiv {
            width: 80%;
            display: table-cell;
            float: left;
        }

        .SubjectPostDiv {
            width: 100%;
            height: auto;
            padding: 4px;
            font-size: x-large;
            margin: 0 auto;
            text-align: center;
            color: #ff6a00;
        }

        .InnerContentDiv {
            display: table;
            width: 100%;
            padding: 4px;
            
        }

        .DescriptionDiv {
            width: 80%;
            display: table-cell;
            float: left;
            text-align: center;
        }

        .SocialLinks {
            width: 20%;
            display: table-cell;
            float: left;
        }

        .LowerDiv {
            margin: 0 auto;
            padding: 4px;
            width: 100%;
        }

        .btnDiv {
            width: 100%;
            display: table-cell;
            float: left;
            text-align: center;
        }

        .dateDiv {
            width: 100%;
            text-align: right;
            float: right;
           
        }

        .imgForShare {
            width: 45px;
            height: 45px;
        }

        .PostbtnStyle {
            transition: all 0.3s linear;
            background-image: linear-gradient(to top right, #feda71 0%, #FDAD18 100%);
            margin: 0 auto;
            color: white;
            width: auto;
            vertical-align: middle;
            font-family: Verdana;
            height: 33px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            text-decoration-color: white;
            text-align: center;
        }
    </style>
    <style>
        #divOutterMyAds {
            width: 90%;
            margin: 0 auto;
            padding-top: 90px;
        }

        #divInnerAds {
            width: 100%;
            margin: 0 auto;
            padding: 4px;
        }

        #blanket {
            background-color: #111;
            opacity: 0.65;
            filter: alpha(opacity=65);
            position: absolute;
            z-index: 9001;
            top: 0px;
            left: 0px;
            width: 100%;
        }

        #popUpDiv {
            position: fixed;
            background-color: #dddddd;
            width: 600px;
            height: 350px;
            z-index: 9002;
            left: 0px;
            border: 3px solid #dddddd;
            padding: 10px 40px;
            background: #dddddd;
            border-radius: 25px;
            -moz-border-radius: 25px; /* Firefox 3.6 and earlier */
            box-shadow: 5px 5px 2px;
            box-shadow: #dddddd;
            margin:0 auto;
            text-align:center;
        }

        #PostOuter {
            width: 100%;
            text-align: center;
            margin: 0 auto;
        }

        #PostHeader {
            width: 100%;
            text-align: center;
            margin: 0 auto;
        }

        .ddl {
            padding: 12px 15px;
            /* Styles */
            background: #fff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 0 1px 0 rgba(0,0,0,0.2);
            cursor: pointer;
            outline: none;
            transition: all 0.3s ease-out;
        }

        .txtAreaFull {
            padding: 12px 15px;
            border: 3px;
            border-style: initial;
            border-color: #ff6a00;
            -khtml-border-radius: 5px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            font: italic 13px Georgia, "Times New Roman", Times, serif;
            outline: initial;
            padding: 5px;
            cursor: pointer;
            transition: all 0.3s ease-out;
        }
    </style>
    <asp:Button ID="btnDel" runat="server" Text="Meraband bajja Aur Barat" style="display:none;" OnClick="btnDel_Click"/>
    <asp:HiddenField ID="HdnVal" runat="server" />
     <asp:HiddenField ID="HdnVal1" runat="server" />
    <div id="blanket" style="display: none;">
    </div>
    <div id="popUpDiv" style="display: none;">
        <img id="imgLoad" src="Content/img/loading.gif" alt="Loading Messages. Please wait!" style="display: none; margin:0 auto; text-align:center;" />
        <div id="PostOuter" style="display: none;">
            <div id="PostHeader" style="font-size: x-large; padding: 5px; color: #ff6a00;">
                Edit Ad
            </div>
            <div id="titlehead" style="margin: 0 auto; padding: 5px;">Title</div>
            <div id="PostTitle">

                <asp:TextBox ID="title" runat="server" placeholder="" required="required" autofocus="autofocus" CssClass="txtAreaFull" Width="350px"></asp:TextBox>
            </div>
            <div id="Div1" style="margin: 0 auto; padding: 5px;">Description</div>
            <div id="PostDescription">
                <asp:TextBox ID="description" runat="server" placeholder="" TextMode="MultiLine" Rows="5" required="required" autofocus="autofocus" CssClass="txtAreaFull" Width="350px"></asp:TextBox>

            </div>
           
            <div id="PostObjective" style="padding:10px;">
                <asp:DropDownList ID="ddlObjective" runat="server" class="ddl" AppendDataBoundItems="true" CausesValidation="true">
                    <asp:ListItem Value="-1" Text="------------Please Select Meta Objective :-) "></asp:ListItem>
                </asp:DropDownList>

            </div>
            <div id="PostAddedTags" style="text-align:center; margin: 0 auto;">
                <asp:Label ID="lblAddedTags" runat="server" Text=""></asp:Label>
            </div>


            <div id="PostButton" style="padding: 8px;">
                <asp:Button ID="Button" runat="server" Text="Edti And Repost!" CssClass="btn" OnClick="Button_Click"></asp:Button>
                <asp:Button ID="Button1" runat="server" Text="Cancel!" CssClass="btn" OnClientClick="return CloseEdit();"></asp:Button>
            </div>
        </div>
    </div>
    <div id="divOutterMyAds">
        <div id="divInnerAds">
            <asp:Label ID="lblMyads" runat="server" Text=""></asp:Label>
        </div>

    </div>

</asp:Content>

