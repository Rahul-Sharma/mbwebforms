﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;
using DAL.Repositories;
using System.Text;
using System.IO;

public partial class EditProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToHome());
        }
        if (!IsPostBack)
        {
            loadTags();
            LoadProfile();
        }
    }

    private void loadAssignedTags()
    {
        ProfileRepository objPro = new ProfileRepository();
        List<Profile> lst = new List<Profile>();
        lst = objPro.loadTagIdForAccount(Convert.ToInt64(Session[clsSession.AccId]));
        StringBuilder sb = new StringBuilder();
        string id = "";
        string ids = "";
        int count = 0;
        foreach (Profile tagName in lst)
        {
            ids = "<div  id=\"filter" + Convert.ToString(tagName.TagId) + "\" class=\"filterDivs\">" + tagName.TagName + "<a id=\"" + tagName.TagId + "\"  onclick=\"removeFilter(this.id)\" href=\"#\"><img src=\"Content/cancel.png\" style=\"vertical-align:middle; padding-left:3px;\" Title=\"Remove\" alt=\"Remove \" /> </a> </div>";
            sb.Append(ids + " ");
            //sb.Append("<div class=\"filterDivs\">" + tagName + "</div>&nbsp;");
            //sb.Append("<div class=\"filterDivsSpace\">&nbsp;&nbsp;</div>");
            if (id == "")
                id = Convert.ToString(tagName.TagId);
            else
                id += "-" + Convert.ToString(tagName.TagId);
            hdnValue.Value = id;
            count++;
        }
        if (sb.ToString() != "")
        {
            lblAddedTags.Text = "  AddedTags :- " + sb.ToString();
            HdnCount.Value = Convert.ToString(count);
        }

    }
    private void loadTags()
    {
        AdsRepository adsObj = new AdsRepository();
        List<Ads> lst = new List<Ads>();
        lst = adsObj.LoadTagsIds();
        StringBuilder sb = new StringBuilder();

        foreach (Ads tag in lst)
        {
            sb.Append(" <div id=\"Tags\"  class=\"clickable\" float=\"left\"> <a id=\"" + tag.TagId + "\" onclick=\"Show(this.id)\" href=\"#\" class=\"TagsNew\"> " + tag.TagName + "</a></div>");
        }
        TagsToAdd.Text = "    " + sb.ToString();
    }
    private void LoadProfile()
    {
        //lblTags.Text = "<div class=\"filterDivs\"> </div>";
        Profile objPro = new Profile();
        ProfileRepository repoPro = new ProfileRepository();
        objPro = repoPro.ShowProfile(Convert.ToInt64(Session[clsSession.AccId]));
        //if (objPro.DisplayName == "")
        //{
        //    Response.Redirect("PageNotFound.aspx");
        //}
        HdnType.Value = Convert.ToString(objPro.ProfileType);// Used at the time of update
        if (objPro.ProfilePic != "")
        {
            hdnFunc.Value = objPro.ProfilePic;// Storing pic value in the Hidden Field , wasn't necessary though! (For deleting old file!)
            if (objPro.ProfilePic.Trim().Contains("facebook"))
            {
                picDefault.ImageUrl = objPro.ProfilePic.Substring(19);
            }
            picDefault.ImageUrl = "Content/profilepic/" + objPro.ProfilePic;
        }
        else
        {
            hdnFunc.Value = "";// Storing vaue in the Hidden Field , wasn't necessary though! 
            picDefault.ImageUrl = "~/Content/img/googleplus.png";
        }
        title.Value = objPro.DisplayName;// Name
        txtDescription.Value = objPro.Description;
        lblSoundFrame.Text = objPro.SoundCloudFrame;
        lblYoutubeFrame.Text = objPro.YoutubeFrame;
        txtFBId.Value = objPro.Facebook;
        txtTWId.Value = objPro.Twitter;
        loadAssignedTags();
    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {
        if (Name.Text == "")
        {
            Response.Write("<script>alert('Sorry, name can not be blank!')</script>");
            return;
        }
        if (picUpload.HasFile)
        {
            if (picUpload.PostedFile.ContentLength > 100000)
            {
                Response.Write("<script>alert('Sorry, Image can not be greater than 100KB! Resize the image and try again!')</script>");
                return;
            }
            int correct = checkUploadedFile();
            if (correct == 0)
            {
                Response.Write("<script>alert('Sorry, but this is not actually a .jpg file!')</script>");
                return;
            }
        }
        //Validate Hdn fields
        UpdateProfile();

    }

    private void UpdateProfile()
    {
        try
        {
            ProfileRepository objRepo = new ProfileRepository();
            Profile obj = new Profile();
            obj.DisplayName = title.Value;
            //Checking for file
            if (picUpload.HasFile)
            {

                string fileName = System.Guid.NewGuid().ToString() + ".jpg";
                picUpload.SaveAs(Server.MapPath("Content/profilepic/" + fileName));
                obj.ProfilePic = fileName;
                DeleteFileFromFolder(hdnValue.Value); // Deleting old file

            }
            else
            {
                if (hdnFunc.Value != "")
                {
                    obj.ProfilePic = hdnFunc.Value; // Same file being updated
                }
                else
                {
                    obj.ProfilePic = ""; // In case neither file selected
                }
            }
            if (chkSoundCloud.Checked)
                obj.SoundCloudFrame = txtsoundcloudframe.Value;
            else
                obj.SoundCloudFrame = lblSoundFrame.Text;
            if (chkYoutube.Checked)
                obj.YoutubeFrame = txtYoutubeFrame.Value;
            else
                obj.YoutubeFrame = lblYoutubeFrame.Text;
            obj.Description = txtDescription.Value;
            obj.Facebook = txtFBId.Value;
            obj.Twitter = txtTWId.Value;
            obj.AccountId = Convert.ToInt64(Session[clsSession.AccId]);
            Int64 statusUpdate = objRepo.UpdateProfile(obj);
            if (statusUpdate > 0)
            {
                UpdateTags();// Updating New Tags
                Session[clsSession.HasName] = 1;
                Response.Redirect(Redirect.GoToEditProfilePage());// AccountId
            }
        }
        catch (System.Threading.ThreadAbortException)
        {
            Response.Redirect(Redirect.GoToCurrentProfilePage());// AccountId
            // do nothing

        }
       
    }
    private void UpdateTags()
    {
        String[] tagsArray = hdnValue.Value.Split('-');
        //First delete old tags
        ProfileRepository repoObj = new ProfileRepository();
        int xReturn = repoObj.deleteTagsByAccount(Convert.ToInt64(Session[clsSession.AccId]));// AccountId
        if (xReturn == -1)
        { // Now Insert new(edited) Tags
            for (int i = 0; i < tagsArray.Length; i++)
            {
                if (tagsArray[i] != "s" && tagsArray[i] != "")
                {
                    repoObj.insertTagsByAccountId(Convert.ToInt64(Session[clsSession.AccId]), Convert.ToInt32(tagsArray[i]));
                }
            }
        }
    }
    private int checkUploadedFile()
    {
        Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
        imageHeader.Add("jpg", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
        byte[] InitialBytesOfFile;
        string filext = picUpload.PostedFile.FileName.Substring(picUpload.PostedFile.FileName.LastIndexOf('.') + 1).ToLower();
        byte[] tmp = imageHeader[filext];
        InitialBytesOfFile = new byte[tmp.Length];
        picUpload.FileContent.Read(InitialBytesOfFile, 0, InitialBytesOfFile.Length);
        if (CompareArray(tmp, InitialBytesOfFile))
            return 1; // file is of correct format
        else
            return 0; // file is not of correct format
    }
    private bool CompareArray(byte[] a1, byte[] a2)
    {
        if (a1.Length != a2.Length) return false;
        for (int i = 0; i < a1.Length; i++)
        {
            if (a1[i] != a2[i])
            {
                return false;
            }
        }
        return true;
    }
    public void DeleteFileFromFolder(string StrFilename)
    {

        string strPhysicalFolder = Server.MapPath("Content/profilepic/");

        string strFileFullPath = strPhysicalFolder + StrFilename;

        if (System.IO.File.Exists(strFileFullPath))
        {
            System.IO.File.Delete(strFileFullPath);
        }

    }



}