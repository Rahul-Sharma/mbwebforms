﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using DAL;
using DAL.Repositories;
using System.Collections;
using System.Collections.Generic;
using BLL;
using System.Text;

public partial class AjaxHandler : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Cid"] != null && Convert.ToString(Request.QueryString["event"]) == "1")
        {
            GetConversationThread(Convert.ToInt64(Request.QueryString["Cid"]));
        }
        if (Request.QueryString["Cid"] != null && Convert.ToString(Request.QueryString["event"]) == "2")
        {
            GetMsgsForAd(Convert.ToInt64(Request.QueryString["Cid"]));
        }
        if (Request.QueryString["Cid"] != null && Convert.ToString(Request.QueryString["event"]) == "3")
        {
            GetSentMsgbyaccountId(Convert.ToInt64(Request.QueryString["Cid"]));
        }
       
    }
   
    private void GetSentMsgbyaccountId(Int64 p)
    {
        string str = "*";
        MessagesRepository msgObj = new MessagesRepository();
        List<MessageEntity> lstObj = new List<MessageEntity>();
        lstObj = msgObj.LoadSent(Convert.ToInt64(Session[clsSession.AccId]));
        StringBuilder sb = new StringBuilder();
        foreach (MessageEntity msgAttr in lstObj)
        {
            sb.Append(" <a href=\"#\" id=\"" + msgAttr.ConversationId + "\" class=\"ClickMsgs\" onclick=\"return OpenMsgThread(this.id);\">");
            sb.Append("<div id=\"OuterShell\">");
            sb.Append("<div id=\"MsgTop\">");
            sb.Append("<div id=\"PicAreaMsg\">");
            sb.Append("<img src=\"Content/profilepic/" + msgAttr.ProfilePic + "\" alt=\"" + msgAttr.BandName + "\"  Width=\"45px\" Height=\"45px\" />");
            // sb.Append("<asp:Image ID=\"Image1\" runat=\"server\" ImageUrl=\"~/Content/profilepic/"+msgAttr.ProfilePic+"\" Width=\"45px\" Height=\"45px\" />");
            sb.Append("</div>");
            sb.Append("<div id=\"MsgContent\">");
            if (msgAttr.MsgBody.Length < 15)
                sb.Append(msgAttr.MsgBody.Substring(0, 3) + "..........");
            else
                sb.Append(msgAttr.MsgBody.Substring(0, 3) + "..........");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div id=\"OuterMsgFooter\">");
            sb.Append("<div id=\"BandNameMsg\">");
            sb.Append(msgAttr.BandName);
            sb.Append("</div>");
            sb.Append("<div id=\"DateMsg\">");
            sb.Append(msgAttr.DisplayDate);
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</a>");
        }
        if (sb.ToString() != "")
            str = sb.ToString();
        Response.Write(str);
        Response.End();

    }

    private void GetConversationThread(Int64 p)
    {

        string str = "*";

        MessagesRepository msgrepoObj = new MessagesRepository();
        List<MessageEntity> lst = new List<MessageEntity>();
        lst = msgrepoObj.OpenMsgThreadbyConversationId(p);
        StringBuilder sb = new StringBuilder();
        foreach (MessageEntity msg in lst)
        {
            if (msg.SentByAccountId == Convert.ToInt64(Session[clsSession.AccId]))
            {
                sb.Append(" <div class=\"MsgBubble\">");
                sb.Append("<div class=\"triangle-right\">" + msg.MsgBody + "</div>");
                sb.Append("<div class=\"picSetMy\">");
                sb.Append("<img src=\"Content/profilepic/" + msg.ProfilePic + "\" alt=\"" + msg.BandName + "\"  Width=\"45px\" Height=\"45px\" />&nbsp;<span class=\"ptext\"><bold>Me </bold>&nbsp; &nbsp;(&nbsp;" + msg.DisplayDate + ")</span>");
                //   sb.Append("<asp:Image ID=\"Imghh\" runat=\"server\" ImageUrl=\"Content/profilepic/" + msg.ProfilePic + "\" Width=\"45px\" Height=\"45px\" />");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<div class=\"MsgBubbleHimHer\">");
                sb.Append("<div class=\"picSetHimHer\">");
                sb.Append("<span class=\"ptext\">  <bold>" + msg.BandName + "</bold> &nbsp; &nbsp;(&nbsp;" + msg.DisplayDate + ")</span>&nbsp;" + "<img src=\"Content/profilepic/" + msg.ProfilePic + "\" alt=\"" + msg.BandName + "\"  Width=\"45px\" Height=\"45px\" />");
                //  sb.Append("<asp:Image ID=\"Image8\" runat=\"server\" ImageUrl=\"Content/profilepic/"+msg.ProfilePic+"\" Width=\"45px\" Height=\"45px\" />");
                sb.Append("</div>");
                sb.Append("<div class=\"triangle-right top\">" + msg.MsgBody + "</div>");
                sb.Append("</div>");
            }
        }
        if (sb.ToString() != "")
            str = sb.ToString();
        Response.Write(str);
        Response.End();
       

    }
    private void GetMsgsForAd(Int64 p)
    {
        string str = "*";
        MessagesRepository msgObj = new MessagesRepository();
        List<MessageEntity> lstObj = new List<MessageEntity>();
        lstObj = msgObj.getMsgsByAd(p, Convert.ToInt64(Session[clsSession.AccId]));

        StringBuilder sb = new StringBuilder();
        foreach (MessageEntity msgAttr in lstObj)
        {
            sb.Append(" <a href=\"#\" id=\"" + msgAttr.ConversationId + "\" class=\"ClickMsgs\" onclick=\"return OpenMsgThread(this.id);\">");
            sb.Append("<div id=\"OuterShell\">");
            sb.Append("<div id=\"MsgTop\">");
            sb.Append("<div id=\"PicAreaMsg\">");
            sb.Append("<img src=\"Content/profilepic/" + msgAttr.ProfilePic + "\" alt=\"" + msgAttr.BandName + "\"  Width=\"45px\" Height=\"45px\" />");
            // sb.Append("<asp:Image ID=\"Image1\" runat=\"server\" ImageUrl=\"~/Content/profilepic/"+msgAttr.ProfilePic+"\" Width=\"45px\" Height=\"45px\" />");
            sb.Append("</div>");
            sb.Append("<div id=\"MsgContent\">");
            if (msgAttr.MsgBody.Length < 15)
                sb.Append(msgAttr.MsgBody.Substring(0, 3) + "..........");
            else
                sb.Append(msgAttr.MsgBody.Substring(0, 3) + "..........");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div id=\"OuterMsgFooter\">");
            sb.Append("<div id=\"BandNameMsg\">");
            sb.Append(msgAttr.BandName);
            sb.Append("</div>");
            sb.Append("<div id=\"DateMsg\">");
            sb.Append(msgAttr.DisplayDate);
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</a>");
        }
        if (sb.ToString() != "")
            str = sb.ToString();
        Response.Write(str);
        Response.End();

    }

}
