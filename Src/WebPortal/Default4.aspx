﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default4.aspx.cs" Inherits="Default4" %>
<!doctype=html>
<html>
    <head>
    <title></title>

        <style type="text/css">
            .OuterPostDiv {
            width:70%;
            margin:0 auto;
            display:table;
            }
            .ReplyCountDiv {
                  width:15%;
            display:table-cell;
            float:left;
            font-size:xx-large;
            text-align:center;
            background-color:#ff6a00;
            color:white;
            }
            .AdDiv {
                  width:85%;
            display:table-cell;
            float:left;
            }
            .SubjectPostDiv {
            width:100%;
            height:auto;
            padding:4px;
            font-size:x-large;
            margin:0 auto;
            text-align:center;
            color:#ff6a00;
            }
            .InnerContentDiv {
            display:table;
            width:100%;
            padding:4px;
            }
            .DescriptionDiv {
            width:85%;
            display:table-cell;
            float:left;
            text-align:center;
            }
            .SocialLinks {
             width:15%;
            display:table-cell;
            float:left;
            }
            .LowerDiv {
            margin:0 auto;
            padding:4px;
            width:100%;
          
            }
            .btnDiv {
             width:100%;
            display:table-cell;
            float:left;
            text-align:center;
           
            }
            .dateDiv {
             width:100%;
          
            float:right;
             
            }
             .imgForShare {
                width: 45px;
                height: 45px;
            }
             
            .PostbtnStyle {
                transition: all 0.3s linear;
                background-image: linear-gradient(to top right, #feda71 0%, #FDAD18 100%);
                margin: 0 auto;
                color: white;
                width: auto;
                vertical-align: middle;
                font-family: Verdana;
                height: 33px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                text-decoration-color: white;
                text-align: center;
            }
        </style>
        </head>
<body>

    <div class="OuterPostDiv">
        <div class="ReplyCountDiv">1+<br />
            Reply
        </div>
        <div class="AdDiv">
        <div class="SubjectPostDiv">My Bloody Valentine </div>
        <div class="InnerContentDiv">
            <div class="DescriptionDiv">This is just a test</div>
              <div class="SocialLinks">
                     <asp:HyperLink ID="fb" runat="server" Tilte="Share on Facebook" Target="_blank"> <asp:Image ID="ImgFB" class="imgForShare" ImageUrl="~/Content/img/Facebook_icon.png" runat="server" Width="40px" Height="40px"/></asp:HyperLink>
                    <asp:HyperLink ID="tw" runat="server" Title="Share on Twitter" Target="_blank"> <asp:Image ID="ImgTW" class="imgForShare" ImageUrl="~/Content/img/twitterlogo.png" runat="server" Width="40px" Height="40px"/></asp:HyperLink>
              </div>
            </div>
             <div class="dateDiv">

                </div>
            <div class="LowerDiv">
                <div class="btnDiv">
                   <input id="Button1" type="button" class="PostbtnStyle" value="Edit" onclick="DoEdit();"/>
                    <input id="Button2" type="button" class="PostbtnStyle" value="Delete"  onclick="DoDelete();"/>
                </div>
               
            </div>
       
        </div>
    </div>

</body>
</html>

<script type="text/javascript">
    function DoEdit() {
        alert('Edit');
    }
    function DoDelete() {
        alert('Delete');
    }
</script>
