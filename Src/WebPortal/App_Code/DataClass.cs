﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

/// <summary>
/// Summary description for DataClass
/// </summary>
/// 


public class DataClass
{
    public DataClass()
    {
    }
    /// <summary>
    ///  return rows depend on position
    ///  if you need 10th to 20th you need to pass start=10 and end=20
    /// </summary>
    /// <param name="start">database start position of one row</param>
    /// <param name="next">database end position of one row</param>
    /// <returns></returns>
    public string GetAjaxContent(int start, int end, Int64 accId,int countryCode, int stateCode, int cityCode)
    {
        string result = string.Empty;
        //adding sp params with values in Dictionary entry.
        Dictionary<string, object> keyValPair = new Dictionary<string, object>();
        keyValPair.Add("@start", start);
        keyValPair.Add("@next", end);
        keyValPair.Add("@AccountId", accId);
        keyValPair.Add("@CountryCode", countryCode);
        keyValPair.Add("@StateCode", stateCode);
        keyValPair.Add("@CityCode", cityCode);
        DBHelper DBHelper = new DBHelper();
        //passing the Stored Procedure name and keyvalue pair
        DataTable dataTable = DBHelper.GetTable("GetAllAdsbyAccId", keyValPair);
        if (dataTable.Rows.Count > 0)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                result += string.Format(@"<tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td style='width:50px;'>{0}</td><td style='width:400px;'>{1}</td><td style='width:150px;'>{2}</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                   </tr>", dataTable.Rows[i][0].ToString(), dataTable.Rows[i][1].ToString(), dataTable.Rows[i][2].ToString(), dataTable.Rows[i][3].ToString(),
                                                          dataTable.Rows[i][4].ToString(), dataTable.Rows[i][5].ToString(), dataTable.Rows[i][6].ToString(), dataTable.Rows[i][7].ToString());
            }

        }
        //this string is going to append on Datalist on client.
        return result;
    }
    /// <summary>
    /// function to bind data on page load
    /// </summary>
    /// <returns></returns>
    public DataTable FirstTenRecords(Int64 accId, int countrycode, int statecode, int citycode)
    {
        Dictionary<string, object> keyValPair = new Dictionary<string, object>();
        keyValPair.Add("@start", 0);
        keyValPair.Add("@next", 10);
        keyValPair.Add("@AccountId", accId);
        keyValPair.Add("@CountryCode", countrycode);
        keyValPair.Add("@StateCode", statecode);
        keyValPair.Add("@CityCode", citycode);
        DBHelper DBHelper = new DBHelper();
        DataTable dataTable = DBHelper.GetTable("GetAllAdsbyAccId", keyValPair);
        return dataTable;
    }
}

/// <summary>
/// return sqlconnection string formweb.config file
/// </summary>
public class Provider
{
    public static SqlConnection GetConnection()
    {
        return new SqlConnection(WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString);
    }
}
/// <summary>
/// Data layer
/// </summary>
public class DBHelper
{
    public DBHelper()
    { }

    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter adapter;
    public DataTable GetTable(string SPName, Dictionary<string, object> SPParamWithValues)
    {
        DataTable dataTable = new DataTable();
        try
        {
            con = Provider.GetConnection();
            //open DB connection
            con.Open();
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.CommandText = SPName;
            foreach (KeyValuePair<string, object> paramValue in SPParamWithValues)
            {
                cmd.Parameters.AddWithValue(paramValue.Key, paramValue.Value);
            }
            adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dataTable);
        }
        finally
        {
            //close connection string
            con.Close();
        }
        return dataTable;
    }
}