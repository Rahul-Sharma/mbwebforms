﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Random15
/// </summary>
public class Random15
{
    public static Int64 generateNum()
    {
        Random rd = new Random();
        int numPivotStart = rd.Next(0, 9);
        int numPvotEnd = rd.Next(0, 9);
        int[] numRangeStart = { 1003466, 1034999, 8753421, 9549246, 1000076, 4900099, 1100076, 6700001, 9000004, 2000065 };
        int[] numRangeEnd = { 980008044, 680020001, 200750010, 100070120, 800436000, 300065000, 900020065, 700869738, 400700036, 500075139 };
        // Depeding on respective pivot values start and end dimension values are selected
        Int64 generatedNum = rd.Next(numRangeStart[numPivotStart], numRangeEnd[numPvotEnd]); // From Start and End
        generatedNum = generatedNum + DateTime.Now.Millisecond; // Radomizing!
        // Now this "generatedNum" can be of length 7 8, 9 or 10
        // Therefore, to keep values to a min and max length of 15, we define
        int numSuffix2 = rd.Next(10, 99);
        int numPrefix3 = rd.Next(100, 111);
        int numSuffix3 = rd.Next(100, 999);
        int numPrefix4 = rd.Next(1000, 1111); // series will start with 1; later can be started with 2
        int numSuffix4 = rd.Next(1000, 9999); // series will start  
        int numPrefix5 = rd.Next(10000, 11111);
        //Taking this string for concatenation
        string strConcatNum = "";
        if (generatedNum.ToString().Length == 7)
        {
            strConcatNum = numPrefix5.ToString() + generatedNum.ToString() + numSuffix3.ToString();
        }
        else if (generatedNum.ToString().Length == 8)
        {
            strConcatNum = numPrefix4.ToString() + generatedNum.ToString() + numSuffix3.ToString();
        }
        else if (generatedNum.ToString().Length == 9)
        {
            strConcatNum = numPrefix3.ToString() + generatedNum.ToString() + numSuffix3.ToString();
        }
        else // that is length is 10
        {
            strConcatNum = numPrefix3.ToString() + generatedNum.ToString() + numSuffix2.ToString();
        }
        // finally converting back to int64
        generatedNum = Convert.ToInt64(strConcatNum.Trim());
        return generatedNum;
    }

    public Random15()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}