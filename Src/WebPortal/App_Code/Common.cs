﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Common
/// </summary>
public static class CommonFunc
{
    
    public static string GenerateRandom()
    {
        string allowedChars = "a,pq,b,c,d,e,f,5,g,h,j,k,m,n,p,q,r,s,67,t,u,v,91,w,x,y,z,";
        allowedChars += "A,B,C,D,E,F,G,H,J,K,L,M,N,3,5,2,9,P,Q,R,S,T,U,V,W,X,Y,Z,4,";
        allowedChars += "2,3,4,5,6,7,8,9,0";

        char[] sep ={ ',' };
        string[] arr = allowedChars.Split(sep);

        string randomString = "";
        string temp;
        Random rand = new Random();
        for (int i = 0; i < 10; i++)
        {
            temp = arr[rand.Next(0, arr.Length)];
            randomString += temp;
        }
        return randomString;
    }
}