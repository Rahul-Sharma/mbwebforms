using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

/// <summary>
/// Summary description for Security
/// </summary>
public class Security
{

	public Security()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //public static string RandomString(int size, bool lowerCase)
    //{
    //    //StringBuilder builder = new StringBuilder();
    //    //Random random = new Random();
    //    //char ch;
    //    //for (int i = 0; i < size; i++)
    //    //{
    //    //    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
    //    //    builder.Append(ch);
    //    //}
    //    //if (lowerCase)
    //    //    return builder.ToString().ToLower();
    //    //return builder.ToString();
    //}
    
    public static void Init()
    {
        //string value = null;
        // value = RandomString(10, true);
        //HttpCookie MyCookie = new HttpCookie("ASPFIXATION");
        //MyCookie.Value = value;
        //HttpContext.Current.Response.Cookies.Add(MyCookie);
        //HttpContext.Current.Session[clsSession.ASPFIXATION] = value;
    }

    public static void RemoveCache()
    {
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.CacheControl = "no-cache";
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.AppendHeader("Cache-Control", "no-cache");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "private");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "no-store");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "must-revalidate");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "max-stale=0");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "post-check=0");
        HttpContext.Current.Response.AppendHeader("Cache-Control", "pre-check=0");
        HttpContext.Current.Response.AppendHeader("Pragma", "no-cache");
        HttpContext.Current.Response.AppendHeader("Keep-Alive", "timeout=3, max=993");
        HttpContext.Current.Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
 }
}
