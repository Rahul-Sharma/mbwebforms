﻿
       function EnableReply() {
         

           document.getElementById('<%=txtreply.ClientID%>').style.display = '';
           document.getElementById('<%=btnMsgReply.ClientID%>').style.display = 'none';
           document.getElementById('<%=btnReply.ClientID%>').style.display = '';
           return false;

       }

function ShowInbox() {
    document.getElementById('<%=HndCId.ClientID%>').value = '';
    //So that function can't be called

    document.getElementById('Responsediv').style.display = 'none';
    document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = '';
    document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
    document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
    document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c))';
    document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#AC822D, #91663c)';
    document.getElementById('Inbox12').style.background = '--o-linear-gradient(#AC822D, #91663c)';
    document.getElementById('Inbox12').style.background = 'linear-gradient(#AC822D, #91663c)';
    document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
    document.getElementById('Sent12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
    document.getElementById('Sent12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
    document.getElementById('Sent12').style.background = 'linear-gradient(#feda71, #febb4a)';
}
function ShowSent() {
    document.getElementById('<%=HndCId.ClientID%>').value = '';
    //So that function can't be called

    document.getElementById('<%=lblSent.ClientID%>').style.display = '';
    document.getElementById('Responsediv').style.display = 'none';
    document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblSent.ClientID%>').style.display = '';
    document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
    document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#AC822D), to(#91663c))';
    document.getElementById('Sent12').style.background = '-moz-linear-gradient(#AC822D, #91663c)';
    document.getElementById('Sent12').style.background = '--o-linear-gradient(#AC822D, #91663c)';
    document.getElementById('Sent12').style.background = 'linear-gradient(#AC822D, #91663c)';
    document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
    document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
    document.getElementById('Inbox12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
    document.getElementById('Inbox12').style.background = 'linear-gradient(#feda71, #febb4a)';

    document.getElementById('imgLoad').style.display = '';
    document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
    document.getElementById('Responsediv').style.display = 'none';


    MakeAjaxRequest("ajaxHandler.aspx?Cid=1&event=3");
    document.getElementById('imgLoad').style.display = 'none';
    if (ajaxOutput != "*") {
        document.getElementById('<%=lblSent.ClientID%>').innerHTML = ajaxOutput;

    }
    else {
        document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = '';
        document.getElementById('InnerMsgHolder').style.color = '#000000';
        document.getElementById('<%=lblSent.ClientID%>').innerHTML = 'No messages for this ad!';
    }
    document.getElementById('<%=lblSent.ClientID%>').style.display = '';
    document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
    document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';
}


function OpenMsgThread(Cid) {

    document.getElementById('imgLoad').style.display = '';
    document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
    document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
    document.getElementById('Responsediv').style.display = '';
    if (Cid != -1 && Cid != '') {
        MakeAjaxRequest("ajaxHandler.aspx?Cid=" + Cid + "&event=1");
        if (ajaxOutput != "*") {
            document.getElementById('imgLoad').style.display = 'none';
            document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = 'none';
            document.getElementById('<%=lblMsgThread.ClientID%>').style.display = '';
            document.getElementById('<%=btnMsgReply.ClientID%>').style.display = '';
            document.getElementById('<%=lblMsgThread.ClientID%>').innerHTML = ajaxOutput;
            document.getElementById('<%=HndCId.ClientID%>').value = Cid;
        }
        else {

        }
    }
    else {

    }
}
function ShowAdMsgs(entityId) {

    document.getElementById('<%=HndCId.ClientID%>').value = '';
    //So that function can't be called

    document.getElementById('imgLoad').style.display = '';
    document.getElementById('<%=txtreply.ClientID%>').style.display = 'none';
    document.getElementById('Responsediv').style.display = 'none';
    document.getElementById('<%=btnReply.ClientID%>').style.display = 'none';
    if (entityId != -1 && entityId != '')
    {
        MakeAjaxRequest("ajaxHandler.aspx?Cid=" + entityId + "&event=2");
        document.getElementById('imgLoad').style.display = 'none';
        if (ajaxOutput != "*") {
            document.getElementById('<%=lblAdMsgs.ClientID%>').innerHTML = ajaxOutput;
        }
        else {
            document.getElementById('InnerMsgHolder').style.color = '#000000';
            document.getElementById('<%=lblAdMsgs.ClientID%>').innerHTML = 'No messages for this ad!';

        }
        document.getElementById('<%=lblSent.ClientID%>').style.display = 'none';
        document.getElementById('<%=lblAdMsgs.ClientID%>').style.display = '';
        document.getElementById('<%=lblMsgsInbox.ClientID%>').style.display = 'none';
        document.getElementById('<%=lblMsgThread.ClientID%>').style.display = 'none';

        document.getElementById('Inbox12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
        document.getElementById('Inbox12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
        document.getElementById('Inbox12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
        document.getElementById('Inbox12').style.background = 'linear-gradient(#feda71, #febb4a)';
        document.getElementById('Sent12').style.background = '-webkit-gradient(linear, 0 0, 0 100%, from(#feda71), to(#febb4a))';
        document.getElementById('Sent12').style.background = '-moz-linear-gradient(#feda71, #febb4a)';
        document.getElementById('Sent12').style.background = '--o-linear-gradient(#feda71, #febb4a)';
        document.getElementById('Sent12').style.background = 'linear-gradient(#feda71, #febb4a)';
    }
    else {

    }
}
