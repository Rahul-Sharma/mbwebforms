﻿/* In Combination with popup.css */

function toggle(div_id) {

    var el = document.getElementById(div_id);

    if (el.style.display == 'none') { el.style.display = 'block'; }

    else { el.style.display = 'none'; }

}

function blanket_size(popUpDivVar) {

    if (typeof window.innerWidth != 'undefined') {

        viewportheight = window.innerHeight;

    } else {

        viewportheight = document.documentElement.clientHeight;

    }

    if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {

        blanket_height = viewportheight;

    } else {

        if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {

            blanket_height = document.body.parentNode.clientHeight;

        } else {

            blanket_height = document.body.parentNode.scrollHeight;

        }

    }

    var blanket = document.getElementById('blanket');

    blanket.style.height = blanket_height + 'px';

    var popUpDiv = document.getElementById(popUpDivVar);

    //popUpDiv_height=blanket_height/2-150;//150 is half popup's height

    popUpDiv.style.top = '100px';

    //popUpDiv.style.top = 50popUpDiv_height + 'px';

}

function window_pos(popUpDivVar) {

    if (typeof window.innerWidth != 'undefined') {

        viewportwidth = window.innerHeight;

    } else {

        viewportwidth = document.documentElement.clientHeight;

    }

    if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {

        window_width = viewportwidth;

    } else {

        if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {

            window_width = document.body.parentNode.clientWidth;

        } else {

            window_width = document.body.parentNode.scrollWidth;

        }

    }

    var popUpDiv = document.getElementById(popUpDivVar);

    if (popUpDiv.id == 'popUpDiv')

        window_width = window_width / 2 - 300; //300 is half popup's width

    if (popUpDiv.id == 'popUpDiv3')

        window_width = window_width / 2 - 200;

    if (popUpDiv.id == 'popUpDiv1')

        window_width = window_width / 2 - 210; //175 is half popup1's width

    if (popUpDiv.id == 'popUpDiv4')

        window_width = window_width / 2 - 135; //175 is half popup1's width

    if (popUpDiv.id == 'popUpDiv6')

        window_width = window_width / 2 - 150; //175 is half popup1's width

    if (popUpDiv.id == 'popUpDivEmp')

        window_width = window_width / 2 - 235; //235 is half popUpDivEmp's width

    if (popUpDiv.id == 'popUpDPREdit')

        window_width = window_width / 2 - 230; //230 is half popUpDPREdit's width

    popUpDiv.style.left = window_width + 'px';

}

function popup(windowname) {

    blanket_size(windowname);

    window_pos(windowname);

    toggle('blanket');

    toggle(windowname);
}