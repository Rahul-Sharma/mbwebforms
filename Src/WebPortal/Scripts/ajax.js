﻿// JScript File
//-------------Ajax Functionlality------------------------------
var XmlHttp;
var ajaxOutput;
function MakeAjaxRequest(url) 
{
   ajaxOutput="";
   XmlHttp = null;
   if (window.XMLHttpRequest) {  
      // Mozilla, Safari,...
      XmlHttp = new XMLHttpRequest();
      if (XmlHttp.overrideMimeType)  XmlHttp.overrideMimeType('text/xml');
    } 
    else if (window.ActiveXObject) 
    { // IE
      try { XmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); } 
      catch (e) { try { XmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } 
      catch (e) {}}
    }
    //Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XmlHttp != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
    if (!XmlHttp) 
    {
       alert('Cannot create XMLHTTP instance, Please use another Browser');
       return false;
    }
    var d=new Date();
    url=url+"&nTime="+d.getTime();
    XmlHttp.open('GET', url , false /*Syncronus Request, Wait For its respone*/);
    XmlHttp.send(null); /* Send Request to Server */
   //ReadyStateChange Event is not handling in mozila browser 
   //so following code is replaced instead of ready state change 
   //Instead you must assume that the synchronous send has completed and instead check the status and process 
   //the response immediately.This means you do not need to define an onReadyStateChange function at all . 
    if (XmlHttp.readyState == 4) 
    {
         if (XmlHttp.status == 200) ajaxOutput=XmlHttp.responseText; 
    }
}
