﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string s = this.Page.Request.FilePath;
            if (s.Contains("AdShareView.aspx"))
            {
                sideNavigation.Visible = false;
            }
        }
    }
}
