﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using DAL.Repositories;
using System.Text;

public partial class ProfileId : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect("Default.aspx");
        }

        if (!IsPostBack)
        {
            LoadProfile();
        }


    }

    private void LoadProfileTag(Int64 varId)
    {
        ProfileRepository objPro = new ProfileRepository();
        List<string> lst = new List<string>();
        lst = objPro.getTagsForProfile(Convert.ToInt64(Session[clsSession.AccId]));
        StringBuilder sb = new StringBuilder();
        if (lst.Count > 0)
        {
            foreach (string tagName in lst)
            {
                sb.Append("<div class=\"filterDivs\">" + tagName + "</div>&nbsp;");
                sb.Append("<div class=\"filterDivsSpace\">&nbsp;&nbsp;</div>");
            }
            lblTags.Text = sb.ToString();
        }

    }

    private void LoadProfile()
    {
        //lblTags.Text = "<div class=\"filterDivs\"> </div>";
        Profile objPro = new Profile();
        ProfileRepository repoPro = new ProfileRepository();
        objPro = repoPro.ShowProfile(Convert.ToInt64(Session[clsSession.AccId]));
        if (objPro.ProfilePic.Contains("facebook"))
            proImg.ImageUrl = objPro.ProfilePic;
        else
            proImg.ImageUrl = "~/Content/profilepic/" + objPro.ProfilePic;
        lblBandName.Text = objPro.DisplayName;
        lblBandName.Text += ". " + objPro.CityName + ", " + objPro.CountryName;
        lblDescription.Text = objPro.Description;
        lblSoundcloudFrame.Text = objPro.SoundCloudFrame;
        lblYoutubeFrame.Text = objPro.YoutubeFrame;
        if (objPro.Facebook == "")
            hyperFB.Visible = false;
        else
            hyperFB.NavigateUrl = "http://www.facebook.com/" + objPro.Facebook;
        if (objPro.Twitter == "")
            hyperTwitter.Visible = false;
        hyperTwitter.NavigateUrl = "http://www.twitter.com/" + objPro.Twitter;
        LoadProfileTag(Convert.ToInt64(Session[clsSession.AccId]));
    }
    protected void btnGotToEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditProfile.aspx");
    }
}