﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using DAL.Repositories;
using BLL;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

public partial class Default123 : System.Web.UI.Page
{
    string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader reader;
    protected void Page_Load(object sender, EventArgs e)
    {

        ClientScript.GetPostBackEventReference(this, string.Empty);
        if (Convert.ToString(Session[clsSession.AccId]) == "")
        {
            Session.Abandon();
            Response.Redirect(Redirect.GoToHome());
        }
        if (!IsPostBack)
        {

            ProfileRepository objProRepo = new ProfileRepository();
            string location = Convert.ToString(objProRepo.getLocation(Convert.ToInt64(Session[clsSession.AccId])));
            if (location != "")
            {
                string[] locArr = location.Split('-');
                Session[clsSession.LocationIds] = Convert.ToString(locArr[0]) + "," + Convert.ToString(locArr[1]) + "," + Convert.ToString(locArr[2]);
                SetSessionOfRegisteredUser(locArr);
                Response.Redirect(Redirect.GoToUserHome());
            }
            LoadCountries();
        }

    }

    private void SetSessionOfRegisteredUser(string[] locArr)
    {
        con = new SqlConnection(connstr);
        cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandText = "Select CountryName from country where countryId=@countryId";
        //CountryName
        cmd.Parameters.AddWithValue("@countryId", locArr[0]);
        Session[clsSession.Location] = Convert.ToString(cmd.ExecuteScalar());
        //CityName
        cmd.Parameters.Clear();
        cmd.CommandText = "Select StateName from States where stateId=@StateId";
        cmd.Parameters.AddWithValue("@StateId", locArr[1]);
        Session[clsSession.Location] += "," + Convert.ToString(cmd.ExecuteScalar());
        //CityName
        cmd.Parameters.Clear();
        cmd.CommandText = "Select CityName from Cities where CityId=@CityId";
        cmd.Parameters.AddWithValue("@CityId", locArr[2]);
        Session[clsSession.Location] += "," + Convert.ToString(cmd.ExecuteScalar());
        con.Close();
    }



    private void LoadCountries()
    {
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandText = "Select CountryId, CountryName from Country where available=1";
        SqlDataReader reader = cmd.ExecuteReader();
        ddlCountryList.DataSource = reader;
        ddlCountryList.DataTextField = "CountryName";
        ddlCountryList.DataValueField = "CountryId";
        ddlCountryList.DataBind();
        reader.Close();
        con.Close();
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        // // Perfrom Server Side Validations for DDLs and Hidden Value
        //Profile objProfile = new Profile();
        //ProfileRepository repProfile = new ProfileRepository();
        //objProfile.CountryName = CountryVal.Value;
        //objProfile.StateName = StateVal.Value;
        //objProfile.CityName = CityVal.Value;
        //objProfile.AccountId = Convert.ToInt64(Session[clsSession.AccId]);
        //int IfLocSet = repProfile.SetLocation(objProfile);
        //if (IfLocSet != 0)
        //{
        //    Session["Location"] = CountryVal.Value.Trim() + "," + StateVal.Value.Trim() + "," + CityVal.Value.Trim();
        //    Response.Redirect("Messages.aspx");
        //}
        //else
        //    Response.Write("<script>alert('Invalid Credentials. Try Again!')</script>");
    }
    protected void ddlStateCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandText = "Select CityId, CityName from Cities where StateCode=@StateCode";
        cmd.Parameters.AddWithValue("@StateCode", ddlStateCity.SelectedValue);
        SqlDataReader reader = cmd.ExecuteReader();
        ddlCityList.Items.Clear();
        ddlCityList.DataSource = reader;
        ddlCityList.DataTextField = "CityName";
        ddlCityList.DataValueField = "CityId";
        ddlCityList.DataBind();
        reader.Close();
        con.Close();
        ddlCityList.Items.Insert(0, new ListItem("---Select City---", "-1"));

    }
    protected void ddlCountryList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandText = "Select statename,stateId from states where countryId=@countryId and available=1";
        cmd.Parameters.AddWithValue("@countryId", ddlCountryList.SelectedValue);
        SqlDataReader reader = cmd.ExecuteReader();
        ddlStateCity.Items.Clear();
        ddlStateCity.DataSource = reader;
        ddlStateCity.DataTextField = "statename";
        ddlStateCity.DataValueField = "stateId";
        ddlStateCity.DataBind();
        reader.Close();
        con.Close();
        ddlStateCity.Items.Insert(0, new ListItem("---Select State/Sub-Region---", "-1"));

    }
    protected void btnAddLoc_Click(object sender, EventArgs e)
    {
        // Perfrom Server Side Validations for DDLs and Hidden Value
        Profile objProfile = new Profile();
        ProfileRepository repProfile = new ProfileRepository();
        objProfile.CountryCode = Convert.ToInt32(ddlCountryList.SelectedValue);
        objProfile.StateCode = Convert.ToInt32(ddlStateCity.SelectedValue);
        objProfile.CityCode = Convert.ToInt32(ddlCityList.SelectedValue);
        objProfile.AccountId = Convert.ToInt64(Session[clsSession.AccId]);
        int IfLocSet = repProfile.SetLocation(objProfile);
        if (IfLocSet != 0)
        {
            SetLocationSesssion();
            SendWelcomeMail();
            Response.Redirect(Redirect.GoToEditProfilePage());
        }
        else
            Response.Write("<script>alert('Invalid Credentials. Try Again!')</script>");
    }

    private void SetLocationSesssion()
    {
        Session[clsSession.LocationIds] = ddlCountryList.SelectedValue + "," + ddlStateCity.SelectedValue + "," + ddlCityList.SelectedValue;
        Session[clsSession.Location] = ddlCountryList.SelectedItem.Text + "," + ddlStateCity.SelectedItem.Text + "," + ddlCityList.SelectedItem.Text;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (chkSuggest.Checked == true)
        {
            if (ddlSuggestCountry.SelectedValue != "-1" && ddlSuggestState.SelectedValue != "-1")
            {
                if (suggCityTxt.Text != "")
                {
                    AddNewLocation(); // First time user would go to edit profile page!
                    Response.Redirect(Redirect.GoToEditProfilePage());
                }
                else
                {
                    Response.Write("<script>alert('You must enter a city name!');</script>");
                    return;
                }
            }
            else
            {
                Response.Write("<script>alert('You must select both country and state!');</script>");
                return;
            }
        }
        else
        {
            Response.Write("<script>alert('You are not allowed to that!');</script>");
            return;
        }
    }

    private void AddNewLocation()
    {
        con = new SqlConnection(connstr);
        cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "setNewLocaation";
        cmd.Parameters.AddWithValue("@CityName", suggCityTxt.Text);
        cmd.Parameters.AddWithValue("@StateCode", ddlSuggestState.SelectedValue);
        int CityId = Convert.ToInt32(cmd.ExecuteScalar());

        //Updating State availabilty
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Update States Set Available=1 Where StateId=@StateCode";
        cmd.Parameters.Add("@StateCode", SqlDbType.Int).Value = ddlSuggestState.SelectedValue;
        cmd.ExecuteNonQuery();


        //Updating Selected Country to available
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Update country Set Available=1 Where CountryId=@CountryId";
        cmd.Parameters.Clear();
        cmd.Parameters.Add("@CountryId", SqlDbType.Int).Value = ddlSuggestCountry.SelectedValue;
        cmd.ExecuteNonQuery();

        //Inserting into Profile
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@CityId", SqlDbType.Int).Value = CityId;
        cmd.Parameters.Add("@StateCode", SqlDbType.Int).Value = ddlSuggestState.SelectedValue;
        cmd.Parameters.Add("@CountryId", SqlDbType.Int).Value = ddlSuggestCountry.SelectedValue;
        cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = Convert.ToInt64(Session[clsSession.AccId]);
        cmd.CommandText = "Update Profile Set CountryCode=@CountryId, StateCode=@StateCode, CityCode=@CityId where AccountId=@AccountId";
        cmd.ExecuteNonQuery();
        //Setting Location Sessions
        Session[clsSession.LocationIds] = ddlSuggestCountry.SelectedValue + "," + ddlSuggestState.SelectedValue + "," + CityId;
        Session[clsSession.Location] = ddlSuggestCountry.SelectedItem.Text + "," + ddlSuggestState.SelectedItem.Text + "," + suggCityTxt.Text;
        //Sending mail for new registered user
        SendWelcomeMail();
    }

    private void SendWelcomeMail()
    {
        con = new SqlConnection(connstr);
        cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Select Email From Account where AccountId=@AccountId";
        cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = Convert.ToInt64(Session[clsSession.AccId]);
        string email = Convert.ToString(cmd.ExecuteReader());
        if (email != "")
        {
            if (!Regex.IsMatch(email, @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"))
            {

            }
            else
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    SmtpClient smtp = new SmtpClient("webmail.meraband.com");
                    msg.From = new MailAddress("info@meraband.com");
                    msg.To.Add(new MailAddress("rahul.sh2087@gmail.com"));
                    msg.IsBodyHtml = true;
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div style=\"font-size:large; float:left; font-weight:bold; padding:10px;\" >Hello Music</div>");
                    sb.Append("<div style=\"text-align:center; color:#ff6a00; font:xx-large;\">A big and warm welcome to Meraband</div>");
                    sb.Append(" <div style=\"text-align:center; font-size:larger; width:50%; margin:0 auto; padding:10px;\">You can use Meraband to:- <br /></div>");
                    sb.Append(" <div style=\"font-size:larger; width:40%; margin:0 auto; color:#ff6a00;\">");
                    sb.Append(" <ol>");
                    sb.Append(" <li>Find musicians for your project or band. Start a band!</li>");
                    sb.Append("<li>Communiate with musicians!</li>");
                    sb.Append("<li>Find gear you're looking for (Coming Soon..)</li>");
                    sb.Append("<li>Promote yourself!</li>");
                    sb.Append("<li>Gig Information-Band Diaries (Coming Soon..)!</li>");
                    sb.Append("</ol>");
                    sb.Append("</div>");
                    sb.Append("<div style=\"text-align:justify; font-size:large; padding:10px;\">");
                    sb.Append("We are trying our best to give you best possible experience, yet if you find any error please try not to judge and report to us about the problem.");
                    sb.Append("<br /> We are working in every free second to tune this free service in best possible and above all it's just a small beginning! ");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("Our goal is to provide any musician or enthusiasts with best possible tools to find just what they need.");
                    sb.Append("Find musicians be for a studio-session, starting a new band or just a single project you have in mind.");
                    sb.Append("</div>");
                    sb.Append("<div style=\"font-size:large; float:left; font-weight:bold; padding:10px;\">");
                    sb.Append("Thank You! The Meraband Team</div>&nbsp; support@meraband.com");
                    msg.Body = sb.ToString();
                    msg.Subject = "Welcome to Meraband";
                    smtp.Send(msg);
                }
                catch (Exception ex)
                {
                    //To be handled
                }
            }
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default6.aspx");
    }
    protected void chkSuggest_CheckedChanged(object sender, EventArgs e)
    {

        if (chkSuggest.Checked == true)
        {
            LocPanel.Visible = false;
            SuggestPanel.Visible = true;
            //  addLocDiv.Attributes.Add("style", "display:'none'");
            SuggLovDiv.Attributes.Add("style", "display:''");
        }
        else
        {
            LocPanel.Visible = true;
            SuggestPanel.Visible = false;
            //  addLocDiv.Attributes.Add("style", "display:''");
            SuggLovDiv.Attributes.Add("style", "display:'none'");
        }
        LoadDDLCountrySuggest();
    }

    private void LoadDDLCountrySuggest()
    {
        string connstr = WebConfigurationManager.ConnectionStrings["MBConStr"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        ddlSuggestState.Items.Clear();
        cmd.CommandText = "Select CountryName, CountryId from Country";
        reader = cmd.ExecuteReader();
        ddlSuggestCountry.DataSource = reader;
        ddlSuggestCountry.DataTextField = "CountryName";
        ddlSuggestCountry.DataValueField = "CountryId";
        ddlSuggestCountry.DataBind();
        reader.Close();
        con.Close();
        ddlSuggestCountry.Items.Insert(0, new ListItem("---Please Select Your Country---", "-1"));
    }

    protected void ddlSuggestState_SelectedIndexChanged(object sender, EventArgs e)
    {
        con = new SqlConnection(connstr);
        cmd = new SqlCommand();
        cmd.Connection = con;
        con.Open();
        ddlSuggestState.Items.Clear();
        cmd.CommandText = "Select StateName, StateId from States where CountryId=@CountryId";
        cmd.Parameters.AddWithValue("@CountryId", ddlSuggestCountry.SelectedValue);
        reader = cmd.ExecuteReader();
        ddlSuggestState.DataSource = reader;
        ddlSuggestState.DataTextField = "StateName";
        ddlSuggestState.DataValueField = "StateId";
        ddlSuggestState.DataBind();
        reader.Close();
        con.Close();
        ddlSuggestState.Items.Insert(0, new ListItem("---Then Select Your State---", "-1"));
    }
}